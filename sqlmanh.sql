USE [master]
GO
/****** Object:  Database [mydemoaptech]    Script Date: 14-Oct-16 2:29:43 PM ******/
CREATE DATABASE [mydemoaptech]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'mydemoaptech', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\mydemoaptech.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'mydemoaptech_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\mydemoaptech_log.ldf' , SIZE = 1072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [mydemoaptech] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [mydemoaptech].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [mydemoaptech] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [mydemoaptech] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [mydemoaptech] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [mydemoaptech] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [mydemoaptech] SET ARITHABORT OFF 
GO
ALTER DATABASE [mydemoaptech] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [mydemoaptech] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [mydemoaptech] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [mydemoaptech] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [mydemoaptech] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [mydemoaptech] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [mydemoaptech] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [mydemoaptech] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [mydemoaptech] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [mydemoaptech] SET  ENABLE_BROKER 
GO
ALTER DATABASE [mydemoaptech] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [mydemoaptech] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [mydemoaptech] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [mydemoaptech] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [mydemoaptech] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [mydemoaptech] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [mydemoaptech] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [mydemoaptech] SET RECOVERY FULL 
GO
ALTER DATABASE [mydemoaptech] SET  MULTI_USER 
GO
ALTER DATABASE [mydemoaptech] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [mydemoaptech] SET DB_CHAINING OFF 
GO
ALTER DATABASE [mydemoaptech] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [mydemoaptech] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [mydemoaptech] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'mydemoaptech', N'ON'
GO
USE [mydemoaptech]
GO
/****** Object:  Table [dbo].[account]    Script Date: 14-Oct-16 2:29:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[account](
	[username] [varchar](250) NOT NULL,
	[password] [varchar](250) NOT NULL,
	[fullname] [varchar](250) NOT NULL,
	[email] [varchar](250) NOT NULL,
	[age] [int] NOT NULL,
	[createdate] [datetime] NOT NULL,
	[address] [nvarchar](250) NULL,
	[phone] [nvarchar](50) NULL,
 CONSTRAINT [PK_account] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[category]    Script Date: 14-Oct-16 2:29:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[category](
	[categoryId] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](256) NOT NULL,
	[Alias] [nvarchar](256) NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [nvarchar](256) NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateBy] [nvarchar](256) NULL,
	[Description] [nvarchar](500) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_category] PRIMARY KEY CLUSTERED 
(
	[categoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[feedBack]    Script Date: 14-Oct-16 2:29:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feedBack](
	[feedBackId] [int] IDENTITY(1,1) NOT NULL,
	[fullName] [nvarchar](50) NULL,
	[phone] [int] NULL,
	[email] [nvarchar](50) NULL,
	[createDate] [datetime] NULL,
	[title] [nvarchar](100) NULL,
	[content] [nvarchar](max) NULL,
 CONSTRAINT [PK_feedBack] PRIMARY KEY CLUSTERED 
(
	[feedBackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[orders]    Script Date: 14-Oct-16 2:29:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[orders](
	[ordersId] [int] IDENTITY(1,1) NOT NULL,
	[datecreation] [datetime] NOT NULL,
	[totalmoney] [decimal](18, 0) NOT NULL,
	[status] [bit] NOT NULL,
	[username] [varchar](250) NULL,
	[paymentId] [int] NULL,
	[CustomerName] [nvarchar](256) NULL,
	[CustomerAddress] [nvarchar](256) NULL,
	[CustomerMobile] [nvarchar](50) NULL,
	[CustomerMessage] [nvarchar](256) NULL,
	[transferId] [int] NULL,
	[CustomerEmaill] [varchar](250) NULL,
 CONSTRAINT [PK_orders] PRIMARY KEY CLUSTERED 
(
	[ordersId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ordersdetail]    Script Date: 14-Oct-16 2:29:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ordersdetail](
	[productid] [int] NOT NULL,
	[ordersid] [int] NOT NULL,
	[price] [decimal](18, 0) NOT NULL,
	[quantity] [int] NOT NULL,
 CONSTRAINT [PK_ordersdetail] PRIMARY KEY CLUSTERED 
(
	[productid] ASC,
	[ordersid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[payment]    Script Date: 14-Oct-16 2:29:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[payment](
	[paymentId] [int] IDENTITY(1,1) NOT NULL,
	[paymentName] [nvarchar](50) NULL,
 CONSTRAINT [PK_payment] PRIMARY KEY CLUSTERED 
(
	[paymentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Permission]    Script Date: 14-Oct-16 2:29:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Permission](
	[idrl] [int] NOT NULL,
	[username] [varchar](250) NOT NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[idrl] ASC,
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[product]    Script Date: 14-Oct-16 2:29:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[product](
	[productId] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](250) NOT NULL,
	[price] [decimal](18, 0) NOT NULL,
	[quantity] [int] NOT NULL,
	[description] [ntext] NULL,
	[photo] [varchar](250) NULL,
	[categoryId] [int] NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [nvarchar](256) NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateBy] [nvarchar](256) NULL,
	[Status] [bit] NOT NULL,
	[PromotionPrice] [decimal](18, 0) NULL,
	[supplierId] [int] NULL,
	[originalPrice] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_product] PRIMARY KEY CLUSTERED 
(
	[productId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Role]    Script Date: 14-Oct-16 2:29:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[roleId] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](250) NOT NULL,
	[createdate] [datetime] NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[roleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[supplier]    Script Date: 14-Oct-16 2:29:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[supplier](
	[supplierId] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](250) NOT NULL,
	[address] [nvarchar](250) NULL,
	[phone] [varchar](50) NOT NULL,
	[status] [bit] NOT NULL,
	[createdate] [datetime] NULL,
 CONSTRAINT [PK_supplier] PRIMARY KEY CLUSTERED 
(
	[supplierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[transfer]    Script Date: 14-Oct-16 2:29:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transfer](
	[transferId] [int] IDENTITY(1,1) NOT NULL,
	[transferName] [nvarchar](250) NULL,
 CONSTRAINT [PK_transfer] PRIMARY KEY CLUSTERED 
(
	[transferId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[account] ([username], [password], [fullname], [email], [age], [createdate], [address], [phone]) VALUES (N'khanh1212', N'1234567', N'fullname', N'emaiil1@gmail.com', 18, CAST(N'2016-10-12 14:58:28.510' AS DateTime), N'addre1', N'12312312321312')
INSERT [dbo].[account] ([username], [password], [fullname], [email], [age], [createdate], [address], [phone]) VALUES (N'khanh123123', N'1234567', N'123khanh', N'email@gmail.com', 19, CAST(N'2016-10-12 14:56:45.827' AS DateTime), N'address13211', N'123456723234234')
INSERT [dbo].[account] ([username], [password], [fullname], [email], [age], [createdate], [address], [phone]) VALUES (N'khanhaptech', N'1234567', N'khanh', N'khanhaptech@gmail.com', 20, CAST(N'2016-09-25 00:00:00.000' AS DateTime), N'da lat', N'019283333')
INSERT [dbo].[account] ([username], [password], [fullname], [email], [age], [createdate], [address], [phone]) VALUES (N'longdang', N'fcea920f7412b5da7be0cf42b8c93759', N'nguyen dang', N'dang@gmail.com', 22, CAST(N'2016-10-10 09:41:40.140' AS DateTime), NULL, N'018222828211')
INSERT [dbo].[account] ([username], [password], [fullname], [email], [age], [createdate], [address], [phone]) VALUES (N'manhaptech', N'fcea920f7412b5da7be0cf42b8c93759', N'Lê Hùng M&#7841;nh', N'manh@gmail.com', 45, CAST(N'2016-10-14 12:22:05.283' AS DateTime), N'hcm ne ahihi', N'029111111')
INSERT [dbo].[account] ([username], [password], [fullname], [email], [age], [createdate], [address], [phone]) VALUES (N'nghiaafc', N'fcea920f7412b5da7be0cf42b8c93759', N'nguyen van nghia', N'nghia@gmail.com', 21, CAST(N'2016-10-11 22:54:03.607' AS DateTime), NULL, N'089122222112')
INSERT [dbo].[account] ([username], [password], [fullname], [email], [age], [createdate], [address], [phone]) VALUES (N'quyenafc', N'fcea920f7412b5da7be0cf42b8c93759', N'nguyen trong quyen', N'trongquyenafc@gmail.com', 21, CAST(N'2016-09-25 00:00:00.000' AS DateTime), NULL, N'777777777777777')
INSERT [dbo].[account] ([username], [password], [fullname], [email], [age], [createdate], [address], [phone]) VALUES (N'quyenmat123', N'1234567', N'quyenmat', N'khanhbozz@gmail.com', 18, CAST(N'2016-10-12 13:33:26.623' AS DateTime), N'address13221', N'123132123123123123')
SET IDENTITY_INSERT [dbo].[category] ON 

INSERT [dbo].[category] ([categoryId], [name], [Alias], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Description], [Status]) VALUES (1, N'Antiques & Collectibles', N'quan-jean', CAST(N'2016-12-11 00:00:00.000' AS DateTime), NULL, CAST(N'2016-09-30 01:18:37.017' AS DateTime), N'quyenafc', N'good', 1)
INSERT [dbo].[category] ([categoryId], [name], [Alias], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Description], [Status]) VALUES (2, N'Art', N'book-store', CAST(N'2016-09-21 14:26:23.407' AS DateTime), NULL, NULL, NULL, N'good', 1)
INSERT [dbo].[category] ([categoryId], [name], [Alias], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Description], [Status]) VALUES (3, N'Comics & Graphic Novels', N'sach-danh-nhan', CAST(N'2016-09-21 14:27:05.517' AS DateTime), NULL, NULL, NULL, N'good', 1)
INSERT [dbo].[category] ([categoryId], [name], [Alias], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Description], [Status]) VALUES (4, N'Biography & Autobiography', N'sach-danh-nhan', CAST(N'2016-09-21 14:27:14.570' AS DateTime), NULL, NULL, NULL, N'good', 1)
INSERT [dbo].[category] ([categoryId], [name], [Alias], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Description], [Status]) VALUES (5, N'Computers', N'sach-thieu-nhi', CAST(N'2016-09-21 14:27:46.277' AS DateTime), NULL, NULL, NULL, N'good', 1)
INSERT [dbo].[category] ([categoryId], [name], [Alias], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Description], [Status]) VALUES (14, N'Cooking', N'dien-thoai', CAST(N'2016-09-21 15:43:38.843' AS DateTime), NULL, CAST(N'2016-10-04 18:20:22.063' AS DateTime), N'quyenafc', N'aaaaaaaaaaaaaaaa', 1)
INSERT [dbo].[category] ([categoryId], [name], [Alias], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Description], [Status]) VALUES (15, N'Foreign Language', N'may-tinh', CAST(N'2016-09-21 15:45:46.130' AS DateTime), NULL, CAST(N'2016-09-22 21:25:34.030' AS DateTime), NULL, N'', 1)
INSERT [dbo].[category] ([categoryId], [name], [Alias], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Description], [Status]) VALUES (99, N'Health & Fitness', N'aaaaaaaaaaaa', CAST(N'2016-09-27 18:27:02.613' AS DateTime), N'vinhafc', CAST(N'2016-10-06 18:13:22.213' AS DateTime), N'quyenafc', N'qqqqqqaaaaaaaaaaaaaaaaaaa', 1)
SET IDENTITY_INSERT [dbo].[category] OFF
SET IDENTITY_INSERT [dbo].[feedBack] ON 

INSERT [dbo].[feedBack] ([feedBackId], [fullName], [phone], [email], [createDate], [title], [content]) VALUES (3, N'tran ti ti', 168888, N'titi@gmail.com', CAST(N'2016-07-21 00:00:00.000' AS DateTime), N'book_content', N'good')
INSERT [dbo].[feedBack] ([feedBackId], [fullName], [phone], [email], [createDate], [title], [content]) VALUES (4, N'chi thanh long', 233333, N'long@gmail.com', CAST(N'2016-11-22 00:00:00.000' AS DateTime), N'title_advance', N'good')
SET IDENTITY_INSERT [dbo].[feedBack] OFF
SET IDENTITY_INSERT [dbo].[orders] ON 

INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (17, CAST(N'2016-10-10 00:00:00.000' AS DateTime), CAST(20 AS Decimal(18, 0)), 0, N'khanhaptech', 1, N'khanh', N'da lat', N'019283333', N'vdfvdf', 1, N'khanhaptech@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (19, CAST(N'2016-10-09 00:00:00.000' AS DateTime), CAST(40 AS Decimal(18, 0)), 0, NULL, 1, N'tran dai su', N'phu quy', N'018299111', N'vdfvdfvdfvdf', 2, N'son@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (20, CAST(N'2016-02-09 00:00:00.000' AS DateTime), CAST(40 AS Decimal(18, 0)), 0, NULL, 2, N'cung an duong', N'phan thiet', N'018299111', N'vdfvdfvdfvdf', 2, N'son@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (21, CAST(N'2016-03-09 00:00:00.000' AS DateTime), CAST(40 AS Decimal(18, 0)), 0, NULL, 2, N'anh hung', N'phu quy', N'018299111', N'vdfvdfvdfvdf', 2, N'son@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (24, CAST(N'2016-06-09 00:00:00.000' AS DateTime), CAST(40 AS Decimal(18, 0)), 0, NULL, 3, N'bay ngoc lua', N'tam thanh', N'018299111', N'vdfvdfvdfvdf', 2, N'son@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (25, CAST(N'2016-10-09 00:00:00.000' AS DateTime), CAST(40 AS Decimal(18, 0)), 0, NULL, 2, N'lana cacca', N'phu quy', N'018299111', N'vdfvdfvdfvdf', 2, N'son@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (27, CAST(N'2016-10-09 00:00:00.000' AS DateTime), CAST(40 AS Decimal(18, 0)), 0, NULL, 3, N'qua qua ', N'long hai', N'018299111', N'vdfvdfvdfvdf', 2, N'son@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (28, CAST(N'2016-01-09 00:00:00.000' AS DateTime), CAST(40 AS Decimal(18, 0)), 0, NULL, 2, N'si si', N'phu quy', N'018299111', N'vdfvdfvdfvdf', 2, N'son@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (32, CAST(N'2016-02-09 00:00:00.000' AS DateTime), CAST(40 AS Decimal(18, 0)), 0, NULL, 2, N'si si', N'phu quy', N'018299111', N'vdfvdfvdfvdf', 2, N'son@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (34, CAST(N'2016-01-12 00:00:00.000' AS DateTime), CAST(40 AS Decimal(18, 0)), 0, NULL, 2, N'si si', N'phu quy', N'018299111', N'vdfvdfvdfvdf', 2, N'son@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (36, CAST(N'2016-04-15 00:00:00.000' AS DateTime), CAST(40 AS Decimal(18, 0)), 0, NULL, 2, N'si si', N'phu quy', N'018299111', N'vdfvdfvdfvdf', 2, N'son@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (37, CAST(N'2016-10-12 00:00:00.000' AS DateTime), CAST(40 AS Decimal(18, 0)), 0, NULL, 2, N'si si', N'phu quy', N'018299111', N'vdfvdfvdfvdf', 2, N'son@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (38, CAST(N'2016-10-12 13:39:18.997' AS DateTime), CAST(310 AS Decimal(18, 0)), 1, N'quyenmat123', 2, N'quyenmat123', N'address13221', N'123132123123123123', N'hqwhehqweh`', 4, N'khanhbozz@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (39, CAST(N'2016-10-12 14:54:10.977' AS DateTime), CAST(22 AS Decimal(18, 0)), 1, N'quyenafc', 2, N'quyenafc', N'342342', N'777777777777777', N'432423423', 1, N'trongquyenafc@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (40, CAST(N'2016-10-12 15:15:57.713' AS DateTime), CAST(12 AS Decimal(18, 0)), 1, N'manhaptech', 1, N'manhaptech', N'hcm ne', N'0001111111111111', N'&#273;âsdasd', 1, N'manh@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (41, CAST(N'2016-10-12 15:32:20.920' AS DateTime), CAST(12 AS Decimal(18, 0)), 1, N'manhaptech', 1, N'manhaptech', N'hcm ne ahi', N'0000111111111', N'nothing', 4, N'manh@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (42, CAST(N'2016-10-12 15:33:14.007' AS DateTime), CAST(15 AS Decimal(18, 0)), 1, N'manhaptech', 1, N'manhaptech', N'hcm ne ahi', N'0000000000000001', N'nothigng', 1, N'manh@gmail.com')
INSERT [dbo].[orders] ([ordersId], [datecreation], [totalmoney], [status], [username], [paymentId], [CustomerName], [CustomerAddress], [CustomerMobile], [CustomerMessage], [transferId], [CustomerEmaill]) VALUES (43, CAST(N'2016-10-12 15:34:01.083' AS DateTime), CAST(167 AS Decimal(18, 0)), 1, N'manhaptech', 1, N'manhaptech', N'hcm ne ahi', N'0000000000001', N'your late', 1, N'manh@gmail.com')
SET IDENTITY_INSERT [dbo].[orders] OFF
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (2, 17, CAST(10 AS Decimal(18, 0)), 2)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (2, 39, CAST(10 AS Decimal(18, 0)), 1)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (6, 19, CAST(20 AS Decimal(18, 0)), 2)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (6, 20, CAST(20 AS Decimal(18, 0)), 2)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (6, 21, CAST(20 AS Decimal(18, 0)), 2)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (6, 24, CAST(20 AS Decimal(18, 0)), 2)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (6, 25, CAST(20 AS Decimal(18, 0)), 2)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (6, 27, CAST(20 AS Decimal(18, 0)), 2)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (6, 28, CAST(20 AS Decimal(18, 0)), 2)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (6, 32, CAST(20 AS Decimal(18, 0)), 2)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (6, 34, CAST(20 AS Decimal(18, 0)), 2)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (6, 36, CAST(20 AS Decimal(18, 0)), 2)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (6, 37, CAST(20 AS Decimal(18, 0)), 2)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (28, 42, CAST(15 AS Decimal(18, 0)), 1)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (29, 39, CAST(12 AS Decimal(18, 0)), 1)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (33, 38, CAST(155 AS Decimal(18, 0)), 2)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (33, 43, CAST(155 AS Decimal(18, 0)), 1)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (34, 43, CAST(12 AS Decimal(18, 0)), 1)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (35, 40, CAST(12 AS Decimal(18, 0)), 1)
INSERT [dbo].[ordersdetail] ([productid], [ordersid], [price], [quantity]) VALUES (35, 41, CAST(12 AS Decimal(18, 0)), 1)
SET IDENTITY_INSERT [dbo].[payment] ON 

INSERT [dbo].[payment] ([paymentId], [paymentName]) VALUES (1, N'PayPal')
INSERT [dbo].[payment] ([paymentId], [paymentName]) VALUES (2, N'COD')
INSERT [dbo].[payment] ([paymentId], [paymentName]) VALUES (3, N'Master Card')
SET IDENTITY_INSERT [dbo].[payment] OFF
INSERT [dbo].[Permission] ([idrl], [username]) VALUES (4, N'khanh1212')
INSERT [dbo].[Permission] ([idrl], [username]) VALUES (4, N'khanh123123')
INSERT [dbo].[Permission] ([idrl], [username]) VALUES (4, N'khanhaptech')
INSERT [dbo].[Permission] ([idrl], [username]) VALUES (4, N'quyenmat123')
INSERT [dbo].[Permission] ([idrl], [username]) VALUES (6, N'quyenafc')
INSERT [dbo].[Permission] ([idrl], [username]) VALUES (8, N'nghiaafc')
SET IDENTITY_INSERT [dbo].[product] ON 

INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (2, N'Clean Eating for Busy Families ', CAST(10 AS Decimal(18, 0)), 11, N'', N'Clean-Eating-for-Busy-Families-Dudash-Michelle-9781592335145.jpg', 1, CAST(N'2016-09-29 23:37:09.570' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(2 AS Decimal(18, 0)), 1, CAST(8 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (6, N'Slavery and Social Death ', CAST(20 AS Decimal(18, 0)), -25, N'', N'Slavery-and-Social-Death-9780674810839.jpg', 1, CAST(N'2016-09-29 23:41:00.563' AS DateTime), N'quyenafc', CAST(N'2016-10-10 21:07:00.137' AS DateTime), N'quyenafc', 1, CAST(10 AS Decimal(18, 0)), 1, CAST(10 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (24, N'The Gadget Geek''s Guide to Your Sony PlayStation Portable ', CAST(37 AS Decimal(18, 0)), 21, N'assss', N'The-Gadget-Geek-s-Guide-to-Your-Sony-PlayStation-Portable-9781598632361.jpg', 1, CAST(N'2016-10-10 21:07:35.837' AS DateTime), N'quyenafc', CAST(N'2016-10-11 19:32:08.530' AS DateTime), N'quyenafc', 1, CAST(3 AS Decimal(18, 0)), 1, CAST(13 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (25, N'Clean Eating for Busy Families ', CAST(122 AS Decimal(18, 0)), 3, N'test', N'The-Borgias-Meyer-G-J-9780345526915.jpg', 1, CAST(N'2016-10-10 21:11:12.540' AS DateTime), N'quyenafc', CAST(N'2016-10-11 21:30:45.893' AS DateTime), N'quyenafc', 1, CAST(1222 AS Decimal(18, 0)), 1, CAST(12 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (26, N'Histoire de Saint Bernard. ', CAST(120 AS Decimal(18, 0)), 16, N'', N'9780316185394.jpg', 4, CAST(N'2016-10-10 21:12:34.223' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(12 AS Decimal(18, 0)), 2, CAST(100 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (27, N'The Walking Dead Compendium Volume 1', CAST(14 AS Decimal(18, 0)), 16, N'', N'Coolidge-Shlaes-Amity-9780061967559.jpg', 15, CAST(N'2016-10-10 21:13:21.343' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(4 AS Decimal(18, 0)), 4, CAST(12 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (28, N'The Ultimates', CAST(15 AS Decimal(18, 0)), 21, N'', N'Ultimates-by-Mark-Millar-Bryan-Hitch-Omnibus-Hc-9780785137801.jpg', 14, CAST(N'2016-10-10 21:14:14.853' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(5 AS Decimal(18, 0)), 3, CAST(12 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (29, N'Building Stories', CAST(12 AS Decimal(18, 0)), 12, N'', N'Building-Stories-Ware-Chris-9780375424335.jpg', 1, CAST(N'2016-10-11 18:14:21.603' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(122 AS Decimal(18, 0)), 1, CAST(10 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (30, N'The Walking Dead Volume 1: Days Gone Bye', CAST(1222 AS Decimal(18, 0)), 1, N'', N'The-Walking-Dead-Volume-1-9781582406725.jpg', 1, CAST(N'2016-10-11 18:21:34.897' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(12 AS Decimal(18, 0)), 1, CAST(12 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (33, N'Thomas Jefferson: The Art of Power', CAST(155 AS Decimal(18, 0)), 16, N'', N'Thomas-Jefferson-Meacham-Jon-9781400067664.jpg', 4, CAST(N'2016-10-11 19:35:50.627' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(21 AS Decimal(18, 0)), 3, CAST(145 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (34, N'Killing Kennedy: The End of Camelot', CAST(12 AS Decimal(18, 0)), 12, N'', N'2.jpg', 3, CAST(N'2016-10-12 14:12:04.077' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(2 AS Decimal(18, 0)), 2, CAST(10 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (35, N'The Amateur: Barack Obama in the White House', CAST(12 AS Decimal(18, 0)), 12, N'', N'9781596987852.jpg', 4, CAST(N'2016-10-12 14:19:36.967' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(23 AS Decimal(18, 0)), 1, CAST(12 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (36, N'Night', CAST(12 AS Decimal(18, 0)), 12, N'', N'Night-Wiesel-Elie-9780374500016.jpg', 4, CAST(N'2016-10-12 14:19:36.967' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(23 AS Decimal(18, 0)), 1, CAST(12 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (37, N'Coolidge', CAST(12 AS Decimal(18, 0)), 12, N'', N'Coolidge-Shlaes-Amity-9780061967559.jpg', 4, CAST(N'2016-10-12 14:19:36.967' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(23 AS Decimal(18, 0)), 1, CAST(12 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (38, N'My Share of the Task: A Memoir', CAST(12 AS Decimal(18, 0)), 12, N'', N'My-Share-of-the-Task-McChrystal-General-9781591844754.jpg', 4, CAST(N'2016-10-12 14:19:36.967' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(23 AS Decimal(18, 0)), 1, CAST(12 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (39, N'Detroit: An American Autopsy', CAST(12 AS Decimal(18, 0)), 12, N'', N'Detroit-LeDuff-Charlie-9781594205347.jpg', 15, CAST(N'2016-10-12 14:19:36.967' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(23 AS Decimal(18, 0)), 1, CAST(12 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (40, N'The Outpost: An Untold Story of American Valor', CAST(12 AS Decimal(18, 0)), 12, N'', N'9780316185394.jpg', 15, CAST(N'2016-10-12 14:19:36.967' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(23 AS Decimal(18, 0)), 1, CAST(12 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (41, N'Damn Few: Making the Modern Seal Warrior', CAST(12 AS Decimal(18, 0)), 12, N'', N'Damn-Few-Denver-Rorke-9781401324797.jpg', 15, CAST(N'2016-10-12 14:19:36.967' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(23 AS Decimal(18, 0)), 1, CAST(12 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (42, N'The Art of War', CAST(12 AS Decimal(18, 0)), 12, N'', N'9781936041756.jpg', 14, CAST(N'2016-10-12 14:19:36.967' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(23 AS Decimal(18, 0)), 1, CAST(12 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (43, N'The Passage of Power: The Years of Lyndon Johnson', CAST(12 AS Decimal(18, 0)), 12, N'', N'The-Passage-of-Power-Caro-Robert-A-9781847922953.jpg', 14, CAST(N'2016-10-12 14:19:36.967' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(23 AS Decimal(18, 0)), 1, CAST(12 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (44, N'The Borgias: The Hidden History', CAST(12 AS Decimal(18, 0)), 12, N'', N'The-Borgias-Meyer-G-J-9780345526915.jpg', 3, CAST(N'2016-10-12 14:19:36.967' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(23 AS Decimal(18, 0)), 1, CAST(12 AS Decimal(18, 0)))
INSERT [dbo].[product] ([productId], [name], [price], [quantity], [description], [photo], [categoryId], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy], [Status], [PromotionPrice], [supplierId], [originalPrice]) VALUES (45, N'Killing Kennedy: The End of Camelot
', CAST(12 AS Decimal(18, 0)), 12, N'', N'2.jpg', 3, CAST(N'2016-10-12 14:19:36.967' AS DateTime), N'quyenafc', NULL, NULL, 1, CAST(23 AS Decimal(18, 0)), 1, CAST(12 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[product] OFF
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([roleId], [name], [createdate]) VALUES (4, N'user', CAST(N'2016-09-25 00:00:00.000' AS DateTime))
INSERT [dbo].[Role] ([roleId], [name], [createdate]) VALUES (6, N'superadmin', CAST(N'2016-09-27 00:00:00.000' AS DateTime))
INSERT [dbo].[Role] ([roleId], [name], [createdate]) VALUES (8, N'admin', CAST(N'2016-09-27 00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Role] OFF
SET IDENTITY_INSERT [dbo].[supplier] ON 

INSERT [dbo].[supplier] ([supplierId], [name], [address], [phone], [status], [createdate]) VALUES (1, N'nanashop_afc', N'tphcm', N'0162882821', 1, NULL)
INSERT [dbo].[supplier] ([supplierId], [name], [address], [phone], [status], [createdate]) VALUES (2, N'lalashop', N'tphcm', N'017262881', 1, NULL)
INSERT [dbo].[supplier] ([supplierId], [name], [address], [phone], [status], [createdate]) VALUES (3, N'amazon', N'American', N'0192829191', 1, NULL)
INSERT [dbo].[supplier] ([supplierId], [name], [address], [phone], [status], [createdate]) VALUES (4, N'alibaba', N'china', N'0938329291', 1, NULL)
INSERT [dbo].[supplier] ([supplierId], [name], [address], [phone], [status], [createdate]) VALUES (7, N'titki', N'TP.HCMsss', N'22222', 1, NULL)
SET IDENTITY_INSERT [dbo].[supplier] OFF
SET IDENTITY_INSERT [dbo].[transfer] ON 

INSERT [dbo].[transfer] ([transferId], [transferName]) VALUES (1, N'Ship COD')
INSERT [dbo].[transfer] ([transferId], [transferName]) VALUES (2, N'Courier services')
INSERT [dbo].[transfer] ([transferId], [transferName]) VALUES (4, N'
Delivery Standards')
SET IDENTITY_INSERT [dbo].[transfer] OFF
ALTER TABLE [dbo].[orders]  WITH CHECK ADD  CONSTRAINT [FK_orders_account] FOREIGN KEY([username])
REFERENCES [dbo].[account] ([username])
GO
ALTER TABLE [dbo].[orders] CHECK CONSTRAINT [FK_orders_account]
GO
ALTER TABLE [dbo].[orders]  WITH CHECK ADD  CONSTRAINT [FK_orders_payment] FOREIGN KEY([paymentId])
REFERENCES [dbo].[payment] ([paymentId])
GO
ALTER TABLE [dbo].[orders] CHECK CONSTRAINT [FK_orders_payment]
GO
ALTER TABLE [dbo].[orders]  WITH CHECK ADD  CONSTRAINT [FK_orders_transfer] FOREIGN KEY([transferId])
REFERENCES [dbo].[transfer] ([transferId])
GO
ALTER TABLE [dbo].[orders] CHECK CONSTRAINT [FK_orders_transfer]
GO
ALTER TABLE [dbo].[ordersdetail]  WITH CHECK ADD  CONSTRAINT [FK_ordersdetail_orders] FOREIGN KEY([ordersid])
REFERENCES [dbo].[orders] ([ordersId])
GO
ALTER TABLE [dbo].[ordersdetail] CHECK CONSTRAINT [FK_ordersdetail_orders]
GO
ALTER TABLE [dbo].[ordersdetail]  WITH CHECK ADD  CONSTRAINT [FK_ordersdetail_product] FOREIGN KEY([productid])
REFERENCES [dbo].[product] ([productId])
GO
ALTER TABLE [dbo].[ordersdetail] CHECK CONSTRAINT [FK_ordersdetail_product]
GO
ALTER TABLE [dbo].[Permission]  WITH CHECK ADD  CONSTRAINT [FK_Permission_account] FOREIGN KEY([username])
REFERENCES [dbo].[account] ([username])
GO
ALTER TABLE [dbo].[Permission] CHECK CONSTRAINT [FK_Permission_account]
GO
ALTER TABLE [dbo].[Permission]  WITH CHECK ADD  CONSTRAINT [FK_Permission_Role] FOREIGN KEY([idrl])
REFERENCES [dbo].[Role] ([roleId])
GO
ALTER TABLE [dbo].[Permission] CHECK CONSTRAINT [FK_Permission_Role]
GO
ALTER TABLE [dbo].[product]  WITH CHECK ADD  CONSTRAINT [FK_product_category] FOREIGN KEY([categoryId])
REFERENCES [dbo].[category] ([categoryId])
GO
ALTER TABLE [dbo].[product] CHECK CONSTRAINT [FK_product_category]
GO
ALTER TABLE [dbo].[product]  WITH CHECK ADD  CONSTRAINT [FK_product_supplier] FOREIGN KEY([supplierId])
REFERENCES [dbo].[supplier] ([supplierId])
GO
ALTER TABLE [dbo].[product] CHECK CONSTRAINT [FK_product_supplier]
GO
/****** Object:  StoredProcedure [dbo].[proGetproduct]    Script Date: 14-Oct-16 2:29:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[proGetproduct]
as
begin
select *
from product 
end

GO
/****** Object:  StoredProcedure [dbo].[proGetproductandcategory]    Script Date: 14-Oct-16 2:29:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[proGetproductandcategory]
as
begin
select pr.productId,pr.name, ca.categoryId,ca.name
from product pr, category ca
where pr.categoryId=ca.categoryId
end

GO
/****** Object:  StoredProcedure [dbo].[proGetRevenueStatistic]    Script Date: 14-Oct-16 2:29:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[proGetRevenueStatistic]
   @fromDate datetime,
   @toDate datetime

as
begin
select ors.datecreation as Datecreate,sum(orde.price * orde.quantity) as Revenues,sum( (orde.price * orde.quantity) - (orde.quantity* pr.originalPrice) ) as  Benefit
from orders ors , ordersdetail orde, product pr
where ors.ordersId = orde.ordersid  and pr.productId = orde.productid
and ors.datecreation<=  @toDate  and ors.datecreation>=     @fromDate
group by ors.datecreation
end

GO
USE [master]
GO
ALTER DATABASE [mydemoaptech] SET  READ_WRITE 
GO
