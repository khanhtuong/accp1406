  <!-- Content Header (Page header) -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
  <script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.6.2.js">
</script>
  
  <script>

$(document).ready(function(){


         if($("#messgae").val()!=""){

             bootbox.alert($("#messgae").val());
         }

	
	  $('#fromaddminproduct').validate({
          rules: {
        	  name:{
            	    required: true,
            	     minlength: 3,
                     maxlength: 250
               },
               price:{
            	  required: true,
            	    number:true,
                    min:1,
                    max:1000000
                },
             
                originalPrice:{
              	  required: true,
              	    number:true,
              	     min:1,
                     max:1000000
                  },
                  quantity:{
                	  required: true,
                	    number:true,
                	     min:1,
                         max:1000000
                    },

                    promotionPrice:{
                        number:true,
                    	  min:0,
                    	  max:100000
                     },
           
          },
          messages: {
            
        	  name: {
                  required: "you must enter Name product",
                  minlength: "Name product must great than 3 character",
                  maxlength:"Name product less than 250 character"
              },
      
              price:{
          	    required: "you must enter Price",
                  max:"price must less than 1000000",
                  min:"price must great  0",
               
              },
              originalPrice:{
            	    required: "you must enter  original Price",
                    max:"original Price must less than 1000000",
                    min:"original Price must great  0",
                 
                },
                promotionPrice:{
                	
                    max:"Promotion Price must less than 1000000",
                    min:"Promotion Price must great  0",
                 
                },
                quantity:{
              	    required: "you must enter quantity",
                      max:"quantity must less than 1000",
                      min:"quantity must great  0",
                   
                  },
              
          }

      });

	
});

  </script>
  
  
  <style>
.error{
  color: red;
}

</style> 
<section class="content-header">
    <h1>
    
Edit Product
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product</a></li>
        <li><a >Product list</a></li>
        <li class="active">EDIT</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Horizontal Form -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Product</h3>
        </div><!-- /.box-header -->
     <input type="hidden" value="${message}" id="messgae"> 
        <s:form class="form-horizontal"  commandName="productview" enctype="multipart/form-data"  action="${pageContext.request.contextPath}/admin/product/edit-product.html" method="post"    id="fromaddminproduct" role="form" >
            <div class="box-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Product Name</label>
                    <div class="col-sm-10">
                        <s:input path="name" class="form-control" value="${name}"  />
              
                 
                      <s:hidden path="productId"/>
                    </div>
                </div>
       
                
                
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Price</label>
                    <div class="col-sm-10">
                        <s:input path="price" class="form-control"  value="${price==0?0:price}" />
                 
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Original Price</label>
                    <div class="col-sm-10">
                         <s:input path="originalPrice" class="form-control"  value="${originalPrice==0?0:originalPrice}" />
             
                    </div>
                </div>
           
               <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Promotion Price</label>
                    <div class="col-sm-10">
                     <s:input path="promotionPrice" class="form-control"  value="${promotionPrice==0?0:promotionPrice}" />
                    </div>
                </div>
                
                          <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Quantity</label>
                    <div class="col-sm-10">
                        <s:input path="quantity" class="form-control"  value="${quantity==0?0:quantity}" />
                 
                    </div>
                </div>
                
                         <div class="form-group">
                    <label  class="col-sm-2 control-label">Photo</label>
                    <div class="col-sm-10">
                      
                              <input type="file" name="file">
                            <s:hidden path="photo" />
                            <img alt="" src="${pageContext.request.contextPath}/assets/admin/img/${productview.photo}" width="90" height="80">
                     
                    </div>
           
                </div>
                
      <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Category</label>
                    <div class="col-sm-10">
              
                  
                
              
     
						
						<select name="categoryid" class="form-control">
						  
						
						<c:forEach var="ca" items="${listcate}">
						      <option value="${ca.categoryId }">${ca.name }</option>
						</c:forEach>
              
                </select>
                            
                            
                     
                    
                    </div>
                </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Supplier</label>
                    <div class="col-sm-10">
                      
   
						  
						<select name="supplierid" class="form-control">
						<c:forEach var="su" items="${listsu}">
						      <option value="${su.supplierId }">${su.name }</option>
						</c:forEach>
              
                </select>
                    </div>
                </div>
       

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                     
                        <s:textarea path="description" cols="20" rows="4" class="form-control" value="${description}"/>
                    </div>
                </div>
             
            
        </div>
            <div class="box-footer">
                <a href="${pageContext.request.contextPath}/admin/product/product.html" class="btn btn-default">Back Manged Product</a>
                <button type="submit" class="btn btn-success pull-right">Save</button>
            </div><!-- /.box-footer -->
        </s:form>
    </div><!-- /.box -->
</section><!-- /.content -->