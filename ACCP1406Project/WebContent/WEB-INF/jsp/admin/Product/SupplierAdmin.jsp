<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.6.2.js">
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.4.2.min.js">
 </script> 


<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-ui-1.8.2.custom.min.js">
 </script> 
 <link href="${pageContext.request.contextPath}/assets/admin/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


<style>

.fotmatsde{
  font-size: 17px;
  font-weight: 800px;
  font-family: sans-serif;
 
}

.formattile{
   color: blue;;
}

</style>



<script type="text/javascript">

$(document).ready(function(){

    $("#txtnamesearch").autocomplete({
        source: '${pageContext.request.contextPath}/admin/product/searchsupplier.html'

     });

    $("#loadimg").hide();




});



var productSupplierController = {

		init:function(){


	
		
			productSupplierController.lodata();
			productSupplierController.RegisterEvent();
	   },

    RegisterEvent:function(){

    	  $('#frmproductsupplier').validate({
              rules: {
            	  namesu:{
                	    required: true,
                	     minlength: 3,
                         maxlength: 250
                   },
                   phonesu:{
                	  required: true,
             	     
                    },

                    address:{
                        required: true,
               	        minlength: 4,
                        maxlength: 250
                     }
               
              },
              messages: {
                
            	  namesu: {
                      required: "you must enter name Supplier",
                      minlength: "Name Supplier must great than 3",
                      maxlength:"Name Supplier must less than 250"
                  },
                  phonesu: {
                	     required: "you must enter Phone Supplier",
                   
                  },

                  address:{
                	    required: "you must enter Address",
                        minlength: "address must great than 3",
                        maxlength:"address must less than 250"
                   }
              }

          });
	    $("#btnseearch").off('click').on('click',function(e){
    	    e.preventDefault();
         
    	    productSupplierController.lodata();
         });


	    $("#btnaddnew").off('click').on('click',function(e){
    	    e.preventDefault();
          $("#modeladdupdate").modal('show');
          productSupplierController.resetform();
       });

	      $("#btnSave").off('click').on('click', function (e) {
	    	     e.preventDefault();
	             var isValid = $('#frmproductsupplier').valid();
	             if(isValid){
	            	 productSupplierController.Savedata();
	             }
	            
	       });

	       $(".btn-editsu").off('click').on('click', function (e) {
	    	   e.preventDefault();
	    	    var id = $(this).data('id');
	           productSupplierController.loadedit(id);
	           $("#modeladdupdate").modal('show');
		    });

		    $(".btn-delete").off('click').on('click', function (e) {
		    	   e.preventDefault();
		           var id = $(this).data('id');
		           bootbox.confirm("Are you sure to delete this Product Supplier?", function (result) {
		               if (result) {
		            	      productSupplierController.deleteproductsupplier(id);
		               }

		            
		           });
		    });

		    $(".btn-info").on('click', function (e) {
		   	   e.preventDefault();
	           var id = $(this).data('id');
	           $("#modeldetails").modal('show');
	           productSupplierController.loadedit(id);
	     	});

		     $("#btn-editsu").on('click', function (e) {
		     	   e.preventDefault();
		           var id  =  $("#txtsupplierid").val();
		     	  productSupplierController.loadedit(id);
	        	    $("#modeldetails").modal('hide');
	               $("#modeladdupdate").modal('show');
			 });
        },





        deleteproductsupplier:function(id){
            $.ajax({
                type: "POST",
    
                data: { id: id },
                url: "${pageContext.request.contextPath}/admin/product/deleteproductsupplier.html",
                success: function (data) {
                    if(data=="success"){
        			    $("#loadimg").show();
        			     setTimeout(function () {
                        bootbox.alert("delete success", function () {
                            $("#loadimg").hide();
                           productSupplierController.lodata();
                        });

        			      }, 1000);
                    }
                    else {
                        bootbox.alert(data);
                    }
                    productCategoryController.RegisterEvent();
                }

            });
        },

        
        loadedit:function(id){
      
                    $.ajax({
                       type:"GET",
                       	headers : {
        					Accept : "application/json; charset=utf-8",
        					"Content-Type" : "application/json; charset=utf-8"
        				},
                           data:{id:id},
                           url:"${pageContext.request.contextPath}/admin/product/detailssupplier.html",
                           success:function(data){
                        	
                              if(data==null){
                                  bootbox.alert("not load Product category")
                                  }
                              else{
                             
                          	   $("#txtnamesu").val(data.name);
                                 $("#phonesu").val(data.phone);
                            
                                  $("#txaddress").val(data.address);
                                  data.status==true ?  $("#status").prop("checked",true): $("#status").prop("checked",false)
                        
                       
                               $("#hidId").val(data.supplierId);



                                  var dtailce =  new Date(data.createdate);
                                  $("#txtnamesude").html(data.name);
                                  $("#txtphonesude").html(data.phone)
                                  $("#txtaddresde").html(data.address);
                             
                               
                                   $("#txtcreatedatede").html(dtailce.getDate() + " /  " + (dtailce.getMonth()+1) + " / " + dtailce.getFullYear() );
                                   
                         
                                    $("#txtstatusde").html(data.status == true ? "<span class='label label-success'>Actived</span>" : "<span class='label label-danger'>Locked</span>");
                                    $("#txtsupplierid").val(data.supplierId);
                            	
                             }
                              productSupplierController.RegisterEvent();
                         }
                           
                               
                   });
            
            },

        Savedata:function(){
     	    var namesu =  $("#txtnamesu").val();
               var phonesu =  $("#phonesu").val();
           
               var address =  $("#txaddress").val();
               var status =  $("#status").is(':checked') ? true : false;
       
      
              var id =  $("#hidId").val();
               $.ajax({
                   type: "POST",
        
                   data:{
                	   namesu:namesu,
                	   phonesu:phonesu,
                	   address:address,
                 	 status:status,
                 	  id:id
                   },

           		 url : "${pageContext.request.contextPath}/admin/product/productsupplier.html",
           		  success: function (data) {
           			  if(data=="success"){
           				   $("#modeladdupdate").modal('hide');
           				    $("#loadimg").show();
           			
                     

                           setTimeout(function () {
                               bootbox.alert("Success Save", function () {
                             	    $("#loadimg").hide();
                                   
                     
                             	  productSupplierController.resetform();
                            	  productSupplierController.lodata();
                               });
                           }, 700);
                      
                       } else {

                           bootbox.alert(data);
                       }
           			productSupplierController.RegisterEvent();
                   }

              });
               
         },
  
        resetform:function(){
            $("#txtnamesu").val("");
            $("#txaddress").val("");
            $("#phonesu").val("");
           $("#status").val("");
           $("#hidId").val(0);
        },

       lodata:function(){
    	
    	   	 var namesu = $("#txtnamesearch").val();
    	   $.ajax({
               type:"GET",
           
           	headers : {
				Accept : "application/json; charset=utf-8",
				"Content-Type" : "application/json; charset=utf-8"
			},
	data:{namesu:namesu},
			url:"${pageContext.request.contextPath}/admin/product/listsupplier.html",
			success:function(data){


		                 	    var html = "";
				                var templates = $("#data-template").html();
				                $.each(data,function(i,item){
				                      var da =  new Date(item.createdate);
				    
				                    html += Mustache.render(templates, {
				                    	id:item.supplierId,
				                    	name:item.name,
				                    	phone:item.phone,
				                    	address:item.address,
				                    	createdate:da
				                    });

					            });

				                $("#tbdada").html(html);

				                productSupplierController.RegisterEvent();
               
		    }


          });
    			 
    	
       }
 }

productSupplierController.init();

</script>













  <section class="content-header">
      <h1>

        Product Supplier List
     
 
      </h1>
      <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/admin/home.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="${pageContext.request.contextPath}/admin/product/product-supplier.html">Supplier Category</a></li>
 
      </ol>
    </section>
    <section class="content">

<br>
<span id="result1"></span>
<div class="row">

        <div class="col-xs-12">
        
          <div class="box">
            <div class="box-header">
         <div class="col-md-5">
                    <input type="text"  class="form-control" id="txtnamesearch" placeholder="Type Name" />
                </div>
  <div class="col-md-3">
                    <button class="btn btn-primary" id="btnseearch">Search</button>
         
                </div>
              <div class="box-tools">
      
                <div class="input-group input-group-sm" style="width: 150px;">
                    <button id="btnaddnew" class="btn btn-success pull-right">Add Product Supplier</button>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
           
            <div class="box-body table-responsive no-padding">
			
                       <div id="loadimg">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
              <i class="fa fa-refresh fa-4x fa-spin"></i> <label class="label label-primary">loading...</label>
            </div>
            <div class="col-xs-4"></div>
          </div>
              <table class="table table-hover">
             <thead>  <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Phone</th>
                  <th>Address</th>
                  <th>Create date</th>
                  <th>Action</th>
                </tr>
                </thead>
                 <tbody id="tbdada">
           
     
             
           
        
               
             
              </tbody></table>
       
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
              <script id="data-template" type="x-tmpl-mustache">
                <tr>
                    <td>{{id}}</td>
                    <td>{{name}}</td>
                       <td>{{phone}}</td>
              
                    <td >{{address}}</td>
            
                     <td>{{createdate}}</td>
                    <td> 
<button class="btn  btn-sm btn-danger btn-delete" data-id="{{id}}"><i class="fa fa-trash"></i> </button> 

  <button class="btn btn-sm btn-primary btn-editsu" data-id="{{id}}"><i class="fa fa-pencil"></i></button>

  <button class="btn btn-sm btn-info btn-info" data-id="{{id}}"><i class="fa fa-info"></i></button>
</td>
                </tr>
            </script>
      </div>
      </section>
      
      
      
      
      
      
      
            
      
        <div class="modal fade" id="modeladdupdate" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
       
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add/Update Product Supplier</h4>
            </div>
                <form method="post"  id="frmproductsupplier">
            <div class="modal-body">
            
                    <div class="form-group">
                        <label for="txtname">Name:</label>
                        <input type="text" class="form-control" id="txtnamesu" name="namesu">
                        <input type="hidden" value="0" id="hidId" />
                    </div>
                    <div class="form-group">
                        <label for="txtSalary">Phone:</label>
                        <input type="text" class="form-control" id="phonesu" name="phonesu">
                    </div>
                <div class="form-group">
                        <label for="txtSalary">Address:</label>
                     
                        <textarea class="form-control" id="txaddress"></textarea>
                    </div>
                    
                         <div class="form-group">
                        <label for="txtSalary">Status:</label>
                        <input type="checkbox" id="status" checked="checked">
               
                    </div>
      
            </div>
            <div class="modal-footer">
                <button  class="btn btn-primary" id="btnSave">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>

    </div>
</div>




<div class="modal fade" id="modeldetails" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
       
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Product Supplier details</h4>
            </div>
            
            <div class="modal-body">
            
                    <div class="form-group">
                        <label for="txtname" class="formattile">Name : </label>&nbsp;&nbsp; <span class="fotmatsde"   id="txtnamesude"></span>
                    
                     
                      
                    </div>
                    <div class="form-group">
                                 <label for="txtname" class="formattile">Phone : </label> &nbsp;&nbsp; <span class="fotmatsde"  id="txtphonesude"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Address : </label> &nbsp;&nbsp;  <span class="fotmatsde"  id="txtaddresde"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Create Date : </label> &nbsp;&nbsp; <span class="fotmatsde"  id="txtcreatedatede"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Status : </label> &nbsp;&nbsp;  <span class="fotmatsde"  id="txtstatusde"></span>
                    
                    </div>
                    
              
            </div>
            <div class="modal-footer">
            <input type="hidden" id="txtsupplierid">
         <button type="button" class="btn btn-primary" id="btn-editsu" style="margin-right: 440px;">Edit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        
        </div>

    </div>
</div>
      
      
   