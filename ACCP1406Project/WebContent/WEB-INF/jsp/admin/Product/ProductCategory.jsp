<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.6.2.js">
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.4.2.min.js">
 </script> 


<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-ui-1.8.2.custom.min.js">
 </script> 
 <link href="${pageContext.request.contextPath}/assets/admin/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


<style>

.fotmatsde{
  font-size: 17px;
  font-weight: 800px;
  font-family: sans-serif;
 
}

.formattile{
   color: blue;;
}

</style>



<script type="text/javascript">

$(document).ready(function(){

    $("#txtnamesearch").autocomplete({
        source: '${pageContext.request.contextPath}/admin/product/searchca.html'

     });

    $("#loadimg").hide();




});



var productCategoryController = {

		init:function(){


	
		
			productCategoryController.lodata();
			productCategoryController.RegisterEvent();
	   },

    RegisterEvent:function(){


  	  $('#frmproductcategory').validate({
            rules: {
                name:{
              	    required: true,
              	     minlength: 3,
                       maxlength: 250
                 },
                alias:{
              	  required: true,
           	     minlength: 3,
                    maxlength: 250
                  }
             
            },
            messages: {
              
          	  name: {
                    required: "you must enter name category",
                    minlength: "Name category must great than 3 charater",
                    maxlength:"Name category must less than 250 charater"
                },
                alias: {
              	     required: "you must enter alias category",
                       minlength: "Alias category must great than 3 charater",
                       maxlength:"Alias category must less than 250 charater"
                }
            }

        });

	    $("#btnseearch").off('click').on('click',function(e){
    	    e.preventDefault();
         
                productCategoryController.lodata();
         });


	    $("#btnaddnew").off('click').on('click',function(e){
    	    e.preventDefault();
          $("#modeladdupdate").modal('show');
          productCategoryController.resetform();
       });


      $("#btnSave").off('click').on('click', function (e) {
    	     e.preventDefault();
             var isValid = $('#frmproductcategory').valid();
             if(isValid){
            	  productCategoryController.Savedata();
             }
            
       });

      $(".btn-delete").off('click').on('click', function (e) {
    	   e.preventDefault();
          var id = $(this).data('id');
          bootbox.confirm("Are you sure to delete this Product Category?", function (result) {
              if (result) {
                    productCategoryController.deleteproductcategory(id);
              }

           
          });
       
      });


      $(".btn-edit").off('click').on('click',function(e){
    	   e.preventDefault();

    	    var id = $(this).data('id');
    	   productCategoryController.loadedit(id);
           $("#modeladdupdate").modal('show');
       });



      $(".btn-info").off('click').on('click',function(e){
    	   e.preventDefault();
    
    	   var id = $(this).data('id');
           $("#modeldetails").modal('show');
           productCategoryController.loadedit(id);
      });

      $("#btn-editca").off('click').on('click',function(e){
    	   e.preventDefault();
               var id  =  $("#txtcategoryid").val();
        	   productCategoryController.loadedit(id);
        	    $("#modeldetails").modal('hide');
               $("#modeladdupdate").modal('show');
       });

        
        },





        Savedata:function(){
     	    var name =  $("#txtname").val();
               var alias =  $("#txtalias").val();
           
               var description =  $("#txdescription").val();
               var status =  $("#status").is(':checked') ? true : false;
       
      
              var id =  $("#hidId").val();
               $.ajax({
                   type: "POST",
        
                   data:{
                 	  name:name,
                 	  alias:alias,
                 	  description:description,
                 	 status:status,
                 	  id:id
                   },

           		 url : "${pageContext.request.contextPath}/admin/product/createproductcategoty.html",
           		  success: function (data) {
           			  if(data=="success"){
           				   $("#modeladdupdate").modal('hide');
           				    $("#loadimg").show();
           			
                     

                           setTimeout(function () {
                               bootbox.alert("Success Save", function () {
                             	    $("#loadimg").hide();
                                   
                                   productCategoryController.lodata();
                                   productCategoryController.resetform();
                               });
                           }, 1000);
                       } else {

                           bootbox.alert(data);
                       }
           	          productCategoryController.RegisterEvent();
                   }

              });
               
         },


         deleteproductcategory:function(id){
             $.ajax({
                 type: "POST",
     
                 data: { id: id },
                 url: "${pageContext.request.contextPath}/admin/product/deleteproductcategory.html",
                 success: function (data) {
                     if(data=="success"){
         			    $("#loadimg").show();
         			     setTimeout(function () {
                         bootbox.alert("delete success", function () {
                             $("#loadimg").hide();
                            productCategoryController.lodata();
                         });

         			      }, 1000);
                     }
                     else {
                         bootbox.alert(data);
                     }
                     productCategoryController.RegisterEvent();
                 }

             });
         },

         resetform:function(){
             $("#txtname").val("");
             $("#txtalias").val("");
             $("#status").val("");
            $("#txdescription").val("");
            $("#hidId").val(0);
         },
        
         

         loadedit:function(id){
       
                     $.ajax({
                        type:"GET",
                        	headers : {
         					Accept : "application/json; charset=utf-8",
         					"Content-Type" : "application/json; charset=utf-8"
         				},
                            data:{id:id},
                            url:"${pageContext.request.contextPath}/admin/product/details.html",
                            success:function(data){
                            	   var dtailce =  new Date(data.createDate);
                                   var dtailup =  new Date(data.updateDate);
                               if(data==null){
                                   bootbox.alert("not load Product category")
                                   }
                               else{
                              
                             	   $("#txtname").val(data.name);
                                    $("#txtalias").val(data.alias);
                                 
                                   $("#txdescription").val(data.description);
                                   data.status==true ?  $("#status").prop("checked",true): $("#status").prop("checked",false)
                            
                          
                                   $("#hidId").val(data.categoryId);
                                   
                                

                                  
                                   $("#txtnamede").html(data.name);
                                   $("#txtcategoryid").val(data.categoryId)
                                   $("#txtaliasde").html(data.alias);
                                   $("#txdescriptionde").html(data.description);
                                
                                    $("#txtcreatedatede").html(dtailce.getDate() + " / 0" + (dtailce.getMonth()+1) + " / " + dtailce.getFullYear() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dtailce.getHours() + ":" + dtailce.getMinutes());
                                    
                                    $("#txtcreateupde").html(data.createBy);
                                   $("#txtupdatedatede").html(dtailup.getDate() + " / 0" + (dtailup.getMonth()+1) + " / " + dtailup.getFullYear()+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dtailup.getHours() + ":" + dtailup.getMinutes());
                                      $("#txtupdatebyde").html(data.updateBy);
                                     $("#txtstatusde").html(data.status == true ? "<span class='label label-success'>Actived</span>" : "<span class='label label-danger'>Locked</span>");
                                     productCategoryController.RegisterEvent();
                              }
                          }
                            
                                
                    });
             
             },

         
  


       lodata:function(){
    	
    	 var nameca = $("#txtnamesearch").val();
    	   $.ajax({
               type:"GET",
           
           	headers : {
				Accept : "application/json; charset=utf-8",
				"Content-Type" : "application/json; charset=utf-8"
			},
		    data:{nameca:nameca},
			url:"${pageContext.request.contextPath}/admin/product/listca.html",
			success:function(data){


		                 	    var html = "";
				                var templates = $("#data-template").html();
				                $.each(data,function(i,item){
				                      var da =  new Date(item.createDate);
				    
				                    html += Mustache.render(templates, {
				                    id: item.categoryId,
			                        name: item.name,
			                        Datecreate:da,
			                        createby: item.createBy,
			                        status:item.status == true ? "<span class='label label-success'>Actived</span>" : "<span class='label label-danger'>Locked</span>",
			                     
				                    });

					            });

				                $("#tbdada").html(html);

				                productCategoryController.RegisterEvent();
               
		    }


          });
    			 
    	
       }
 }

productCategoryController.init();

</script>













  <section class="content-header">
      <h1>

        Product Category List
     
 
      </h1>
      <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/admin/home.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="${pageContext.request.contextPath}/admin/product/product-category.html">Product Category</a></li>
 
      </ol>
    </section>
    <section class="content">

<br>
<span id="result1"></span>
<div class="row">

        <div class="col-xs-12">
        
          <div class="box">
            <div class="box-header">
         <div class="col-md-5">
                    <input type="text"  class="form-control" id="txtnamesearch" placeholder="Type Name" />
                </div>
  <div class="col-md-3">
                    <button class="btn btn-primary" id="btnseearch">Search</button>
              
                </div>
              <div class="box-tools">
      
                <div class="input-group input-group-sm" style="width: 150px;">
                    <button id="btnaddnew" class="btn btn-success pull-right">Add Product Category</button>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
           
            <div class="box-body table-responsive no-padding">
			
                       <div id="loadimg">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
              <i class="fa fa-refresh fa-4x fa-spin"></i> <label class="label label-primary">loading...</label>
            </div>
            <div class="col-xs-4"></div>
          </div>
              <table class="table table-hover">
             <thead>  <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Create date</th>
                  <th>Create by</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                 <tbody id="tbdada">
           
     
             
           
        
               
             
              </tbody></table>
       
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
              <script id="data-template" type="x-tmpl-mustache">
                <tr>
                    <td>{{id}}</td>
                    <td>{{name}}</td>
               
              
                    <td >{{Datecreate}}</td>
                    <td>{{createby}}</td>
                    <td>{{{status}}}</td>
                    <td> <button class="btn  btn-sm btn-danger btn-delete" data-id="{{id}}"><i class="fa fa-trash"></i> </button> 


  <button class="btn btn-sm btn-primary btn-edit" data-id="{{id}}"><i class="fa fa-pencil"></i></button>
 
           

  <button class="btn btn-sm btn-info btn-info" data-id="{{id}}"><i class="fa fa-info"></i></button>

</td>
                </tr>
            </script>
      </div>
      </section>
      
      
      
      
      
      
      
      
      
      
      
        <div class="modal fade" id="modeladdupdate" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
       
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add/Update Product Category</h4>
            </div>
                <form method="post"  id="frmproductcategory">
            <div class="modal-body">
            
                    <div class="form-group">
                        <label for="txtname">Name:</label>
                        <input type="text" class="form-control" id="txtname" name="name">
                        <input type="hidden" value="0" id="hidId" />
                    </div>
                    <div class="form-group">
                        <label for="txtSalary">Alias:</label>
                        <input type="text" class="form-control" id="txtalias" name="alias">
                    </div>
                <div class="form-group">
                        <label for="txtSalary">Description:</label>
                     
                        <textarea class="form-control" id="txdescription"></textarea>
                    </div>
                    
                         <div class="form-group">
                        <label for="txtSalary">Status:</label>
                        <input type="checkbox" id="status" checked="checked">
               
                    </div>
      
            </div>
            <div class="modal-footer">
                <button  class="btn btn-primary" id="btnSave">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>

    </div>
</div>


  <div class="modal fade" id="modeldetails" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
       
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Product Category details</h4>
            </div>
            
            <div class="modal-body">
            
                    <div class="form-group">
                        <label for="txtname" class="formattile">Name : </label>&nbsp;&nbsp; <span class="fotmatsde"   id="txtnamede"></span>
                    
                     
                      
                    </div>
                    <div class="form-group">
                                 <label for="txtname" class="formattile">Alias : </label> &nbsp;&nbsp; <span class="fotmatsde"  id="txtaliasde"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Create date : </label> &nbsp;&nbsp;  <span class="fotmatsde"  id="txtcreatedatede"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Create By : </label> &nbsp;&nbsp; <span class="fotmatsde"  id="txtcreateupde"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Update date : </label> &nbsp;&nbsp;  <span class="fotmatsde"  id="txtupdatedatede"></span>
                    
                    </div>
                    
                       <div class="form-group">
                                 <label for="txtname" class="formattile">Update by  : </label> &nbsp;&nbsp;  <span class="fotmatsde"   id="txtupdatebyde"></span>
                    
                    </div>
                         <div class="form-group">
                                 <label for="txtname" class="formattile">Status  : </label> &nbsp;&nbsp;  <span class="fotmatsde"  id="txtstatusde"></span>
                    
                    </div>
                <div class="form-group">
                        <label for="txtSalary" class="formattile">Description:</label>&nbsp;&nbsp;  <span class="fotmatsde"  id="txdescriptionde"></span>
                     
                     
                    </div>
      
            </div>
            <div class="modal-footer">
            <input type="hidden" id="txtcategoryid">
         <button type="button" class="btn btn-primary" id="btn-editca" style="margin-right: 440px;">Edit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        
        </div>

    </div>
</div>