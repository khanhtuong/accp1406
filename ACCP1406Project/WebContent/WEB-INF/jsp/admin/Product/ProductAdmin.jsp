<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.6.2.js">
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.4.2.min.js">
 </script> 


<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-ui-1.8.2.custom.min.js">
 </script> 
 <link href="${pageContext.request.contextPath}/assets/admin/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

 

 <style>

.fotmatsde{
  font-size: 17px;
  font-weight: 800px;
  font-family: sans-serif;
 
}

.formattile{
   color: blue;;
}

</style>   
    
    
    <script type="text/javascript">

$(document).ready(function(){

    $("#txtnameprsearch").autocomplete({
        source: '${pageContext.request.contextPath}/admin/product/searchpr.html'

     });

    $("#loadimg").hide();





});

var homeconfig = {

	    pagesize: 7,
	    pageIndex:1
}

var productController = {

		init:function(){


	
		
			productController.lodata(true);
			productController.RegisterEvent();
	   },

    RegisterEvent:function(){



        $("#btnseearch").off('click').on('click',function(e){
    	    e.preventDefault();

    	    if($.trim($("#txtnameprsearch").val())!=''){
    	    	productController.searchpr();
  	
              }else{
                  window.location.assign("${pageContext.request.contextPath}/admin/product/product.html")
              }
      	  
      
         
    	
         });
        $("#resetload").off('click').on('click',function(e){
    	    e.preventDefault();
            window.location.assign("${pageContext.request.contextPath}/admin/product/product.html")
         });
      

        $(".btn-delete").off('click').on('click',function(e){
            
    	    e.preventDefault();
    	    var id  =  $(this).data('id');
    	     bootbox.confirm("Are you sure to delete this Product " +id, function (result) {
	              if (result) {
	            	  productController.deleteproduct(id);
 
	              }

	           
	          });
   	    
         
             
               
        });


        $(".btn-info").off('click').on('click',function(e){

            e.preventDefault();
            var id  =  $(this).data('id');
            $("#modeldetails").modal('show');
            productController.loadedit(id);
        });

        $("#btn-editpr").off('click').on('click',function(e){   
            e.preventDefault();
            var id  = $("#txproductid").val();
            window.location.assign("${pageContext.request.contextPath}/admin/product/edit-product/"+id+".html")
           });

    },

    loadedit:function(id){
        
        $.ajax({
           type:"GET",
           	headers : {
				Accept : "application/json; charset=utf-8",
				"Content-Type" : "application/json; charset=utf-8"
			},
               data:{id:id},
               url:"${pageContext.request.contextPath}/admin/product/detailsprouctadmin.html",
               success:function(data){
               	   var dtailce =  new Date(data.createDate);
                      var dtailup =  new Date(data.updateDate);
                  if(data==null){
                      bootbox.alert("not load Product category")
                      }
                  else{
                 
               
                      
                   

                     
                      $("#txtnameproduct").html(data.name);
                      $("#txtprice").html(data.price)
                      $("#txtoriginalprice").html(data.originalPrice);
                      $("#txtpromotionprice").html(data.promotionPrice);
                      $("#txtquantity").html(data.quantity);
                      $("#txtphoto").html("<img src='${pageContext.request.contextPath}/assets/admin/img/"+data.photo+"' width='90' height='60'/>" );
                      $("#txtsupplier").html(data.supplierview.name);
                      $("#txtcategory").html(data.categoryview.name);
                       $("#txdcreatedate").html(dtailce.getDate() + " / " + (dtailce.getMonth()+1) + " / " + dtailce.getFullYear() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dtailce.getHours() + ":" + dtailce.getMinutes());
                       
                       $("#txdcreateby").html(data.createBy);
                      $("#txdupdatedate").html(dtailup.getDate() + " / " + (dtailup.getMonth()+1) + " / " + dtailup.getFullYear()+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dtailup.getHours() + ":" + dtailup.getMinutes());
                         $("#txdupdateby").html(data.updateBy);

                          $("#txdstatus").html(data.status == true ? "<span class='label label-success'>Actived</span>" : "<span class='label label-danger'>Locked</span>");
                          $("#txdescriptionde").html(data.description);
                        $("#txproductid").val(data.productId);
                        productController.RegisterEvent();
                 }
             }
               
                   
       });

},



    deleteproduct:function(id){
        $.ajax({
            type: "POST",

            data: { id: id },
            url: "${pageContext.request.contextPath}/admin/product/deleteproduct.html",
            success: function (data) {
                if(data=="success"){
    			
    			     setTimeout(function () {
                    bootbox.alert("delete success", function () {
                  
                        productController.lodata(true);
                        window.location.assign("${pageContext.request.contextPath}/admin/product/product.html")
                    });

    			      }, 500);
                }
                else {
                    bootbox.alert(data);
                }
            }

        });
    },


       lodata:function(changePageSize){



    	   $.ajax({
               type:"GET",
           
           	headers : {
				Accept : "application/json; charset=utf-8",
				"Content-Type" : "application/json; charset=utf-8"
			},
		    data:{page: homeconfig.pageIndex,
	              pagesize:homeconfig.pagesize},
			url:"${pageContext.request.contextPath}/admin/product/listpr.html",
			success:function(data){


		                 	    var html = "";
				                var templates = $("#data-template").html();
				                $.each(data,function(i,item){
				           
				    
				                    html += Mustache.render(templates, {
				                    id: item.productId,
			                        name: item.name,
			                        photo: "<img src='${pageContext.request.contextPath}/assets/admin/img/"+item.photo+"' width='90' height='50'>",
			                        price: "$"+item.price,
			                        originalPrice:item.originalPrice,
			                        quantity:item.quantity
				                    });

					            });

				                $("#tbdada").html(html);
				                productController.paging($("#totalproduct").val(), function () {

				                	 productController.lodata();
			                    }, changePageSize);

				                productController.RegisterEvent();
               
		    }


          });
    			 
    	
       },

       searchpr:function(){
      	 var namepr = $("#txtnameprsearch").val();
  	   $.ajax({
           type:"GET",
       
       	headers : {
			Accept : "application/json; charset=utf-8",
			"Content-Type" : "application/json; charset=utf-8"
		},
	    data:{namepr:namepr},
		url:"${pageContext.request.contextPath}/admin/product/searlistpr.html",
		success:function(data){


	                 	    var html = "";
			                var templates = $("#data-template").html();
			                $.each(data,function(i,item){
			           
			    
			                    html += Mustache.render(templates, {
			                    id: item.productId,
		                        name: item.name,
		                        photo: "<img src='${pageContext.request.contextPath}/assets/admin/img/"+item.photo+"' width='90' height='50'>",
		                        price: "$"+item.price,
		                        originalPrice:item.originalPrice,
		                        quantity:item.quantity
			                    });

				            });

			                $("#tbdada").html(html);
			                productController.paging(0, function () {

			                	 productController.lodata();
		                    }, true);

			                productController.RegisterEvent();
           
	    }


      });
           },


       paging: function (totalrow, callback,changePageSize) {

           var totalpage = Math.ceil(totalrow / homeconfig.pagesize);

           if ($("#paginationdata a").length === 0 || changePageSize===true) {
               $("#paginationdata").empty();
               $("#paginationdata").removeData("twbs-pagination");
               $("#paginationdata").unbind("page");
           }

           $("#paginationdata").twbsPagination({
               totalPages: totalpage,
               first: "first",
               prev: "prev",
               next: "next",
               last: "last",
               visiblePages: 7,
               onPageClick: function (event, page) {
                   homeconfig.pageIndex= page;
                   setTimeout(callback, 200);
               }




           });
       }


 }

productController.init();

</script>













  <section class="content-header">
      <h1>

        Product Information
     
 
      </h1>
      <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/admin/home.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="${pageContext.request.contextPath}/admin/product/product.html">Product</a></li>
 
      </ol>
    </section>
    <section class="content">
<input type="hidden" id="totalproduct" value="${totalproduct}">
<br>
<span id="result1"></span>
<div class="row">

        <div class="col-xs-12">
        
          <div class="box">
            <div class="box-header">
         <div class="col-md-5">
                    <input type="text"  class="form-control" id="txtnameprsearch"  placeholder="Type Name" />
                </div>
  <div class="col-md-3">
                    <button class="btn btn-primary" id="btnseearch">Search</button>
                 
                </div>
              <div class="box-tools">
      
                <div class="input-group input-group-sm" style="width: 150px;">
                    <a  href="${pageContext.request.contextPath}/admin/product/add-product-admin.html" class="btn btn-success pull-right">Add Product</a>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
           
            <div class="box-body table-responsive no-padding">
			
                       <div id="loadimg">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
              <i class="fa fa-refresh fa-4x fa-spin"></i> <label class="label label-primary">loading...</label>
            </div>
            <div class="col-xs-4"></div>
          </div>
              <table class="table table-hover">
             <thead>  <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Photo</th>
                  <th>Price</th>
                  <th>OriginalPrice</th>
                  <th>Quantity</th>
                  <th>Action</th>
                </tr>
                </thead>
                 <tbody id="tbdada">
           
     
             
           
        
               
             
              </tbody></table>
         <div class="pagination" id="paginationdata">

            </div>
            </div>
            

            
   
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
              <script id="data-template" type="x-tmpl-mustache">
                <tr>
                    <td>{{id}}</td>
                    <td>{{name}}</td>
               
              
                    <td>{{{photo}}}</td>
                    <td>{{{price}}}</td>
                    <td> {{originalPrice}}


</td>
<td>{{quantity}}</td>
<td><button class="btn  btn-sm btn-danger btn-delete" data-id="{{id}}"><i class="fa fa-trash"></i> </button>

  <a href="${pageContext.request.contextPath}/admin/product/edit-product/{{id}}.html" class="btn btn-sm btn-primary" ><i class="fa fa-pencil"></i></a>
  <button class="btn btn-sm btn-info btn-info" data-id="{{id}}"><i class="fa fa-info"></i></button>
 </td>
                </tr>
            </script>
      </div>
      </section>
      
      
      
      
      
      
      
        <div class="modal fade" id="modeldetails" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
       
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Product Category details</h4>
            </div>
            
            <div class="modal-body">
            
                    <div class="form-group">
                        <label for="txtname" class="formattile">Name Product : </label>&nbsp;&nbsp; <span class="fotmatsde"   id="txtnameproduct"></span>
                    
                     
                      
                    </div>
                    <div class="form-group">
                                 <label for="txtname" class="formattile">Price : </label> &nbsp;&nbsp; <span class="fotmatsde"  id="txtprice"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Original Price: </label> &nbsp;&nbsp;  <span class="fotmatsde"  id="txtoriginalprice"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Promotion Price: </label> &nbsp;&nbsp; <span class="fotmatsde"  id="txtpromotionprice"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Quantity : </label> &nbsp;&nbsp;  <span class="fotmatsde"  id="txtquantity"></span>
                    
                    </div>
                    
                       <div class="form-group">
                                 <label for="txtname" class="formattile">Photo  : </label> &nbsp;&nbsp;  <span class="fotmatsde"   id="txtphoto"></span>
                    
                    </div>
                         <div class="form-group">
                                 <label for="txtname" class="formattile">Category : </label> &nbsp;&nbsp;  <span class="fotmatsde"  id="txtcategory"></span>
                    
                    </div>
                    
                            <div class="form-group">
                                 <label for="txtname" class="formattile">Supplier: </label> &nbsp;&nbsp;  <span class="fotmatsde"  id="txtsupplier"></span>
                    
                    </div>
                <div class="form-group">
                        <label for="txtSalary" class="formattile">CreateDate:</label>&nbsp;&nbsp;  <span class="fotmatsde"  id="txdcreatedate"></span>
                     
                     
                    </div>
                         <div class="form-group">
                        <label for="txtSalary" class="formattile">CreateBy:</label>&nbsp;&nbsp;  <span class="fotmatsde"  id="txdcreateby"></span>
                     
                     
                    </div>
                         <div class="form-group">
                        <label for="txtSalary" class="formattile">UpdateDate:</label>&nbsp;&nbsp;  <span class="fotmatsde"  id="txdupdatedate"></span>
                     
                     
                    </div>
                         <div class="form-group">
                        <label for="txtSalary" class="formattile">UpdateBy:</label>&nbsp;&nbsp;  <span class="fotmatsde"  id="txdupdateby"></span>
                     
                     
                    </div>
                    
                            <div class="form-group">
                        <label for="txtSalary" class="formattile">Status:</label>&nbsp;&nbsp;  <span class="fotmatsde"  id="txdstatus"></span>
                     
                     
                    </div>
                               <div class="form-group">
                        <label for="txtSalary" class="formattile">Description:</label>&nbsp;&nbsp;  <span class="fotmatsde"  id="txdescriptionde"></span>
                     
                     
                    </div>
      
            </div>
            <div class="modal-footer">
        <input type="hidden" id="txproductid">
       <button type="button" class="btn btn-primary" id="btn-editpr" style="margin-right: 440px;">Edit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        
        </div>

    </div>
</div>
      
      
      
        
