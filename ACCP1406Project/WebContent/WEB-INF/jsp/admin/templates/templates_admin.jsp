<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${title}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
 <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/admin/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/admin/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/admin/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/admin/css/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/admin/css/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/admin/css/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/admin/css/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/admin/css/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/admin/css/bootstrap3-wysihtml5.min.css">

	  <link href="${pageContext.request.contextPath}/assets/admin/css/style.css" rel="stylesheet" type="text/css"/>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  
  <header class="main-header">
    <!-- Logo -->
   
    <a href="${pageContext.request.contextPath}/admin/home.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Online</b> Book Store</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
   
  
     
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="${pageContext.request.contextPath}/assets/admin/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">${sessionScope.username}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="${pageContext.request.contextPath}/assets/admin/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
              ${sessionScope.username}
                  <small>Member since <fmt:formatDate value="${sessionScope.createdate}" pattern="yyyy/MM/dd"/> </small>
                </p>
              </li>
              <!-- Menu Body -->
      
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="${pageContext.request.contextPath}/admin/account/profile.html" class="btn btn-default btn-flat">Profile</a>
                </div>
              
                
                <div class="pull-right">
                  <a href="${pageContext.request.contextPath}/admin/lougout.html" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="${pageContext.request.contextPath}/assets/admin/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>       ${sessionScope.username}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
   
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
  
        <li class="active treeview">
           
          <a href="${pageContext.request.contextPath}/admin/home.html">
            <i class="fa fa-dashboard"></i> <span>Home</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
     
        </li>
        
                <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Information</span>
        
          </a>
          <ul class="treeview-menu">
            
            <li><a href="${pageContext.request.contextPath}/admin/information/feedback.html"><i class="fa fa-circle-o"></i>Feedback Customer</a></li>
   
          </ul>
        <ul class="treeview-menu">
      
            <li><a href="${pageContext.request.contextPath}/admin/information/managed-orders.html"><i class="fa fa-circle-o"></i>Managed Order</a></li>
   
          </ul>
       
                <ul class="treeview-menu">
                    
            <li><a href="${pageContext.request.contextPath}/admin/information/revenues.html"><i class="fa fa-circle-o"></i>Revenues</a></li>
   
          </ul>
          
                 <ul class="treeview-menu">
                      
            <li><a href="${pageContext.request.contextPath}/admin/information/payment.html"><i class="fa fa-circle-o"></i>Payment methods</a></li>
   
          </ul>
          
                 <ul class="treeview-menu">
                      
            <li><a href="${pageContext.request.contextPath}/admin/information/tranfer.html"><i class="fa fa-circle-o"></i>Transfer</a></li>
   
          </ul>
        </li>
        
        
        
        
        
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Product</span>
        
          </a>
          <ul class="treeview-menu">
              
            <li><a href="${pageContext.request.contextPath}/admin/product/product-category.html"><i class="fa fa-circle-o"></i>Product Category</a></li>
   
          </ul>
        <ul class="treeview-menu">
          
            <li><a href="${pageContext.request.contextPath}/admin/product/product.html"><i class="fa fa-circle-o"></i>Product</a></li>
   
          </ul>
            <ul class="treeview-menu">
           
            <li><a href="${pageContext.request.contextPath}/admin/product/product-supplier.html"><i class="fa fa-circle-o"></i>Product Supplier</a></li>
   
          </ul>
        </li>
    
            <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Account</span>
        
          </a>
          <ul class="treeview-menu">
              
            <li><a href="${pageContext.request.contextPath}/admin/account/user.html"><i class="fa fa-circle-o"></i>Account user List</a></li>
   
          </ul>
             <ul class="treeview-menu">
              
            <li><a href="${pageContext.request.contextPath}/admin/account/admin.html"><i class="fa fa-circle-o"></i>Account admin List</a></li>
   
          </ul>
    
        </li>
             <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Setting</span>
        
          </a>
          <ul class="treeview-menu">
             
            <li><a href="${pageContext.request.contextPath}/admin/account/changepasswordadmin.html"><i class="fa fa-circle-o"></i>
Change Password</a></li>
   
          </ul>
             <ul class="treeview-menu">
   
            <li><a href="${pageContext.request.contextPath}/admin/account/profile.html"><i class="fa fa-circle-o"></i>Profile</a></li>
 
 
          </ul>
    
        </li>
    
     
       </ul>
    
    
   
  
  
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
 

    <!-- Main content -->
 
      <!-- Small boxes (Stat box) -->
  <tiles:insertAttribute name="content"></tiles:insertAttribute>

      <!-- /.row (main row) -->

  
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.6
    </div>
    <strong>Copyright &copy; 2014-2016 .</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->

<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/respond.js">
</script>

<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/respond.min.js">
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 


 <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>  



 
 
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

<script src="${pageContext.request.contextPath}/assets/admin/js/bootstrap.min.js"></script>

<!-- Sparkline -->
<script src="${pageContext.request.contextPath}/assets/admin/js/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="${pageContext.request.contextPath}/assets/admin/js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/admin/js/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="${pageContext.request.contextPath}/assets/admin/js/jquery.knob.js"></script>
<!-- daterangepicker -->

<script src="${pageContext.request.contextPath}/assets/admin/js/daterangepicker.js"></script>
<!-- datepicker -->
<script src="${pageContext.request.contextPath}/assets/admin/js/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->

<!-- Slimscroll -->
<script src="${pageContext.request.contextPath}/assets/admin/js/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/assets/admin/js/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/assets/admin/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="${pageContext.request.contextPath}/assets/admin/js/pages/dashboard.js"></script>





    <script src="${pageContext.request.contextPath}/assets/admin/js/jquery.validate.js"></script>

  
	<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/additional-methods.min.js">
	</script>
	
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/mustache.min.js">
</script>



<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/bootbox.min.js">
</script>

   <script src="${pageContext.request.contextPath}/assets/admin/js/jquery.twbsPagination.min.js"></script>


<!-- AdminLTE for demo purposes -->








</body>
</html>
