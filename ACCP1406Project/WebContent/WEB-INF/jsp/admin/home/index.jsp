<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<section class="content-header">
      <h1>
      ${username }
  
      </h1>
      <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/admin/home.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Book Online</li>
      </ol>
    </section>
    
    
    
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Revenue</span>
              <span class="info-box-number">$${totalreven}<small></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">FeedBack</span>
              <span class="info-box-number">${countfeedback}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Benefit</span>
              <span class="info-box-number">$${totalBenefit}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Count Members</span>
              <span class="info-box-number">${countaccount}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

<div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Static Website</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tbody><tr>
                  <th style="width: 10px">#</th>
             
                  <th>Name</th>
                   <th>Amount</th>
             
           
                </tr>
                <tr>
                  <td>1.</td>
                  <td>Total Orders</td>
                  <td>
                   ${totalorder }
                  </td>
            
                </tr>
            
                 <tr>
                  <td>2.</td>
                  <td>Total Product</td>
                  <td>
                   ${totalproduct }
                  </td>
            
                </tr>
                     <tr>
                  <td>3.</td>
                  <td>Total Product Category</td>
                  <td>
                   ${totalcategory}
                  </td>
            
                </tr>
                     <tr>
                  <td>4.</td>
                  <td>Total Account</td>
                  <td>
                   ${totalaccount}
                  </td>
            
                </tr>
                         <tr>
                  <td>5.</td>
                  <td>Total Supplier</td>
                  <td>
                   ${totalsuppliermodel}
                  </td>
            
                </tr>
                         <tr>
                  <td>6.</td>
                  <td>Total Payment</td>
                  <td>
                   ${totalpayment}
                  </td>
            
                </tr>
              </tbody></table>
            </div>
            <!-- /.box-body -->
     
          </div>

      <!-- /.row -->
    </section>