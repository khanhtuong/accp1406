<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.6.2.js">
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.4.2.min.js">
 </script> 


<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-ui-1.8.2.custom.min.js">
 </script> 
 <link href="${pageContext.request.contextPath}/assets/admin/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


<style>

.fotmatsde{
  font-size: 17px;
  font-weight: 800px;
  font-family: sans-serif;
 
}

.formattile{
   color: blue;;
}

</style>



<script type="text/javascript">

$(document).ready(function(){

    $("#keywowrd").autocomplete({
        source: '${pageContext.request.contextPath}/admin/information/searchfeed.html'

     });

    $("#loadimg").hide();




});



var feedbackController = {

		init:function(){


	
		
			feedbackController.lodata();
			feedbackController.RegisterEvent();
	   },

    RegisterEvent:function(){
        $("#btnseearch").off('click').on('click',function(e){
    	    e.preventDefault();
//            if($.trim($("#keywowrd").val())!=''){
//          	  feedbackController.lodata();
	
//             }else{
//                 window.location.assign("${pageContext.request.contextPath}/admin/information/feedback.html")
//             }


     	  feedbackController.lodata();
    	  
         });


        $(".btn-info").off('click').on('click',function(e){
    	    e.preventDefault();
    	    var id =  $(this).data('id');
    	       $("#modeldetails").modal('show');
    		feedbackController.loadedit(id);
        });


        $(".btn-delete").off('click').on('click',function(e){
        	   e.preventDefault();
       	    var id =  $(this).data('id');

            bootbox.confirm("Are you sure to delete this feedback back?", function (result) {
                if (result) {
            		feedbackController.deletefeedbackadmin(id);
                }

             
            });
    
         });
        
        },



         resetform:function(){
             $("#txtname").val("");
             $("#txtalias").val("");
             $("#status").val("");
            $("#txdescription").val("");
            $("#hidId").val(0);
         },
        
         
         deletefeedbackadmin:function(id){
             $.ajax({
                 type: "POST",
     
                 data: { id: id },
                 url: "${pageContext.request.contextPath}/admin/information/deletefeedbackadmin.html",
                 success: function (data) {
                     if(data=="success"){
         			    $("#loadimg").show();
         			     setTimeout(function () {
                         bootbox.alert("delete success", function () {
                             $("#loadimg").hide();
                         	feedbackController.lodata();
                         });

         			      }, 700);
                     }
                     else {
                         bootbox.alert(data);
                     }
                 	feedbackController.RegisterEvent();
                 }

             });
         },
         

         loadedit:function(id){
       
                     $.ajax({
                        type:"GET",
                        	headers : {
         					Accept : "application/json; charset=utf-8",
         					"Content-Type" : "application/json; charset=utf-8"
         				},
                            data:{id:id},
                            url:"${pageContext.request.contextPath}/admin/information/detailsfeedback.html",
                            success:function(data){
                            	   var dtailce =  new Date(data.createDate);
                             
                               if(data==null){
                                   bootbox.alert("not load Product category")
                                   }
                               else{
                              
                          
                                   
                                

                                  
                                   $("#txtfullname").html(data.fullName);
                                   $("#txtphone").html(data.phone);
                                   $("#txtemail").html(data.email);
                                
                                    $("#txtcreatedate").html(dtailce.getDate() + " / " + (dtailce.getMonth()+1) + " / " + dtailce.getFullYear());
                                    
                                    $("#txttitle").html(data.title);
                                   $("#txtcontent").html(data.content);
                                 
                                    
                             	
                              }

                           	feedbackController.RegisterEvent();
                          }
                            
                                
                    });
             
             },


       lodata:function(){
    	
    	 var keywowrd = $("#keywowrd").val();
    	   $.ajax({
               type:"GET",
           
           	headers : {
				Accept : "application/json; charset=utf-8",
				"Content-Type" : "application/json; charset=utf-8"
			},
		    data:{keywowrd :keywowrd },
			url:"${pageContext.request.contextPath}/admin/information/listfeedback.html",
			success:function(data){


		                 	    var html = "";
				                var templates = $("#data-template").html();
				                $.each(data,function(i,item){
				                      var da =  new Date(item.createDate);
				    
				                    html += Mustache.render(templates, {
				                    id: item.feedBackId,
				                    fullname: item.fullName,
				                    title:item.title,
				                    content: item.content,
				                    createDate:da
			                     
				                    });

					            });

				                $("#tbdada").html(html);

				                feedbackController.RegisterEvent();
               
		    }


          });
    			 
    	
       }
 }

feedbackController.init();

</script>




  <section class="content-header">
      <h1>

      Information Feedback
     
 
      </h1>
      <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/admin/home.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="${pageContext.request.contextPath}/admin/information/feedback.html">Feedback</a></li>
 
      </ol>
    </section>
    <section class="content">

<br>
<span id="result1"></span>
<div class="row">

        <div class="col-xs-12">
        
          <div class="box">
            <div class="box-header">
         <div class="col-md-5">
                    <input type="text"  class="form-control" id="keywowrd" placeholder="Type Name" />
                </div>
  <div class="col-md-3">
                    <button class="btn btn-primary" id="btnseearch">Search</button>
              
                </div>
          
            </div>
            <!-- /.box-header -->
           
            <div class="box-body table-responsive no-padding">
			
                       <div id="loadimg">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
              <i class="fa fa-refresh fa-4x fa-spin"></i> <label class="label label-primary">loading...</label>
            </div>
            <div class="col-xs-4"></div>
          </div>
              <table class="table table-hover">
             <thead>  <tr>
                  <th>ID</th>
                  <th>Full Name</th>
                  <th>title</th>
                  <th>content</th>
                  <th>create Date</th>
                  <th></th>
                </tr>
                </thead>
                 <tbody id="tbdada">
           
     
             
           
        
               
             
              </tbody></table>
       
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
              <script id="data-template" type="x-tmpl-mustache">
                <tr>
                    <td>{{id}}</td>
                    <td>{{fullname}}</td>
               
              
                    <td >{{title}}</td>
                    <td>{{content}}</td>
                    <td>{{createDate}}</td>
                    <td> <button class="btn  btn-sm btn-danger btn-delete" data-id="{{id}}"><i class="fa fa-trash"></i> </button> 



 
           

  <button class="btn btn-sm btn-info btn-info" data-id="{{id}}"><i class="fa fa-info"></i></button>

</td>
                </tr>
            </script>
      </div>
      </section>
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
        <div class="modal fade" id="modeldetails" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
       
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Product Category details</h4>
            </div>
            
            <div class="modal-body">
            
                    <div class="form-group">
                        <label for="txtname" class="formattile">Full Name : </label>&nbsp;&nbsp; <span class="fotmatsde"   id="txtfullname"></span>
                    
                     
                      
                    </div>
                    <div class="form-group">
                                 <label for="txtname" class="formattile">Phone : </label> &nbsp;&nbsp; <span class="fotmatsde"  id="txtphone"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Email : </label> &nbsp;&nbsp;  <span class="fotmatsde"  id="txtemail"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Create date: </label> &nbsp;&nbsp; <span class="fotmatsde"  id="txtcreatedate"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Title  : </label> &nbsp;&nbsp;  <span class="fotmatsde"  id="txttitle"></span>
                    
                    </div>
                    
                       <div class="form-group">
                                 <label for="txtname" class="formattile">Content  : </label> &nbsp;&nbsp;  <span class="fotmatsde"   id="txtcontent"></span>
                    
                    </div>
         
      
            </div>
            <div class="modal-footer">


                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        
        </div>

    </div>
</div>
      
      