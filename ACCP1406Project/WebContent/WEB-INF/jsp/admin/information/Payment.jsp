<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.6.2.js">
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.4.2.min.js">
 </script> 


<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-ui-1.8.2.custom.min.js">
 </script> 
 <link href="${pageContext.request.contextPath}/assets/admin/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>




<script type="text/javascript">

$(document).ready(function(){

    $("#loadimg").hide();




});



var PaymentController = {

		init:function(){


	
		
			PaymentController.lodata();
			PaymentController.RegisterEvent();
	   },

    RegisterEvent:function(){



    	  $('#frmpaymentmethods').validate({
              rules: {
                  namepayment:{
                	    required: true,
                	     minlength: 3,
                         maxlength: 250
                   }
              
               
              },
              messages: {
                
            	  namepayment: {
                      required: "you must enter Payment name",
                      minlength: "Name Payment must great than 3",
                      maxlength:"Name Payment must less than 250"
                  }
              
              }

          });
    	

        
	    $("#btnaddnew").off('click').on('click',function(e){
    	    e.preventDefault();
          $("#modeladdupdate").modal('show');
          PaymentController.resetform();
       });


	    $("#btnSave").off('click').on('click', function (e) {
   	     e.preventDefault();
            var isValid = $('#frmpaymentmethods').valid();
            if(isValid){
            	  PaymentController.Savedata();
            }
           
           });




	      $(".btn-edit").off('click').on('click',function(e){
	    	   e.preventDefault();

	    	    var id = $(this).data('id');
	    		   PaymentController.loadedit(id);
	           $("#modeladdupdate").modal('show');
	       });

	       $(".btn-delete").off('click').on('click',function(e){
	    	   e.preventDefault();
	   	       var id = $(this).data('id');
	   	       PaymentController.deletepaymentadmin(id);
	       });

        
        },



     

        resetform:function(){
            $("#txtnamepayment").val("");
 
           $("#hidId").val(0);
        },





        loadedit:function(id){
      
                    $.ajax({
                       type:"GET",
                       	headers : {
        					Accept : "application/json; charset=utf-8",
        					"Content-Type" : "application/json; charset=utf-8"
        				},
                           data:{id:id},
                           url:"${pageContext.request.contextPath}/admin/information/detailspayment.html",
                           success:function(data){
                 	
                              if(data==null){
                                  bootbox.alert("not load Payment methods")
                                  }
                              else{
                             
                            	   $("#txtnamepayment").val(data.paymentName);
                           
                         
                                  $("#hidId").val(data.paymentId);
                                  
                               

                                 
                               
                             }
                         }
                           
                               
                   });
            
            },



            deletepaymentadmin:function(id){
                $.ajax({
                    type: "POST",
        
                    data: { id: id },
                    url: "${pageContext.request.contextPath}/admin/information/deletepaymentadmin.html",
                    success: function (data) {
                        if(data=="success"){
            			    $("#loadimg").show();
            			     setTimeout(function () {
                            bootbox.alert("delete success", function () {
                                $("#loadimg").hide();
                                PaymentController.lodata();
                            });

            			      }, 700);
                        }
                        else {
                            bootbox.alert(data);
                        }
                        PaymentController.RegisterEvent();
                    }

                });
            },
            




        
        Savedata:function(){
     	    var namepayment =  $("#txtnamepayment").val();
     	      
            var id =  $("#hidId").val();

       

               $.ajax({
                   type: "POST",
        
                   data:{
                	   namepayment:namepayment,
                	   id:id,
                 	
                   },

           		 url : "${pageContext.request.contextPath}/admin/information/createpaymentmethods.html",
           		  success: function (data) {
           			  if(data=="success"){
           				   $("#modeladdupdate").modal('hide');
           				    $("#loadimg").show();
           			
                     

                           setTimeout(function () {
                               bootbox.alert("Success Save", function () {
                             	    $("#loadimg").hide();
                                   
                  
                             	  PaymentController.resetform();
                             	 PaymentController.lodata();
                               });
                           }, 1000);
                       } else {

                           bootbox.alert(data);
                       }
           			PaymentController.RegisterEvent();
                   }

              });
               
         },

       lodata:function(){
    	
  
    	   $.ajax({
               type:"GET",
           
           	headers : {
				Accept : "application/json; charset=utf-8",
				"Content-Type" : "application/json; charset=utf-8"
			},
			url:"${pageContext.request.contextPath}/admin/information/listpayment.html",
			success:function(data){


		                 	    var html = "";
				                var templates = $("#data-template").html();
				                $.each(data,function(i,item){
				   
				    
				                    html += Mustache.render(templates, {
				                    id: item.paymentId,
				                    paymentname: item.paymentName,
				                 
			                     
				                    });

					            });

				                $("#tbdada").html(html);

				                PaymentController.RegisterEvent();
               
		    }


          });
    			 
    	
       }
 }

PaymentController.init();

</script>











 <section class="content-header">
      <h1>

      Information Payment methods
     
 
      </h1>
      <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/admin/home.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="${pageContext.request.contextPath}/admin/information/payment.html">Payment methods</a></li>
 
      </ol>
    </section>
    <section class="content">

<br>
<span id="result1"></span>
<div class="row">

        <div class="col-xs-12">
         
          <div class="box">
            <div class="box-header">
 
              <div class="box-tools">
      
                <div class="input-group input-group-sm" style="width: 150px;">
                    <button id="btnaddnew" class="btn btn-success pull-right">Add Payment</button>
                </div>
              </div>
         
            </div>
            <!-- /.box-header -->
           
            <div class="box-body table-responsive no-padding">
			
                       <div id="loadimg">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
              <i class="fa fa-refresh fa-4x fa-spin"></i> <label class="label label-primary">loading...</label>
            </div>
            <div class="col-xs-4"></div>
          </div>
              <table class="table table-hover">
             <thead>  <tr>
                  <th>ID</th>
                  <th>Payment Name</th>
            
                  <th>Action</th>
                </tr>
                </thead>
                 <tbody id="tbdada">
           
     
             
           
        
               
             
              </tbody></table>
       
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
              <script id="data-template" type="x-tmpl-mustache">
                <tr>
                    <td>{{id}}</td>
                    <td>{{paymentname}}</td>
               
              
              
                    <td> <button class="btn  btn-sm btn-danger btn-delete" data-id="{{id}}"><i class="fa fa-trash"></i> </button> 



 
  <button class="btn btn-sm btn-primary btn-edit" data-id="{{id}}"><i class="fa fa-pencil"></i></button>

</td>
                </tr>
            </script>
      </div>
      </section>
      
      
      
      
              <div class="modal fade" id="modeladdupdate" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
       
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add/Update Payment methods</h4>
            </div>
                <form method="post"  id="frmpaymentmethods">
            <div class="modal-body">
            
                    <div class="form-group">
                        <label for="txtname">Payment Name:</label>
                        <input type="text" class="form-control" id="txtnamepayment" name="namepayment">
                        <input type="hidden" value="0" id="hidId" />
                    </div>
                
                 
      
            </div>
            <div class="modal-footer">
                <button  class="btn btn-primary" id="btnSave">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>

    </div>
</div>
      