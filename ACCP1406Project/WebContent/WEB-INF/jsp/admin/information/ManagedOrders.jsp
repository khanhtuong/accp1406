<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.6.2.js">
 </script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.4.2.min.js">
 </script>  


<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-ui-1.8.2.custom.min.js">
 </script> 
 <link href="${pageContext.request.contextPath}/assets/admin/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


<script type="text/javascript">

$(document).ready(function(){

    $("#keywowrd").autocomplete({

        source: '${pageContext.request.contextPath}/admin/information/searchorders.html'
     });



    $("#loadimg").hide();




});
var homeconfig = {

	    pagesize: 7,
	    pageIndex:1
}


var ordersController = {

		init:function(){


	
		
			ordersController.lodata(true);
			ordersController.RegisterEvent();
	   },

    RegisterEvent:function(){
        $("#btnseearch").off('click').on('click',function(e){
    	    e.preventDefault();
    	      if($.trim($("#keywowrd").val())==''){
    	          ordersController.lodata(true);
    	    	
    	      }else{
    	    	  ordersController.searchor();
        	  }
    	    
         });


        $(".btn-delete").off('click').on('click',function(e){

      	    e.preventDefault();
      	   var id =  $(this).data('id');
   


      	    bootbox.confirm("Are you sure to delete this Orders " + id, function (result) {
	              if (result) {
	              	   ordersController.deleteorderadmin(id);

	              }

	           
	          });
      	   
        });


        
        },


        deleteorderadmin:function(id){
               

            
            $.ajax({
                type: "POST",
    
                data: { id: id },
                url: "${pageContext.request.contextPath}/admin/information/deleteorder.html",
                success: function (data) {
                    if(data=="success"){
        			    $("#loadimg").show();
        			     setTimeout(function () {
                        bootbox.alert("delete success", function () {
                            $("#loadimg").hide();
                            ordersController.lodata(true);
                         
                        });

        			      }, 700);
                    }
                    else {
                        bootbox.alert(data);
                    }
                    ordersController.RegisterEvent();
                }

            });
        },

       lodata:function(changePageSize){
    

    	   $.ajax({
               type:"GET",
           
           	headers : {
				Accept : "application/json; charset=utf-8",
				"Content-Type" : "application/json; charset=utf-8"
			},
		    data:{page: homeconfig.pageIndex,
	              pagesize:homeconfig.pagesize},
			url:"${pageContext.request.contextPath}/admin/information/listorders.html",
			success:function(data){


		                 	    var html = "";
				                var templates = $("#data-template").html();
				                $.each(data,function(i,item){
				                      var da =  new Date(item.datecreation);
				    
				                    html += Mustache.render(templates, {
				                    id: item.ordersId,
				                    totalmoney: "$"+item.totalmoney,
				                    datecreate:da,
				                    status:item.status==false?"<span class='label label-success'>Payment  already</span>" : "<span class='label label-danger'> Payment Yet</span>",
				                    Username: item.username,
				                    customername: item.customerName,
				                    customerphone: item.customerMobile,
				           
				                   
			                     
				                    });

					            });

				                $("#tbdada").html(html);
				                ordersController.paging($("#totalorrder").val(), function () {

				                	ordersController.lodata();
			                    }, changePageSize);

				                ordersController.RegisterEvent();
               
		    }


          });
    			 
    	
       },
       searchor:function(){
        	 var keywordor = $("#keywowrd").val();
    	   $.ajax({
             type:"GET",
         
         	headers : {
  			Accept : "application/json; charset=utf-8",
  			"Content-Type" : "application/json; charset=utf-8"
  		},
  	    data:{keywordor:keywordor},
  		url:"${pageContext.request.contextPath}/admin/information/searlistor.html",
  		success:function(data){


  	 	    var html = "";
            var templates = $("#data-template").html();
            $.each(data,function(i,item){
                  var da =  new Date(item.datecreation);

                html += Mustache.render(templates, {
                id: item.ordersId,
                totalmoney: "$"+item.totalmoney,
                datecreate:da,
                statusor:item.status,
                Username: item.username,
                customername: item.customerName,
                customerphone: item.customerMobile,
       
               
             
                });

            });

            $("#tbdada").html(html);
            ordersController.paging(0, function () {

            	ordersController.lodata();
            }, changePageSize);

            ordersController.RegisterEvent();
             
  	    }


        });
             },


       paging: function (totalrow, callback,changePageSize) {

           var totalpage = Math.ceil(totalrow / homeconfig.pagesize);

           if ($("#paginationdata a").length === 0 || changePageSize===true) {
               $("#paginationdata").empty();
               $("#paginationdata").removeData("twbs-pagination");
               $("#paginationdata").unbind("page");
           }

           $("#paginationdata").twbsPagination({
               totalPages: totalpage,
               first: "first",
               prev: "prev",
               next: "next",
               last: "last",
               visiblePages: 7,
               onPageClick: function (event, page) {
                   homeconfig.pageIndex= page;
                   setTimeout(callback, 200);
               }




           });
       }
 }

ordersController.init();







</script>















    <section class="content-header">
      <h1>

      Information Managed Orders
     
 
      </h1>
      <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/admin/home.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="${pageContext.request.contextPath}/admin/information/managed-orders.html">Managed Orders</a></li>
 
      </ol>
    </section>
    <section class="content">
<input type="hidden" id="totalorrder" value="${totalorder}">
<br>
<span id="result1"></span>
<div class="row">

        <div class="col-xs-12">
        
          <div class="box">
            <div class="box-header">
         <div class="col-md-5">
                    <input type="text"  class="form-control" id="keywowrd" placeholder="Type Name" />
                </div>
  <div class="col-md-3">
                    <button class="btn btn-primary" id="btnseearch">Search</button>
          
                </div>
          
            </div>
            <!-- /.box-header -->
           
            <div class="box-body table-responsive no-padding">
			
                       <div id="loadimg">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
              <i class="fa fa-refresh fa-4x fa-spin"></i> <label class="label label-primary">loading...</label>
            </div>
            <div class="col-xs-4"></div>
          </div>
              <table class="table table-hover">
             <thead>  <tr>
                  <th>ID</th>
                  <th>Total Money</th>
                  <th>Date Create</th>
                  <th>Status</th>
                  <th>Username</th>
                  <th>Customer Name</th>
                  <th>Customer Phone</th>
                  <th>Action</th>
                </tr>
                </thead>
                 <tbody id="tbdada">
           
     
             
           
        
               
             
              </tbody></table>
            <div class="pagination" id="paginationdata">

            </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
              <script id="data-template" type="x-tmpl-mustache">
                <tr>
                    <td>{{id}}</td>
                    <td>{{{totalmoney}}}</td>
               
              
                    <td >{{datecreate}}</td>
              <td >{{{status}}}</td>
                    <td>{{Username}}</td>
                    <td> {{customername}}</td>
                   <td>{{customerphone}}</td>
<td>  <a href="${pageContext.request.contextPath}/admin/information/detail-order/{{id}}.html" class="btn btn-sm btn-info" ><i class="fa fa-info"></i></a>
 <button class="btn  btn-sm btn-danger btn-delete" data-id="{{id}}"><i class="fa fa-trash"></i> </button> 

  <a href="${pageContext.request.contextPath}/admin/information/eidt-order/{{id}}.html" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></button>

</td>
</tr>
   
            </script>
      </div>
      </section>
      
      
      
      
      

      
      
      
      
      