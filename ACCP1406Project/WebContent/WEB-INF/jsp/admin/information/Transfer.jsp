<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.6.2.js">
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.4.2.min.js">
 </script> 


<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-ui-1.8.2.custom.min.js">
 </script> 
 <link href="${pageContext.request.contextPath}/assets/admin/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>




<script type="text/javascript">

$(document).ready(function(){

    $("#loadimg").hide();




});



var TransferController = {

		init:function(){


	
		
			TransferController.lodata();
			TransferController.RegisterEvent();
	   },

    RegisterEvent:function(){



    	  $('#fromvalidatransfer').validate({
              rules: {
            	  nametransfer:{
                	    required: true,
                	     minlength: 3,
                         maxlength: 250
                   }
              
               
              },
              messages: {
                
            	  nametransfer: {
                      required: "you must enter Transfer name",
                      minlength: "Name Transfer must great than 3",
                      maxlength:"Name Transfer must less than 250"
                  }
              
              }

          });
    	

        
	    $("#btnaddnew").off('click').on('click',function(e){
    	    e.preventDefault();
          $("#modeladdupdate").modal('show');
          TransferController.resetform();
       });


	    $("#btnSave").off('click').on('click', function (e) {
   	     e.preventDefault();
            var isValid = $('#fromvalidatransfer').valid();
            if(isValid){
            	TransferController.Savedata();
            }
           
           });




	      $(".btn-edit").off('click').on('click',function(e){
	    	   e.preventDefault();

	    	    var id = $(this).data('id');
	    	    TransferController.loadedit(id);
	            $("#modeladdupdate").modal('show');
	       });

	       $(".btn-delete").off('click').on('click',function(e){
	    	   e.preventDefault();
	   	       var id = $(this).data('id');
	   	    TransferController.deletetransferadmin(id);
	       });

        
        },



     

 
        resetform:function(){
            $("#txtnametransfer").val("");
 
           $("#hidIdtrafer").val(0);
        },




        loadedit:function(id){
            
            $.ajax({
               type:"GET",
               	headers : {
					Accept : "application/json; charset=utf-8",
					"Content-Type" : "application/json; charset=utf-8"
				},
                   data:{id:id},
                   url:"${pageContext.request.contextPath}/admin/information/detailstranfer.html",
                   success:function(data){
               
                    
                      if(data==null){
                          bootbox.alert("not load Product category")
                          }
                      else{
                     
                 
                            $("#hidIdtrafer").val(data.transferId);
                             $("#txtnametransfer").val(data.transferName);

              
                        
                           
                    	
                     }

              	    TransferController.RegisterEvent();
                 }
                   
                       
           });
    
    },


            deletetransferadmin:function(id){
                $.ajax({
                    type: "POST",
        
                    data: { id: id },
                    url: "${pageContext.request.contextPath}/admin/information/delettransferadmin.html",
                    success: function (data) {
                        if(data=="success"){
            			    $("#loadimg").show();
            			     setTimeout(function () {
                            bootbox.alert("delete success", function () {
                                $("#loadimg").hide();
                                TransferController.lodata();
                            });

            			      }, 700);
                        }
                        else {
                            bootbox.alert(data);
                        }
                        TransferController.RegisterEvent();
                    }

                });
            },
            




        
        Savedata:function(){
     	    var nametransfer =  $("#txtnametransfer").val();
     	      
            var id =  $("#hidIdtrafer").val();

       

               $.ajax({
                   type: "POST",
        
                   data:{
                	   nametransfer:nametransfer,
                	   id:id,
                 	
                   },

           		 url : "${pageContext.request.contextPath}/admin/information/savestransfer.html",
           		  success: function (data) {
           			  if(data=="success"){
           				   $("#modeladdupdate").modal('hide');
           				    $("#loadimg").show();
           			
                     

                           setTimeout(function () {
                               bootbox.alert("Success Save", function () {
                             	    $("#loadimg").hide();
                                   
                       
             
                             	  TransferController.resetform();
                             	   window.location.assign("${pageContext.request.contextPath}/admin/information/tranfer.html")
                               });
                           }, 1000);
                       } else {

                           bootbox.alert(data);
                       }
           			TransferController.RegisterEvent();
                   }

              });
               
         },
     
       lodata:function(){
    	
  
    	   $.ajax({
               type:"GET",
           
           	headers : {
				Accept : "application/json; charset=utf-8",
				"Content-Type" : "application/json; charset=utf-8"
			},
			url:"${pageContext.request.contextPath}/admin/information/listtransfer.html",
			success:function(data){


		                 	    var html = "";
				                var templates = $("#data-template").html();
				                $.each(data,function(i,item){
				   
				    
				                    html += Mustache.render(templates, {
				                    id: item.transferId,
				                    transfername: item.transferName,
				                 
			                     
				                    });

					            });

				                $("#tbdada").html(html);
				           	  TransferController.resetform();
				                TransferController.RegisterEvent();
               
		    }


          });
    			 
    	
       }
 }

TransferController.init();

</script>











 <section class="content-header">
      <h1>

      Information Transfer
     
 
      </h1>
      <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/admin/home.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="${pageContext.request.contextPath}/admin/information/tranfer.html">Transfer</a></li>
 
      </ol>
    </section>
    <section class="content">

<br>
<span id="result1"></span>
<div class="row">

        <div class="col-xs-12">
         
          <div class="box">
            <div class="box-header">
 
              <div class="box-tools">
      
                <div class="input-group input-group-sm" style="width: 150px;">
                    <button id="btnaddnew" class="btn btn-success pull-right">Add Transfer</button>
                </div>
              </div>
         
            </div>
            <!-- /.box-header -->
           
            <div class="box-body table-responsive no-padding">
			
                       <div id="loadimg">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
              <i class="fa fa-refresh fa-4x fa-spin"></i> <label class="label label-primary">loading...</label>
            </div>
            <div class="col-xs-4"></div>
          </div>
              <table class="table table-hover">
             <thead>  <tr>
                  <th>ID</th>
                  <th>Transfer Name</th>
            
                  <th>Action</th>
                </tr>
                </thead>
                 <tbody id="tbdada">
           
     
             
           
        
               
             
              </tbody></table>
       
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
              <script id="data-template" type="x-tmpl-mustache">
                <tr>
                    <td>{{id}}</td>
                    <td>{{transfername}}</td>
               
              
              
                    <td> <button class="btn  btn-sm btn-danger btn-delete" data-id="{{id}}"><i class="fa fa-trash"></i> </button> 



 
  <button class="btn btn-sm btn-primary btn-edit" data-id="{{id}}"><i class="fa fa-pencil"></i></button>

</td>
                </tr>
            </script>
      </div>
      </section>
      
      
      
      
              <div class="modal fade" id="modeladdupdate" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
       
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add/Update Transfer</h4>
            </div>
                <form method="post"  id="fromvalidatransfer">
            <div class="modal-body">
            
                    <div class="form-group">
                        <label for="txtname">Transfer Name:</label>
                        <input type="text" class="form-control" id="txtnametransfer" name="nametransfer">
                        <input type="hidden" value="0" id="hidIdtrafer" />
                    </div>
                
                 
      
            </div>
            <div class="modal-footer">
                <button  class="btn btn-primary" id="btnSave">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>

    </div>
</div>
      