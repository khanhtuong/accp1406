<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
  <script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.6.2.js">
</script>
  
  <script>

$(document).ready(function(){

	

    if($("#messgae").val()!=""){

        bootbox.alert($("#messgae").val());
    }
	  $('#fromaddminorder').validate({
        rules: {
      	  address:{
          	    required: true,
          	     minlength: 3,
                   maxlength: 250
             },
          
                quantity:{
              	  required: true,
              	    number:true,
              	     min:1,
                       max:1000000
                  }
         
        },
        messages: {
          
        	address: {
                required: "you must enter Address",
                minlength: "Name product must great than 3",
                maxlength:"Name product less than 250"
            },
    
     
              quantity:{
            	    required: "you must enter quantity",
                    max:"quantity must less than 1000000",
                    min:"quantity must great  0",
                 
                },
            
        }

    });
});

  </script>
  
  
  <style>
.error{
  color: red;
}
.title{
  font-size: 18px;
  font-weight: bold;
}

</style> 
<section class="content-header">
    <h1>
    
Edit Orders
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Orders</a></li>
        <li><a >Orders list</a></li>
        <li class="active">EDIT</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Horizontal Form -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Edit order ${orders.ordersId} </h3>
        </div><!-- /.box-header -->
 
             <input type="hidden" value="${updatesuccessorder}" id="messgae"> 
        <form class="form-horizontal" id="fromaddminorder"   action="${pageContext.request.contextPath}/admin/information/eidt-order/${orders.ordersId}.html" method="post"    >
            <div class="box-body">
                         <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"></label>
                    <input type="hidden" value="${orders.ordersId}" name="orderid">
                     <table border="0" width="900" >
                        <tr>
                          <th>     <span class="title">Product Name </span> </th>
                  
                          
                             <th>      <span class="title">  Quantity </span></th>
                       <th>      <span class="title">  Price </span></th>
                        </tr>
                        
                  
                        
                        
                        
                        
                   
        
         
                     <c:forEach var="orde" items="${orders.ordersdetails}">
                     
                    
                    
                    <div class="col-sm-10">
                        <input type="hidden" value="${orde.product.productId}" name="productid">
                    
                                 <tr>
                          <td>${orde.product.name}</td>
                          <td><input type="number" value="${orde.quantity}" name="quantity"></td>
                                  <td><input type="text" value="${orde.price}" name="price"  readonly="readonly" ></td>
                        </tr>

                    </div>
    
       
                     </c:forEach>
   
                
                      </table>
                         </div>
             
                   <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                  
              
                 
                                <textarea  cols="20" rows="4" class="form-control" name="description">${orders.customerMessage }</textarea>
                    </div>
                </div>
       
                                    <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-10">
                  
              
                 
                                <textarea  cols="20" rows="4" class="form-control" name="address">${orders.customerAddress }</textarea>
                    </div>
                </div>
       
       
       
            
      
            
        </div>
        
        
        
        
            <div class="box-footer">
                <a href="${pageContext.request.contextPath}/admin/information/managed-orders.html" class="btn btn-default">Back Manged Orders </a>
                <button type="submit" class="btn btn-success pull-right">Save</button>
            </div><!-- /.box-footer -->
        </form>
    </div><!-- /.box -->
</section><!-- /.content -->