<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.6.2.js">
	
</script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.4.2.min.js">
	
</script>


<script type="text/javascript"
	src="${pageContext.request.contextPath}/assets/admin/js/jquery-ui-1.8.2.custom.min.js">
	
</script>
<link
	href="${pageContext.request.contextPath}/assets/admin/css/jquery-ui.min.css"
	rel="stylesheet" type="text/css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">

$(document).ready(function(){

    $("#loadimg").hide();


    $.ajax({
    	type : "GET",

		headers : {
			Accept : "application/json; charset=utf-8",
			"Content-Type" : "application/json; charset=utf-8"
		},
		url : "${pageContext.request.contextPath}/admin/information/revenueschart.html",
		success : function(data) {
                 google.charts.load('current',{
                        'packages':['corechart']
                 });
                 google.charts.setOnLoadCallback(function(){
                	 drawChart(data)
                  });
          

		}
     });

        function drawChart(result){
              var data =  new google.visualization.DataTable();
            
              data.addColumn('string','Datecreation');
              data.addColumn('number','Revenues');
              data.addColumn('number','Benefit');
              var dataArray = [];
              $.each(result,function(i,obj){
          		var da = new Date(obj.datecreation);
            	  dataArray.push([da.getDate() + " / "
									+ (da.getMonth() + 1) + " / "
									+ da.getFullYear(),obj.revenues,obj.benefit]);
               });

              data.addRows(dataArray);

              var piechart_options = {
                     title:'Pie chart : ',
                     width:600,
                     height:300
               };
              var piechart =   new google.visualization.PieChart(document.getElementById('piechart_div'));
              piechart.draw(data,piechart_options);

              
              var barchart_options = {
                      title:'bar chart : ',
                      width:600,
                      height:300,
                      legend:'none'
                };
              var barhart =   new google.visualization.BarChart(document.getElementById('barchart_div'));
              barhart.draw(data,barchart_options);
        }
    
});

	var revenuesController = {

		init : function() {

			revenuesController.lodata();
			revenuesController.RegisterEvent();
		},

		RegisterEvent : function() {
		       $("#btnseearch").off('click').on('click',function(e){
		    	    e.preventDefault();
		              

		    	    revenuesController.lodata();
		    	  
		         });

		       $("#reset").off('click').on('click',function(e){
		    	    e.preventDefault();
		              

		    	      window.location.assign("${pageContext.request.contextPath}/admin/information/revenues.html")
		    	  
		         });
		   
		},



	

		lodata : function() {

			var fromdata = $("#formdata").val();
			var todate =  $("#todata").val();
			$
					.ajax({
						type : "GET",

						headers : {
							Accept : "application/json; charset=utf-8",
							"Content-Type" : "application/json; charset=utf-8"
						},
						data : {
							fromdata:fromdata,
							todate:todate
						},
						url : "${pageContext.request.contextPath}/admin/information/revenuesfindll.html",
						success : function(data) {
						    var flag =  true;
                            if(data!=null){
                            	var html = "";
    							var templates = $("#data-template").html();
    							$.each(data, function(i, item) {

    								var da = new Date(item.datecreation);

    								html += Mustache.render(templates, {
    									date : da.getDate() + " / "
    											+ (da.getMonth() + 1) + " / "
    											+ da.getFullYear(),
    									revenues : item.revenues,
    									benefit : item.benefit,

    								});
    								flag =  false;
    							});

    							$("#tbdada").html(html);
                             }
                            

    						if(flag){
    						     bootbox.alert("you must choose greater than from date to date or Revenues null")
        					}

							revenuesController.RegisterEvent();

						}

					});

		}
	}

	revenuesController.init();
</script>

<section class="content-header">
	<h1>Information Revenues</h1>
	<ol class="breadcrumb">
		<li><a href="${pageContext.request.contextPath}/admin/home.html"><i
				class="fa fa-dashboard"></i> Home</a></li>
		<li><a
			href="${pageContext.request.contextPath}/admin/information/revenues.html">Revenues</a></li>

	</ol>
</section>
<section class="content">

	<br> <span id="result1"></span>
	<table class="columns">
	   <tr>
	      <td><div id="piechart_div" style="border: 1px solid #ccc"></div> </td>
	          <td><div id="barchart_div" style="border: 1px solid #ccc"></div> </td>
	   </tr>
	</table>
	<div class="row">

		<div class="col-xs-12">

			<div class="box">
				<div class="box-header">
					<div class="col-md-5">
						From date <select id="formdata" aria-controls="example1">

							<c:forEach var="data" items="${listdata}">
								<option value="${data}">${data}</option>
							</c:forEach>



						</select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; To date <select
							id="todata" aria-controls="example1">

							<c:forEach var="data" items="${listdata}">
								<option value="${data}">${data}</option>
							</c:forEach>



						</select> &nbsp;&nbsp;&nbsp;&nbsp;
						<button class="btn btn-primary" id="btnseearch">Search</button>
						<button class="btn btn-success" id="reset">Reset</button>
				
					</div>
					<div class="col-md-3"></div>

				</div>
				<!-- /.box-header -->

				<div class="box-body table-responsive no-padding">

					<div id="loadimg">
						<div class="col-xs-4"></div>
						<div class="col-xs-4">
							<i class="fa fa-refresh fa-4x fa-spin"></i> <label
								class="label label-primary">loading...</label>
						</div>
						<div class="col-xs-4"></div>
					</div>
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Date</th>
								<th>Revenues</th>
								<th>Benefit</th>

							
							</tr>
						</thead>
						<tbody id="tbdada">







						</tbody>
					</table>

				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<script id="data-template" type="x-tmpl-mustache">
                <tr>
                    <td>{{date}}</td>
                    <td>{{revenues}}</td>
               
              
                    <td>{{benefit}}</td>
                   
                </tr>
            </script>
	</div>
</section>