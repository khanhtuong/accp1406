<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
       <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.6.2.js">
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.4.2.min.js">
 </script> 


<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-ui-1.8.2.custom.min.js">
 </script> 
 <link href="${pageContext.request.contextPath}/assets/admin/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    
  <script>
  $(document).ready(function(){
	    if($("#messgaeor").val()!=""){

	        bootbox.alert($("#messgaeor").val());
	    }
	  
  });  
     
  </script>  

    <section class="content-header">
      <h1>
        Invoice
        <small>${orders.ordersId}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/admin/home.html"><i class="fa fa-dashboard"></i> Home</a></li>
              <li><a href="${pageContext.request.contextPath}/admin/information/managed-orders.html"><i class="fa fa-dashboard"></i> Managed Orders</a></li>
        <li class="active">Invoice</li>
      </ol>
    </section>
<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Orders Managed Details
            <small class="pull-right">Date: <fmt:formatDate value="${orders.datecreation}" pattern="dd/MM/yyyy"/></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
       
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
        <c:if test="${orders.account.username!=null}">
              <address>
            <strong>Username : ${orders.account.username}</strong><br>
          <strong>  Full Name:</strong> ${orders.account.fullname}<br>
           <strong>   Email: </strong> ${orders.account.email}<br>
         <strong>    Address: </strong>  ${orders.account.address}
         <br>
         <strong>  Customer Message: </strong>  ${orders.customerMessage}
          </address>
        </c:if>
        
                <c:if test="${orders.account.username==null}">
              <address>
            <strong>Customer Name : ${orders.customerName}</strong><br>
          <strong>  Customer Mobile:</strong> ${orders.customerMobile}<br>
           <strong>   Customer Address: </strong> ${orders.customerAddress}<br>
         <strong>  Customer Message: </strong>  ${orders.customerMessage}
          </address>
        </c:if>
    
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
     
          <br>
          <b>Order ID:</b> ${orders.ordersId }<br><input type="hidden" value="${orders.ordersId}" id="idorders">
          <b>Day Order:</b> <fmt:formatDate value="${orders.datecreation}" pattern="dd/MM/yyyy"/></small><br>
          <b>Status:</b> ${sa== true ? "<span class='label label-success'>Payment  
 already</span>" : "<span class='label label-danger'>
 Payment Yet</span>"} 


  
  
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
         <c:set var="s" value="0"></c:set>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Product</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Description</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
         

          <c:forEach var="orderdetail" items="${listorderde}">
             <tr>
              <td>${orderdetail.product.name}</td>
              <td>${orderdetail.price}</td>
              <td>${orderdetail.quantity}</td>
              <td>${orderdetail.orders.customerMessage}</td>
              <td>$${orderdetail.price* orderdetail.quantity}</td>
              <c:set var="s" value="${s = s+ (orderdetail.price* orderdetail.quantity) }"></c:set>
            </tr>
          </c:forEach>
         
  
            
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Payment Methods:</p>
          <c:if test="${orders.payment.paymentId==1}">
               <img src="${pageContext.request.contextPath}/assets/admin/img/credit/paypal2.png" alt="Paypal">
          
          </c:if>
              <c:if test="${orders.payment.paymentId==2}">
               <img src="${pageContext.request.contextPath}/assets/admin/img/credit/visa.png" alt="Paypal">
          
          </c:if>
              <c:if test="${orders.payment.paymentId==3}">
               <img src="${pageContext.request.contextPath}/assets/admin/img/credit/mastercard.png" alt="Paypal">
          
          </c:if>
   
     
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
     <p class="lead"><b>Transfer Methods</b>:${orders.transfer.transferName}</p>
          </p>
          
          
          
     
    
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Amount</p>

          <div class="table-responsive">
            <table class="table">
              <tbody><tr>
                <th style="width:50%">Subtotal:</th>
                <td>$${s}</td>
              </tr>
            
              <tr>
                <th>Shipping:</th>
                <td>Free</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td style="color: red;font-weight: 900;">$${s}</td>
              </tr>
            </tbody></table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
     <input type="hidden" value="${message}" id="messgaeor">
     <form action="${pageContext.request.contextPath}/admin/information/detail-order/${orders.ordersId}.html" method="post">
       <button type="submit" class="btn btn-success pull-right" ><i class="fa fa-credit-card"></i> Submit Payment
          </button>
     </form>
        
    
        </div>
      </div>
 
    </section>
    
                <a href="${pageContext.request.contextPath}/admin/information/managed-orders.html" class="btn btn-success pull-right" >Back Manged Orders      </a>