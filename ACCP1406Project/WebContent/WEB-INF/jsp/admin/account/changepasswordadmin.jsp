<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    
<style>
.error{
  color: red;
}

</style>    
<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Change password</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="${pageContext.request.contextPath}/admin/account/changepasswordadmin.html"  method="post" class="form-horizontal">
              <div class="box-body">
                   
                <div class="form-group">
                <span class="error">${errorpasswordnot}</span>
         
                  <label for="inputEmail3"  class="col-sm-2 control-label">Password</label>

                  <div class="col-sm-10">
                    <input class="form-control" name="password"  placeholder="Password" type="password">
                     <span class="error">  ${errorpassword}</span>   
                  </div>
                  
            
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">New Password</label>

                  <div class="col-sm-10">
                    <input class="form-control" name="newpassword" placeholder="New Password" type="password">
                        <span class="error">${errornewpassword}</span>  
                  </div>
            
                </div>
            
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
     
                <button type="submit" class="btn btn-info pull-right">Save</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>