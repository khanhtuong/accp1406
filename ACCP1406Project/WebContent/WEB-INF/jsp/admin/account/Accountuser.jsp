<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.6.2.js">
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.4.2.min.js">
 </script> 


<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-ui-1.8.2.custom.min.js">
 </script> 
 <link href="${pageContext.request.contextPath}/assets/admin/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <style>
    .fotmatsde{
  font-size: 17px;
  font-weight: 800px;
  font-family: sans-serif;
 
}

.formattile{
   color: blue;;
}

</style>
    
    
    <script type="text/javascript">

$(document).ready(function(){

    $("#txtusernamesearch").autocomplete({
        source: '${pageContext.request.contextPath}/admin/account/searchusernameadmin.html'

     });

    $("#loadimg").hide();




});



var AccountadminController = {

		init:function(){


	
		
			AccountadminController.lodata();
			AccountadminController.RegisterEvent();
	   },

    RegisterEvent:function(){


	    $("#btnseearch").off('click').on('click',function(e){
    	    e.preventDefault();
         
    	    AccountadminController.lodata();
         });

	   

	    $(".btn-info").off('click').on('click',function(e){
    	    e.preventDefault();
            var username = $(this).data('username');
            $("#modeldetailsaccount").modal('show');
            AccountadminController.loadingfind(username);
         });

     
	      $(".btn-delete").off('click').on('click', function (e) {
	    	   e.preventDefault();
	           var username  =  $(this).data('username');
	          bootbox.confirm("Are you sure to delete this Account " + username, function (result) {
	              if (result) {
	            	  AccountadminController.deleteaccountadmin(username);
 
	              }

	           
	          });

    
      
        
	      });
        
        },

        deleteaccountadmin:function(username){
            $.ajax({
                type: "POST",
    
                data: { username:username },
                url: "${pageContext.request.contextPath}/admin/account/deleteaccountadmin.html",
                success: function (data) {
                    if(data=="success"){
        			    $("#loadimg").show();
        			     setTimeout(function () {
                        bootbox.alert("delete success", function () {
                            $("#loadimg").hide();
                     	   AccountadminController.lodata();
                        });

        			      }, 1000);
                    }
                    else {
                        bootbox.alert(data);
                    }
                }

            });
        },



        loadingfind:function(username){
            $.ajax({
                type:"GET",
                	headers : {
 					Accept : "application/json; charset=utf-8",
 					"Content-Type" : "application/json; charset=utf-8"
 				},
                    data:{username:username},
                    url:"${pageContext.request.contextPath}/admin/account/findaccountadminusername.html",
                    success:function(data){
                    	   var d =  new Date(data.createdate);
                       if(data==null){
                           bootbox.alert("not load Product category")
                           }
                       else{

                

                          
                           $("#txtusername").html(data.username);
                           $("#txtfullnames").html(data.fullname)
                           $("#txtemail").html(data.email);
                           $("#txtage").html(data.age);
                           $("#txtphone").html(data.phone);
                     
                           $("#txtuseraddress").html(data.address);
                            $("#txtcreatedateaccount").html(d.getDate() + " / " + (d.getMonth()+1) + " / " + d.getFullYear());
                            
                     
                         
                        
                      }
                  }
            });
       },
  


       lodata:function(){
    	
    	 var username = $("#txtusernamesearch").val();
    	   $.ajax({
               type:"GET",
           
           	headers : {
				Accept : "application/json; charset=utf-8",
				"Content-Type" : "application/json; charset=utf-8"
			},
		    data:{username:username},
			url:"${pageContext.request.contextPath}/admin/account/listaccountuser.html",
			success:function(data){


		                 	    var html = "";
				                var templates = $("#data-template").html();
				                $.each(data,function(i,item){
				                      var da =  new Date(item.createdate);
				    
				                    html += Mustache.render(templates, {
				                    username: item.username,
				                    fullanme:item.fullname,
				                    email: item.email,
				                    phone:item.phone,
				                    createdate: da.getDate() + " / " + (da.getMonth()+1) + " / " + da.getFullYear() 
			                     
			                     
				                    });

					            });

				                $("#tbdada").html(html);

				                AccountadminController.RegisterEvent();
               
		    }


          });
    			 
    	
       }
 }

AccountadminController.init();

</script>
    
    
    
    
    
    
      <section class="content-header">
      <h1>

        Account User List
     
 
      </h1>
      <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/admin/home.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Account List</a></li>
 
      </ol>
    </section>
    <section class="content">

<br>
<span id="result1"></span>
<div class="row">

        <div class="col-xs-12">
        
          <div class="box">
            <div class="box-header">
         <div class="col-md-5">
                    <input type="text"  class="form-control" id="txtusernamesearch" placeholder="Type Name" />
                </div>
  <div class="col-md-3">
                    <button class="btn btn-primary" id="btnseearch">Search</button>
              
                </div>
        
            </div>
            <!-- /.box-header -->
           
            <div class="box-body table-responsive no-padding">
			
                       <div id="loadimg">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
              <i class="fa fa-refresh fa-4x fa-spin"></i> <label class="label label-primary">loading...</label>
            </div>
            <div class="col-xs-4"></div>
          </div>
              <table class="table table-hover">
             <thead>  <tr>
                  <th>Username</th>
                  <th>Fullname</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Create date</th>
                  <th>Action</th>
                </tr>
                </thead>
                 <tbody id="tbdada">
           
     
             
           
        
               
             
              </tbody></table>
       
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
              <script id="data-template" type="x-tmpl-mustache">
                <tr>
                    <td>{{username}}</td>
                    <td>{{fullanme}}</td>
               
              
                    <td >{{email}}</td>
                    <td>{{phone}}</td>
                    <td>{{{createdate}}}</td>
                    <td>
  <button class="btn btn-sm btn-info btn-info" data-username="{{username}}"><i class="fa fa-info"></i></button>
<button class='btn  btn-sm btn-danger btn-delete' data-username="{{username}}"><i class='fa fa-trash'></i></button>
</td>
                </tr>
            </script>
      </div>
      </section>
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
        <div class="modal fade" id="modeldetailsaccount" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
       
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Account User details</h4>
            </div>
            
            <div class="modal-body">
            
                    <div class="form-group">
                        <label for="txtname" class="formattile">Username : </label>&nbsp;&nbsp; <span class="fotmatsde"   id="txtusername"></span>
                    
                     
                      
                    </div>
                    <div class="form-group">
                                 <label for="txtname" class="formattile">Fullname : </label> &nbsp;&nbsp; <span class="fotmatsde"  id="txtfullnames"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Email: </label> &nbsp;&nbsp;  <span class="fotmatsde"  id="txtemail"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Age: </label> &nbsp;&nbsp; <span class="fotmatsde"  id="txtage"></span>
                    
                    </div>
                    
                              <div class="form-group">
                                 <label for="txtname" class="formattile">Phone: </label> &nbsp;&nbsp; <span class="fotmatsde"  id="txtphone"></span>
                    
                    </div>
                                   <div class="form-group">
                                 <label for="txtname" class="formattile">Address: </label> &nbsp;&nbsp; <span class="fotmatsde"  id="txtuseraddress"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Create date : </label> &nbsp;&nbsp;  <span class="fotmatsde"  id="txtcreatedateaccount"></span>
                    
                    </div>
                    
                 
                 
             
      
            </div>
            <div class="modal-footer">

   
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        
        </div>

    </div>
</div>
      