<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form"%>

    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.6.2.js">
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-1.4.2.min.js">
 </script> 


<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-ui-1.8.2.custom.min.js">
 </script> 
 <link href="${pageContext.request.contextPath}/assets/admin/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    
    
    
    
    
     
    <script type="text/javascript">

$(document).ready(function(){




	
 

    

      
   
	
});

var profileaccount = {

		init:function(){


			
			
			profileaccount.lodataedit();
			profileaccount.RegisterEvent();
	   },



	   RegisterEvent:function(){


			  $('#frmeditaccoutnadmin').validate({
		           rules: {
		         	  
		                 fullname:{
		                	  required: true,
		           	     minlength: 3,
		                    maxlength: 250
		                    },

		                    email:{
		                 	 	  required: true,
		                 	       email:true,
		                 	    maxlength: 250
		                    },
		                    age:{
		                 	     required:true,
		                          number:true,
		                          min:18,
		                          max:60
		                    },
		                    phone:{
		                  	  required: true,
		         		    maxlength: 47
		                        },

		                        address:{
		                         	  required: true,
		                		    maxlength: 250
		                               }
		            
		           },
		           messages: {
		             
		         
		         	  fullname:{
		             	    required: "you must enter fullname",
		                     minlength: "fullname must great than 3 charater",
		                     maxlength:"fullname must less than 250 charater"
		              },
		              email:{
		          	    required: "you must enter Email",
		                  maxlength:"Email must less than 250 charater",
		                  email:"you must enter right email charater"
		              },
		              age:{
		           	    required: "you must enter Age",
		                   max:"Age must less than 60",
		                   min:"Age must great than 18",
		                
		               },

		               phone:{
		           	    required: "you must enter Phone",
		           	  maxlength:"Phone must less than 47 charater",
		                  },
		                  address:{
		               	   required: "you must enter Address",
		               	     maxlength:"Address must less than 250 charater",
		                  }
		               
		           }

		       });


				   
		    $("#btn-editpro").off('click').on('click',function(e){
	    	    e.preventDefault();

	           $("#modeleditadmin").modal('show');

	         });


	    $("#saveaccount").off('click').on('click',function(e){
	 	    e.preventDefault();
	 	    var isValid = $('#frmeditaccoutnadmin').valid();
            if(isValid){
	 		profileaccount.Savedata();
            }
           });

	},


    Savedata:function(){

        

   

  

     
      
 	    var fullname =       $("#upfullname").val();
 	               var email =          $("#upemail").val();
       
           var phone =     $("#upphone").val();
           var age =       $("#upage").val();
   var address =   $("#upAddress").val();
  
    
           $.ajax({
               type: "POST",
    
               data:{
            	   fullname:fullname,
            	   email:email,
            	   phone:phone,
            	   age:age,
            	   address:address
               },

       		 url : "${pageContext.request.contextPath}/admin/account/save.html",
       		  success: function (data) {
       			  if(data=="success"){
       				   $("#modeleditadmin").modal('hide');
       				    $("#loadimg").show();
       			
                 

                       setTimeout(function () {
                           bootbox.alert("Success Save", function () {
                         	    $("#loadimg").hide();
                               
                        		profileaccount.lodataedit();
                         	
                           });
                       }, 700);
                  
                   } else {

                       bootbox.alert(data);
                   }
       			profileaccount.RegisterEvent();
               }

          });
           
     },
	  
	   lodataedit:function(){
		   $.ajax({
			     type:"GET",
		           	headers : {
						Accept : "application/json; charset=utf-8",
						"Content-Type" : "application/json; charset=utf-8"
					},
				    url:"${pageContext.request.contextPath}/admin/account/accountprofile.html",
				   success:function(data){

				 	   var createdate =  new Date(data.createdate);
	                 
	                  if(data==null){
	                      bootbox.alert("not load Account")
	                      }
	                  else{
	                 
	               
	                      
	                   

	                     
	                      $("#textusername").html(data.username);
	                      $("#textfullname").html(data.fullname)
	                      $("#textemail").html(data.email);
	                      $("#textage").html(data.age);
	                      $("#textcreate").html(createdate.getDate() + " / " + (createdate.getMonth()+1) + " / " + createdate.getFullYear());
	                      $("#textphone").html(data.phone);
	                      $("#textaddress").html(data.address);


	                  
	                      $("#upusername").val(data.username);
	                      $("#upfullname").val(data.fullname)
	                      $("#upemail").val(data.email);
	                      $("#upage").val(data.age);
	             
	                      $("#upphone").val(data.phone);
	                      $("#upAddress").val(data.address);
	                  
	                    	profileaccount.RegisterEvent();
	                 }
					 }
			   });
	

       }
	   
}
profileaccount.init();


</script>
    
    
    
    
    
    
    
    
    
    
    
<style>
.fotmatcontent{
  font-size: 17px;
  font-weight: 800px;
  font-family: sans-serif;
  margin-left: 30px;
} 
.fotmatitle{
  font-size: 17px;
  font-weight: 800px;
  font-family: sans-serif;
  margin-left: 150px;
}
</style>
    <div class="col-md-10">
            
<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Information Account</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                <label class="fotmatitle">Username :<span class="text-green fotmatcontent"  id="textusername">${requestScope.account.username}</span>  </label>
           
 
              
               
                </div>
                     <div class="form-group">
                <label class="fotmatitle">Full name :<span class="text-green fotmatcontent"  id="textfullname">${account.fullname}</span>  </label>
           
              
              
               
                </div>
                     <div class="form-group">
                <label class="fotmatitle">Email :<span class="text-green fotmatcontent"  id="textemail">${account.email}</span>  </label>
           
              
              
               
                </div>
                
                     <div class="form-group">
                <label class="fotmatitle">Age :<span class="text-green fotmatcontent"  id="textage">${requestScope.account.age}</span>  </label>
           
              
              
               
                </div>
                
                        <div class="form-group">
                <label class="fotmatitle">Create date :<span class="text-green fotmatcontent"  id="textcreate">${account.createdate}</span>  </label>
           
              
              
               
                </div>
                
                        <div class="form-group">
                <label class="fotmatitle">Phone  :<span class="text-green fotmatcontent"  id="textphone">${account.phone}</span>  </label>
           
              
              
               
                </div>
                
                
             
                    
                    
                              <div class="form-group">
                <label class="fotmatitle">Address  :<span class="text-green fotmatcontent"  id="textaddress">${account.address}</span>  </label>
           
              
                     ${usernamesave}
               
                </div>
               </div>
          
                
       
              <!-- /.box-body -->
              <div class="box-footer">
           
                <button  class="btn btn-info pull-right" id="btn-editpro">Edit</button>
              </div>
              <!-- /.box-footer -->
            </form>
            
          </div>
          </div>
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
                     <div class="modal fade" id="modeleditadmin" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
       
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Account admin</h4>
            </div>
              <form id="frmeditaccoutnadmin">
            <div class="modal-body">
            
                    <div class="form-group">
                        <label >Username:</label>
              
              <input type="text" id="upusername" class="form-control" readonly="readonly">
                    </div>
                 
               <div class="form-group">
                        <label >Fullname:</label>
                   
                                 <input type="text" id="upfullname" class="form-control"  name="fullname">
                    </div>
                    
                         <div class="form-group">
                        <label >Email:</label>
               
                                     <input type="text" id="upemail" class="form-control"  name="email">
                    </div>
                         <div class="form-group">
                        <label >Age:</label>
                
                                   <input type="number" id="upage" class="form-control"   name="age">
                    </div>
                 
                 <div class="form-group">
                        <label >Phone:</label>
                    
                              <input type="text" id="upphone" class="form-control"    name="phone">
                    </div>
                    
                              <div class="form-group">
                        <label >Address:</label>

                          
                   
                          <textarea rows="" cols="" id="upAddress" class="form-control" name="address" ></textarea>
                    </div>
                

             
            </div>
            <div class="modal-footer">
                <button type="button"  class="btn btn-primary" id="saveaccount" >Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
        </div>

    </div>
</div>
      