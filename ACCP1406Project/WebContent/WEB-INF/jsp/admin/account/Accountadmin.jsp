<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<script type="text/javascript" src="${pageContext.request.contextPath}/assets/admin/js/jquery-ui-1.8.2.custom.min.js">
 </script> 
 <link href="${pageContext.request.contextPath}/assets/admin/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <style>
    .fotmatsde{
  font-size: 17px;
  font-weight: 800px;
  font-family: sans-serif;
 
}

.formattile{
   color: blue;;
}

</style>
    
    
    <script type="text/javascript">

$(document).ready(function(){

	  $("#txtusernamesearch").autocomplete({
	        source: '${pageContext.request.contextPath}/admin/account/searchusernameadmin.html'

	     });


    $("#loadimg").hide();




});



var AccountadminController = {

		init:function(){


		
			AccountadminController.lodata();
			AccountadminController.RegisterEvent();
	   },

    RegisterEvent:function(){


    	  $('#frmaddaccoutnadmin').validate({
              rules: {
            	  username:{
                	    required: true,
                	     minlength: 3,
                         maxlength: 250
                   },
                   password:{
                	  required: true,
             	     minlength: 3,
                      maxlength: 250
                    },
                    fullname:{
                   	  required: true,
              	     minlength: 3,
                       maxlength: 250
                       },

                       email:{
                    	 	  required: true,
                    	        email:true,
                    	    maxlength: 250
                       },
                       age:{
                    	     required:true,
                    
                             min:18,
                             max:60
                       },
                       phone:{
                    		  required: true,
                    		    maxlength: 47
                           },

                           address:{
                        		  required: true,
                      		    maxlength:250

                           }
               
              },
              messages: {
                
            	  username: {
                      required: "you must enter Username",
                      minlength: "Username must great than 3 charater",
                      maxlength:"Username must less than 250 charater"
                  },
                  password: {
                	     required: "you must enter Password",
                         minlength: "Password must great than 3 charater",
                         maxlength:"Password must less than 250 charater"
                  },
                  fullname:{
                	    required: "you must enter fullname",
                        minlength: "fullname must great than 3 ",
                        maxlength:"fullname must less than 250 charater"
                 },
                 email:{
             	    required: "you must enter Email",
                     maxlength:"Email must less than 250 charater" ,
                     email:"you must enter right email"
                 },
                 age:{
              	    required: "you must enter Age",
                      max:"Age must less than 60",
                      min:"Age must great than 18",
                   
                  },
                  phone:{
                	    required: "you must enter Phone",
                	    maxlength:"Phone must less than 47 charater",
                 
                     
                    },
                    address:{
                        required: "you must enter address",
                        maxlength:"address must less than 250 charater"
                   }
                  
              }

          });

	    $("#btnseearch").off('click').on('click',function(e){
    	    e.preventDefault();
         
    	    AccountadminController.lodata();
         });

	   

	    $(".btn-info").off('click').on('click',function(e){
    	    e.preventDefault();
            var username = $(this).data('username');
            $("#modeldetailsaccount").modal('show');
            AccountadminController.loadingfind(username);
         });

     
	    $("#btnaddnew").off('click').on('click',function(e){
    	    e.preventDefault();
          $("#modeladdupdate").modal('show');
          AccountadminController.resetform();
     
        });

	      $("#btnSave").off('click').on('click', function (e) {
	    	     e.preventDefault();
	             var isValid = $('#frmaddaccoutnadmin').valid();
	             if(isValid){
	            	    AccountadminController.Savedata();
	             }
	            
	       });

	      $(".btn-delete").off('click').on('click', function (e) {
	    	   e.preventDefault();
	           var username  =  $(this).data('username');
	          bootbox.confirm("Are you sure to delete this Account " + username, function (result) {
	              if (result) {
	            	  AccountadminController.deleteaccountadmin(username);
  
	              }

	           
	          });

     
       
         
	      });
		       
        
        },

        deleteaccountadmin:function(username){
            $.ajax({
                type: "POST",
    
                data: { username:username },
                url: "${pageContext.request.contextPath}/admin/account/deleteaccountadmin.html",
                success: function (data) {
                    if(data=="success"){
        			    $("#loadimg").show();
        			     setTimeout(function () {
                        bootbox.alert("delete success", function () {
                            $("#loadimg").hide();
                     	   AccountadminController.lodata();
                        });

        			      }, 1000);
                    }
                    else {
                        bootbox.alert(data);
                    }
                }

            });
        },
        Savedata:function(){
     	    var username =  $("#addusername").val();
               var password =  $("#addpassword").val();
           
               var fullname =  $("#addfullname").val();
               var email =  $("#addemail").val();
               var age =  $("#addage").val();
               var phone =  $("#addphone").val();
               var address =  $("#addAddress").val();
               $.ajax({
                   type: "POST",
        
                   data:{
                	   username:username,
                	   password:password,
                	   fullname:fullname,
                	   email:email,
                	   age:age,
                	   phone:phone,
                	   address:address
                   },

           		 url : "${pageContext.request.contextPath}/admin/account/createaccountadmin.html",
           		  success: function (data) {
           			  if(data=="success"){
           				   $("#modeladdupdate").modal('hide');
           				    $("#loadimg").show();
           			
                     

                           setTimeout(function () {
                               bootbox.alert("Success Save", function () {
                             	    $("#loadimg").hide();
                                   
                             	   AccountadminController.lodata();
                             	   AccountadminController.resetform();
                               });
                           }, 1000);
                       }

           			  else if(data=="existusername"){
           			      bootbox.alert("username already exists");
               		  }
                       else {

                           bootbox.alert(data);
                       }
               	
                   }

              });
               
         },


        loadingfind:function(username){
            $.ajax({
                type:"GET",
                	headers : {
 					Accept : "application/json; charset=utf-8",
 					"Content-Type" : "application/json; charset=utf-8"
 				},
                    data:{username:username},
                    url:"${pageContext.request.contextPath}/admin/account/findaccountadminusername.html",
                    success:function(data){
                    	   var d =  new Date(data.createdate);
                       if(data==null){
                           bootbox.alert("not load Product category")
                           }
                       else{

                

                          
                           $("#txtusername").html(data.username);
                           $("#txtfullnames").html(data.fullname)
                           $("#txtemail").html(data.email);
                           $("#txtage").html(data.age);
                           $("#txtphone").html(data.phone);
                           $("#txtaddress").html(data.address);
                           
                            $("#txtcreatedateaccount").html(d.getDate() + " / " + (d.getMonth()+1) + " / " + d.getFullYear());
                            
                     
                         
                        
                      }
                  }
            });
       },
  

       resetform:function(){

    	    $("#addusername").val("");
            $("#addpassword").val("");
            $("#addfullname").val("");
           $("#addemail").val("");
           $("#addage").val("");
           $("#addphone").val("");
           $("#addAddress").val("");
       },

       lodata:function(){
    	
    	 var username = $("#txtusernamesearch").val();
    	   $.ajax({
               type:"GET",
           
           	headers : {
				Accept : "application/json; charset=utf-8",
				"Content-Type" : "application/json; charset=utf-8"
			},
		    data:{username:username},
			url:"${pageContext.request.contextPath}/admin/account/listaccountadmin.html",
			success:function(data){
                       var btndelete =  $(".btndelete").val();
                    
		                 	    var html = "";
				                var templates = $("#data-template").html();
				                $.each(data,function(i,item){
				                      var da =  new Date(item.createdate);
				    
				                    html += Mustache.render(templates, {
				                    username: item.username,
				                    fullanme:item.fullname,
				                    email: item.email,
				                    phone:item.phone,
				                    createdate: da.getDate() + " / " + (da.getMonth()+1) + " / " + da.getFullYear(),
				                    action:btndelete == "true" ? "<button class='btn  btn-sm btn-danger btn-delete' data-username='"+item.username+"'><i class='fa fa-trash'></i></button>":""
				                    
,
			                     
			                     
				                    });

					            });

				                $("#tbdada").html(html);

				                AccountadminController.RegisterEvent();
               
		    }


          });
    			 
    	
       }
 }

AccountadminController.init();

</script>
    
    
    
    
    
    
      <section class="content-header">
      <h1>

        Account Admin List
     
 
      </h1>
      <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/admin/home.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Account List</a></li>
 
      </ol>
    </section>
    <section class="content">

<br>
<span id="result1"></span>
<div class="row">

        <div class="col-xs-12">
        
          <div class="box">
            <div class="box-header">
         <div class="col-md-5">
                    <input type="text"  class="form-control" id="txtusernamesearch" placeholder="Type Name" />
                </div>
  <div class="col-md-3">
                    <button class="btn btn-primary" id="btnseearch">Search</button>
              
                </div>
                
                <c:if test="${checksupadmin==true }">
                     <input type="hidden" class="btndelete" value="true">
                        <div class="box-tools">
      
                <div class="input-group input-group-sm" style="width: 150px;">
                    <button id="btnaddnew" class="btn btn-success pull-right">Add Admin</button>
                </div>
              </div>
                </c:if>
                
           
        
            </div>
            <!-- /.box-header -->
           
            <div class="box-body table-responsive no-padding">
			
                       <div id="loadimg">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
              <i class="fa fa-refresh fa-4x fa-spin"></i> <label class="label label-primary">loading...</label>
            </div>
            <div class="col-xs-4"></div>
          </div>
              <table class="table table-hover">
             <thead>  <tr>
                  <th>Username</th>
                  <th>Fullname</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Create date</th>
                  <th>Action</th>
                </tr>
                </thead>
                 <tbody id="tbdada">
           
     
             
           
        
               
             
              </tbody></table>
       
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
              <script id="data-template" type="x-tmpl-mustache">
                <tr>
                    <td>{{username}}</td>
                    <td>{{fullanme}}</td>
               
              
                    <td >{{email}}</td>
                    <td>{{phone}}</td>
                    <td>{{{createdate}}}</td>
                    <td>

  <button class="btn btn-sm btn-info btn-info" data-username="{{username}}"><i class="fa fa-info"></i></button>


                {{{action}}}

</td>
                </tr>
            </script>
      </div>
      </section>
      
      
      
      
      
      
      
      
      
      
      
      
      
              <div class="modal fade" id="modeladdupdate" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
       
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Account admin</h4>
            </div>
                <form method="post"  id="frmaddaccoutnadmin">
            <div class="modal-body">
            
                    <div class="form-group">
                        <label >Username:</label>
                        <input type="text" class="form-control" id="addusername" name="username">
                  
                    </div>
                    <div class="form-group">
                        <label >Password:</label>
                        <input type="password" class="form-control" id="addpassword" name="password">
                    </div>
               <div class="form-group">
                        <label >Fullname:</label>
                        <input type="text" class="form-control" id="addfullname" name="fullname">
                    </div>
                    
                         <div class="form-group">
                        <label >Email:</label>
                        <input type="text" class="form-control" id="addemail" name="email">
                    </div>
                         <div class="form-group">
                        <label >Age:</label>
                        <input type="number" class="form-control" id="addage" name="age">
                    </div>
                    
                           <div class="form-group">
                        <label >Phone:</label>
                        <input type="text" class="form-control" id="addphone" name="phone">
                    </div>
                 
               <div class="form-group">
                        <label >Address:</label>
                          <textarea rows="" cols="" id="addAddress" name="address" class="form-control"></textarea>
                    </div>
                    
                    
            </div>
            <div class="modal-footer">
                <button  class="btn btn-primary" id="btnSave">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>

    </div>
</div>
      
      
      
      
      
      
      
      
      
      
      
        <div class="modal fade" id="modeldetailsaccount" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
       
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Account admin details</h4>
            </div>
            
            <div class="modal-body">
            
                    <div class="form-group">
                        <label for="txtname" class="formattile">Username : </label>&nbsp;&nbsp; <span class="fotmatsde"   id="txtusername"></span>
                    
                     
                      
                    </div>
                    <div class="form-group">
                                 <label for="txtname" class="formattile">Fullname : </label> &nbsp;&nbsp; <span class="fotmatsde"  id="txtfullnames"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Email: </label> &nbsp;&nbsp;  <span class="fotmatsde"  id="txtemail"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Age: </label> &nbsp;&nbsp; <span class="fotmatsde"  id="txtage"></span>
                    
                    </div>
                    
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Phone: </label> &nbsp;&nbsp; <span class="fotmatsde"  id="txtphone"></span>
                    
                    </div>
                    
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Address : </label> &nbsp;&nbsp;  <span class="fotmatsde"  id="txtaddress"></span>
                    
                    </div>
                      <div class="form-group">
                                 <label for="txtname" class="formattile">Create date : </label> &nbsp;&nbsp;  <span class="fotmatsde"  id="txtcreatedateaccount"></span>
                    
                    </div>
                    
                 
                 
             
      
            </div>
            <div class="modal-footer">

   
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        
        </div>

    </div>
</div>
      