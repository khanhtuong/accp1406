<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ taglib prefix="s" uri="http://www.springframework.org/tags/form"%>
      <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<Script>
$(document).ready(function() {				
	$("#sendContact").validate({
		rules : {		
			fullName : {
				required : true,
				minlength : 6,
				maxlength : 50
			},
			email : {
				required : true,
				maxlength : 50,
				minlength : 10

			},			
			phone : {
				required : true,
				minlength : 11,
				maxlength : 20,
			},
			title : {
				required : true,
				maxlength : 100,
				minlength : 11
			},
			content : {
				required : true,
				maxlength : 250,
				minlength : 6
			}
		},		
			messages : {
				fullName : {
					minlength : "At least 6 characters !",
					maxlength : "Must be less than 50 characters !",
					required : "Do not empty !"
				},
				email : {
					minlength : "At least 10 characters !",
					required : "Do not empty !",
					maxlength : "Must be less than 50 characters !"
				},
				phone : {
					minlength : "At least 11 digits !",
					required : "Do not empty !",
					maxlength : "Must be less than 20 digits !"
				},
				title : {
					required : "Do not empty !",
					minlength : "At least 11 characters !",
					maxlength : "Must be less than 100 characters !",
				},
				content : {
					required : "Do not empty !",
					minlength : "At least 6 characters !",
					maxlength : "Must be less than 250 characters !",
				}									
			},
			submitHandler : function(form) {
				if (confirm("Data Form is valid. Do you want to submit ?")) {
					form.submit();
				}
			}		
	})
	
	});

	
</Script>
<style type="text/css">
div#map_container {
	width: 100%;
	height: 350px;
}
</style>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_A-I-p2byYWvg55vaLTjwtdcPh7AyxFQ&callback=loadMap"
  type="text/javascript"></script>
 <script>
  function loadMap() {
	 
    var latlng = new google.maps.LatLng(10.8109212, 106.68017310);
    var myOptions = {
      zoom: 15,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_container"),myOptions);

    var marker = new google.maps.Marker({
      position: latlng,
      map: map,
      title:"my hometown, Malim Nawar!"
    });

  }
</script>
<body onload="loadMap()">
<div  id="contact-page" class="container">
    	<div class="bg">
	    	<div class="row">    		
	    		<div class="col-sm-12">    			   			
					<h2 class="title text-center">Contact <strong>Us</strong></h2>    			    				    								
				</div>			 		
			</div>    	
    		<div class="row">  	
	    		<div class="col-sm-8">
	    			<div class="contact-form">
	    				<h2 class="title text-center">Get In Touch</h2>
	    				<div class="status alert alert-success" style="display: none"></div>
	    				<div><font  size="4" color="red" style="font-weight: true">${successContact}</font></div>
	    				<!-- Start Form -->
				    	<s:form id="sendContact" class="contact-form row" name="contact-form" method="post" commandName="contact"
				    	action="${pageContext.request.contextPath }/contact/add.html">
				    						
							<c:if test="${sessionScope.username !=null }">
							<div class="form-group col-md-12">
								<input name="fullName" class="form-control" 
									placeholder="Name" type="text" value="${account.fullname }">
									<s:errors  path="fullName"></s:errors>
							</div>
							<div class="form-group col-md-12">
								<input name="email" class="form-control" 
									placeholder="Email" type="email" value="${account.email }">
									<s:errors  path="email"></s:errors>
							</div>
							<div class="form-group col-md-12">
								<input name="phone" class="form-control" 
									placeholder="Phone" type="text" value="${account.phone }">
									<s:errors  path="phone"></s:errors>
							</div>
							<div class="form-group col-md-12">
								<input name="title" class="form-control" 
									placeholder="Subject" type="text">
									<s:errors  path="title"></s:errors>
							</div>
							<div class="form-group col-md-12">
								<textarea name="content" id="message" 
									class="form-control" rows="8" placeholder="Your Message Here"></textarea>
									<s:errors  path="content"></s:errors>
							</div>
							<div class="form-group col-md-12">
								<input name="submit" class="btn btn-primary pull-right"
									value="Submit" type="submit" id="btnSubmit">
							</div>
							<span id="result"></span>
						</c:if>
						
						<c:if test="${sessionScope.username == null }">
							<div class="form-group col-md-12">
								<input name="fullName" class="form-control" 
									placeholder="Name" type="text">
									<s:errors  path="fullName"></s:errors>
							</div>
							<div class="form-group col-md-12">
								<input name="email" class="form-control" 
									placeholder="Email" type="email">
									<s:errors  path="email"></s:errors>
							</div>
							<div class="form-group col-md-12">
							
								<input name="phone" class="form-control" 
									placeholder="Phone" type="text">
									<s:errors  path="phone"></s:errors>
							</div>
							<div class="form-group col-md-12">
								<input name="title" class="form-control" 
									placeholder="Subject" type="text">
									<s:errors  path="title"></s:errors>
							</div>
							<div class="form-group col-md-12">
								<textarea name="content" id="message" 
									class="form-control" rows="8" placeholder="Your Message Here"></textarea>
									<s:errors  path="content"></s:errors>
							</div>
							<div class="form-group col-md-12">
								<input name="submit" class="btn btn-primary pull-right"
									value="Submit" type="submit" id="btnSubmit">
							</div>
							<span id="result"></span>
						</c:if>
						</s:form>
				        <!-- End Form -->
				        
	    			</div>
	    		</div>
	    		<div class="col-sm-4">
	    			<div class="contact-info">
	    				<h2 class="title text-center">Contact Info</h2>
	    				<address>
	    					<p>E-Shopper Inc.</p>
							<p>935 W. Webster Ave New Streets Chicago, IL 60614, NY</p>
							<p>Newyork USA</p>
							<p>Mobile: +2346 17 38 93</p>
							<p>Fax: 1-714-252-0026</p>
							<p>Email: info@e-shopper.com</p>
	    				</address>
	    				<div id="map_container">
	    					
	    				</div>
	    			</div>
    			</div>    			
	    	</div>  
    	</div>	
    </div>
    </body>