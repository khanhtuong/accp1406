<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="title">
	<span class="title_icon"><img
		src="${pageContext.servletContext.contextPath}/resources/user/images/bullet1.gif"
		alt="" title="" /></span>Success
</div>

<div class="feat_prod_box_details">

	<div class="contact_form xacnhan">
		<div class="form_subtitle">Order Information</div>

		<div class="form_row">
			<label class="contact"><strong>Transaction Id:</strong></label>
			${result.txn_id }
		</div>

		<div class="form_row">
			<label class="contact"><strong>First Name:</strong></label>
			${result.first_name }
		</div>

		<div class="form_row">
			<label class="contact"><strong>Last Name:</strong></label>
			${result.last_name }
		</div>

		<div class="form_row">
			<label class="contact"><strong>Email:</strong></label>
			${result.payer_email }
		</div>

		<div class="form_row">
			<label class="contact"><strong>Payment Gross:</strong></label>
			${result.payment_gross }
		</div>

		<div class="form_row">
			<label class="contact"><strong>Payment Fee:</strong></label>
			${result.payment_fee }
		</div>


	</div>

</div>
