<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form method="post" action="${initParam['posturl']}">

	<input type="image" class="title-paypal" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" class="register" value="Checkout" /> <input
		type="hidden" name="upload" value="1" /> <input type="hidden"
		name="return" value="${initParam['returnurl']}" /> <input
		type="hidden" name="cmd" value="_cart" /> <input type="hidden"
		name="business" value="${initParam['business']}" />
  	
	<c:forEach var="it" items="${sessionScope.cart }" varStatus="i">
		<input type="hidden" name="item_name_${i.index + 1}"
			value="${it.product.name}" />
		<input type="hidden" name="item_number_${i.index + 1}"
			value="${it.product.productId}" />
		<input type="hidden" name="amount_${i.index + 1}" value="${it.product.price}" />
		<input type="hidden" name="quantity_${i.index + 1}"
			value="${it.quantity }" />
	</c:forEach>

</form>
</body>
</html>