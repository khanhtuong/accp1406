<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script>
	$(document).ready(function() {
		$('#myTable').dataTable();
	});
</script>

<body>
	<div class="container">
		<div class="bg">
			<div class="row">
			<li><a href="${pageContext.request.contextPath}/account/myorders/${sessionScope.username}.html"><< Back to My Orders</a></li>
				<div class="col-sm-12">
					<h2 class="title text-center">
						 <strong>Orders Detail</strong>
					</h2>
				</div>
			</div>
			
			<table id="myTable"
				class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th>Day Order</th>
						<th>Product name</th>
						<th>Price</th>
						<th>Number</th>
						<th>Status</th>
						<th>Payment</th>
						<th>Transfer</th>
					</tr>
				</thead>				

				<tbody>
					<c:forEach items="${listOrdersDetail}" var="orders">
						<tr>
							<td>${orders.orders.datecreation}</td>
							<td>${orders.product.name}</td>
							<td>${orders.price }</td>
							<td>${orders.quantity }</td>
							<td>${orders.orders.status == true? "On process" : "Done"}</td>
							<td>${orders.orders.payment.paymentName}</td>
							<td>${orders.orders.transfer.transferName}</td>
						</tr>
					</c:forEach>
				</tbody>

			</table>		
		</div>
	</div>
</body>