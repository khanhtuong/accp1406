<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>




<body>
	<div id="contact-page" class="container">
		<div class="bg">
			<div class="row">
				<div class="col-sm-4">
					<div class="contact-form">
						<h2 class="title text-center">Back</h2>	
						<ul>
							<li><a href="${pageContext.request.contextPath}/account/update/${sessionScope.username}.html"><< Back to Managed Account</a></li>
						</ul>					
					</div>
				</div>
				<div class="col-sm-8">
					<div class="contact-info">
						<h2 class="title text-center">Oders</h2>					
						<table id="myTable"
							class="table table-hover table-striped table-bordered">
							<thead>
								<tr>							
									<th>Order Id</th>
									<th>Create date</th>
									<th>Status</th>
									<th>Payment</th>
									<th>Total</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listOders}" var="oders">
									<tr>
										<td>${oders.ordersId }</td>
										<td>${oders.datecreation }</td>
										<td>${oders.status == true? "On process" : "Done"}</td>
										<td>${oders.payment.paymentName }</td>
										<td>$${oders.totalmoney }</td>
										<td><a
											href="${pageContext.request.contextPath}/account/ordersdetail/${oders.ordersId}.html">Detail</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>