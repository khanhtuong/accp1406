<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<Script>
$(document).ready(function() {				
	$("#updateForm").validate({
		rules : {		
			fullname : {
				required : true,
				minlength : 6,
				maxlength : 50
			},
			email : {
				required : true,
				maxlength : 50,
				minlength : 10

			},
			age : {
				required : true,
				max : 100,
				min : 18
			},
			phone : {
				required : true,
				maxlength : 20,
				minlength : 12
			},
			address : {
				required : true,
				maxlength : 120,
				minlength : 6
			}
		},		
			messages : {
				fullname : {
					minlength : "At least 6 characters !",
					maxlength : "Must be less than 50 characters !",
					required : "Do not empty !"
				},
				email : {
					minlength : "At least 10 characters !",
					maxlength : "Must be less than 50 characters !",
					required : "Do not empty !"
				},
				age : {
					required : "Do not empty !",
					min: "Must be greater than 18 age",
					max: "Must be less than 100 age"
				},
				phone : {
					required : "Do not empty !",
					maxlength : "Max values is 20 !",
					minlength : "Min values is 12 !"
				},
				address : {
					required : "Do not empty !",
					maxlength : "Must be less than 120 characters !",
					minlength : "At least 6 characters !"
				}
			},
			submitHandler : function(form) {
				if (confirm("Data Form is valid. Do you want to submit ?")) {
					form.submit();
				}
			}		
	})
	
	});

	
</Script>

<body>
	<div id="contact-page" class="container">
		<div class="bg">
			<div class="row">
				<div class="col-sm-8">
					<div class="contact-form">
						<h2 class="title text-center">Update Information</h2>
						<s:form method="post" commandName="account" id="updateForm"
							action="${pageContext.request.contextPath }/account/update.html">
							<div><span class="messageUpdate">${msgUpdateSS}</span></div>
							<table cellpadding="2" cellspacing="2" align="center">
								<tr>
									<td>Username:</td>
									<td class="form-control">${account.username }<s:hidden
											path="username" /></td>
								</tr>
								<tr>
									<td>Password:</td>
									<td><s:password path="password" class="form-control"
											value="${account.password }" readonly="true" /></td>
									<td><a
										href="${pageContext.request.contextPath }/account/changepassword/${sessionScope.username == null ? 'Login' : sessionScope.username }.html">
											<i class="fa fa-edit icon-test" style="font-size: 30px"></i>
									</a></td>
								</tr>
								<tr>
									<td>Full Name:</td>
									<td><s:input path="fullname" class="form-control"
											id="txtFullname" required="required" /></td>
								</tr>
								<tr>
									<td>Email:</td>
									<td><s:input path="email" class="form-control"
											required="required" type="email" id="txtEmail" /></td>
									 <td>${msgEmailError}</td>		
								</tr>
								<tr>
									<td>Phone:</td>
									<td><s:input path="phone" class="form-control"
											required="required" type="number" id="txtPhone" /></td>
								</tr>
								<tr>
									<td>Address:</td>
									<td><s:input path="address" class="form-control"
											required="required" type="text" id="txtAddress" /></td>
								</tr>
								<tr>
									<td>Age:</td>
									<td><s:input path="age" class="form-control"
											required="required" type="number" id="txtAge" /></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td><input type="submit" class="btn btn-fefault"
										value="Save"></td>
								</tr>
							</table>
						</s:form>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="contact-info">
						<h2 class="title text-center">Other Information</h2>						
						<ul>
							<li><a href="${pageContext.request.contextPath}/account/myorders/${sessionScope.username}.html">>>> My Orders</a></li>
						</ul>						
					</div>
				</div>
			</div>
		</div>
	</div>
</body>