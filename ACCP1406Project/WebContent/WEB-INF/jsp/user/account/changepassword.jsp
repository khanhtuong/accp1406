<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form"%>   

<Script>
$(document).ready(function() {
	$("#ChangePasswordForm").validate({
		rules : {
			txtpassword : {
				minlength : 6,				
				required : true
			},
			txtnewPassword : {
				minlength : 6,
				required : true
			},
			txtRetypenewPassword:{
				required : true,
				minlength : 6,
				equalTo : '[name="txtnewPassword"]'
			}
		},
		messages : {
			txtpassword : {
				minlength : "At least 6 characters !",
				required : "Do not empty !"
			},
			txtnewPassword : {
				minlength : "At least 6 characters !",
				required : "Do not empty !"
			},
			txtRetypenewPassword : {
				minlength : "At least 6 characters !",
				required : "Do not empty !",
				equalTo  : "Retype not match with password"
					
			}
		},
		submitHandler : function(form) {
			if (confirm("Data Form is valid. Do you want change password ?")) {
				form.submit();
			}
		}
	})
});

</Script>  
<div id="contact-page" class="container">
    	<div class="bg">   		    		    	
	    	<div class="col-sm-12" align="center"> 
	    			<div class="contact-info">
	    				<h2 class="title text-center">Change Password</h2>
				<form method="post" id="ChangePasswordForm"
					action="${pageContext.request.contextPath }/account/changepassword.html">
					<table cellpadding="2" cellspacing="2" align="center">												 
						<tr>
							<td>Password:</td>
							<td><input name="txtpassword1" id="txtpassword1" class="form-control"
								required="required" type="password" /></td>
						
						</tr>
						<tr>
							<td>New Password:</td>
							<td><input name="txtnewPassword" id="txtnewPassword" class="form-control "
								required="required" type="password" /></td>
								
						</tr>
						<tr>
							<td>Retype New Password:</td>
							<td><input name="txtRetypenewPassword" class="form-control"
								required="required" type="password" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td><input type="submit" value="Save" class="btn btn-fefault"
								onclick="validateChangePasswordForm();"></td>
						</tr>
					</table>
				</form>
			</div>
    			</div>  
    	</div>	
    	
    </div>
    
    