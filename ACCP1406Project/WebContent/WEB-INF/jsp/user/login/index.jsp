<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form"%>
<!--form-->
<script type="text/javascript">


	$(document).ready(function() {
		$("#loginForm").validate({
			rules : {
				txtUserName : {
					minlength : 6,
					required : true,
					maxlength : 100
				},
				txtPassword : {
					minlength : 6,
					required : true,
					maxlength: 100
				}
			},
			messages : {
				txtUserName : {
					minlength : "At least 6 characters !",
					required : "Do not empty !",
					maxlength: "Must be less than 100 characters!"
				},
				txtPassword : {
					minlength : "At least 6 characters !",
					required : "Do not empty !",
					maxlength: "Must be less than 100 characters!"
				}
			},
			submitHandler : function(form) {
				if (confirm("Data Form is valid. Do you want to submit ?")) {
					form.submit();
				}
			}
		})

		$("#FormRegister").validate({
			rules : {
				username : {
					minlength : 6,
					required : true,
					maxlength : 100
				},
				fullname : {
					minlength : 6,
					required : true,
					maxlength : 100
				},
				address : {
					minlength : 6,
					required : true,
					maxlength : 100
				}
				,
				age : {
					required : true,
					min: 18,
					max: 100
				},
				password : {
					minlength : 6,
					required : true,
					maxlength : 100
				},
				email : {
					required : true,
					maxlength : 100,
					minlength : 6
				},
				phone : {
					minlength : 12,
					required : true,
					maxlength: 16
				},
				retypePassword : {
					required : true,
					equalTo : '[name="password"]'
				}

			},
			messages : {
				username : {
					minlength : "At least 6 characters !",
					required : "Do not empty !",
					maxlength: "Must be less than 100 characters!"
				},
				fullname : {
					minlength : "At least 6 characters !",
					required : "Do not empty !",
					maxlength: "Must be less than 100 characters!"
				},
				address:{
					minlength : "At least 6 characters !",
					required : "Do not empty !",
					maxlength: "Must be less than 100 characters!"
				},
				age : {
					required : "Do not empty !",
					min: "Must be greater than 18 age",
					max: "Must be less than 100 age"
				},
				password : {
					minlength : "At least 6 characters !",
					required : "Do not empty !",
					maxlength: "Must be less than 100 characters!"
				},
				email : {
					required : "Do not empty !",
					maxlength: "Must be less than 100 characters!",
					minlength : "At least 6 characters !"
				},
				phone : {
					minlength : "Must be greater than 12 digits",
					required : "Do not empty !",
					maxlength: "Must be less than 16 digits"
				},
				retypePassword : {
					required : "Do not empty !",
					equalTo : "Retype not match with password"
				}

			},
			submitHandler : function(form) {
				if (confirm("Data Form is valid. Do you want to submit ?")) {
					form.submit();
				
				}
			}

		})
	});
	
	
		

		
	
</script>
<div class="container">

	<div class="row">
		<div class="col-sm-4 col-sm-offset-1">
			<div class="login-form">
				<!--login form-->
				<h2>Login to your account</h2>
				<form action="${pageContext.request.contextPath}/login/login1.html" method="POST" id="loginForm">
					<input placeholder="User Name" name="txtUserName" type="text" /> 
					<input placeholder="Password" name="txtPassword" type="password"/> 
					<div> ${msg}</div>
					<span> 
						<input class="checkbox" type="checkbox" /> Keep me signed in
					</span>
					<button type="submit" class="btn btn-default" >Login</button>
				</form>
			</div>
			<!--/login form-->
		</div>
		<div class="col-sm-1">
			<h2 class="or">OR</h2>
		</div>
		<div class="col-sm-4">
			<div class="signup-form">
				<!--sign up form-->
				<h2>New User Signup!</h2>
				<s:form commandName="account"
					id="FormRegister"
					action="${pageContext.request.contextPath}/login/register.html"
					method="POST" >
					<s:input placeholder="User Name" type="text"
						path="username"  />
					<s:errors path="username"></s:errors>
					<s:input placeholder="Full Name" type="text" id="txtFullName"
						path="fullname" />
					<s:errors path="fullname"></s:errors>
					<s:input placeholder="Address" type="text" id="txtAddress"
						path="address" />
					<s:errors path="address"></s:errors>
					<s:input placeholder="Age" type="text" id="mycalendar" path="age" />
					<s:errors path="age"></s:errors>
					<s:input placeholder="Email Address" type="email" id="txtEmail"
						path="email" />
					<s:errors path="email"></s:errors>
					<s:input placeholder="Phone" type="number" id="txtPhone"
						path="phone" />
					<s:errors path="phone"></s:errors>
					<s:input placeholder="Password" type="password" id="txtPassword"
						path="password" />
					<s:errors path="password"></s:errors>
					<input placeholder="Retype Password" name="retypePassword" id="txtRetypePassword"
						type="password" />
					<button type="submit" class="btn btn-default"  >Signup</button>
				</s:form>
			</div>
			<!--/sign up form-->
		</div>
	</div>
</div>
