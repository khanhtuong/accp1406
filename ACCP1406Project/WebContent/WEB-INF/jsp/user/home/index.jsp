<%@page import="com.cnc.model.ProductModel"%>
<%@page import="com.cnc.controller.user.HomeController"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <script>  
        $(document).on('show.bs.modal', '#confirm-addToCart',function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });

        function addToCart(productId) {
    		$.ajax({
    			type : "GET",
    			data : {
    				
    			},
    			url : "${pageContext.request.contextPath}/product/buy/"+productId+".html",
    			success : function(res) {
    				
    				

    			},

    		})

    	}
            
</script>
<div class="modal fade" id="confirm-addToCart" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				
				<h4 class="modal-title" id="myModalLabel">Confirm</h4>
			</div>

			<div class="modal-body">
				<p>Products have been successfully added to your cart.</p>
				<p class="debug-url"></p>
			</div>

			<div class="modal-footer">
				<a class="btn btn-danger " href="${pageContext.request.contextPath }/home.html">Continue Shopping</a>
				<a class="btn btn-danger btn-ok">Go to cart</a>
			</div>
		</div>
	</div>
</div>
<section id="slider">
	<!--slider-->
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div id="slider-carousel" class="carousel slide"
					data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#slider-carousel" data-slide-to="0" class=""></li>
						<li data-target="#slider-carousel" data-slide-to="1"
							class="active"></li>
						<li data-target="#slider-carousel" data-slide-to="2"></li>
					</ol>

					<div class="carousel-inner">
						<div class="item">
							<div class="col-sm-6">
								<h1>
									<span>Online</span>- Book Store
								</h1>
								<h2>Free E-Commerce Template21323 </h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
									sed do eiusmod tempor incididunt ut labore et dolore magna
									aliqua.</p>
								<button type="button" class="btn btn-default get">Get
									it now</button>
							</div>
							<div class="col-sm-6">
								<img src="${pageContext.request.contextPath }/assets/user/images/banner/banner2.jpg" class="girl img-responsive"
									alt="">
							</div>
						</div>
						<div class="item active">
							<div class="col-sm-6">
								<h1>
									<span>Online</span>- Book Store
								</h1>
								<h2>100% Responsive Design</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
									sed do eiusmod tempor incididunt ut labore et dolore magna
									aliqua.</p>
								<button type="button" class="btn btn-default get">Get
									it now</button>
							</div>
							<div class="col-sm-6">
								<img src="${pageContext.request.contextPath }/assets/user/images/banner/Journal-Banner.jpg" class="girl img-responsive"
									alt="">  
							</div>
						</div>

						<div class="item">
							<div class="col-sm-6">
								<h1>
									<span>Online</span>- Book Store
								</h1>
								<h2>Free Ecommerce Template</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
									sed do eiusmod tempor incididunt ut labore et dolore magna
									aliqua.</p>
								<button type="button" class="btn btn-default get">Get
									it now</button>
							</div>
							<div class="col-sm-6">
								<img src="${pageContext.request.contextPath }/assets/user/images/banner/banner1.jpg" class="girl img-responsive"
									alt="">
							</div>
						</div>

					</div>

					<a href="#slider-carousel" class="left control-carousel hidden-xs"
						data-slide="prev"> <i class="fa fa-angle-left"></i>
					</a> <a href="#slider-carousel"
						class="right control-carousel hidden-xs" data-slide="next"> <i
						class="fa fa-angle-right"></i>
					</a>
				</div>

			</div>
		</div>
	</div>
</section>
<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="left-sidebar">
					<h2>Category  </h2>
					<div class="panel-group category-products" id="accordian">
						<!--category-productsr-->
						<c:forEach var="category" items="${listCategory }">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="${pageContext.request.contextPath }/product/category/${category.categoryId}.html">${category.name }</a>
									</h4>
								</div>
							</div>
						</c:forEach>
					</div>
					<!--/category-products-->

					<div class="brands_products">
						<!--brands_products-->
						<h2>Brands</h2>
						<div class="brands-name">
							<c:forEach items="${listSuppiler}" var="supplier">
								<ul class="nav nav-pills nav-stacked">
									<li><a href="${pageContext.request.contextPath }/product/supplier/${supplier.supplierId}.html"> <span class="pull-right">(${supplier.products.size() })</span>${supplier.name }
									</a></li>

								</ul>
							</c:forEach>
						</div>
					</div>
					<!--/brands_products-->

					<div class="price-range">
						<!--price-range-->
						<h2>Price Range</h2>
						<div class="well text-center">
							<div class="slider slider-horizontal" style="width: 163px;">
								<div class="slider-track">
									<div class="slider-selection"
										style="left: 41.6667%; width: 33.3333%;"></div>
									<div class="slider-handle round left-round"
										style="left: 41.6667%;"></div>
									<div class="slider-handle round" style="left: 75%;"></div>
								</div>
								<div class="tooltip top" style="top: -30px; left: 62.5833px;">
									<div class="tooltip-arrow"></div>
									<div class="tooltip-inner">250 : 450</div>
								</div>
								<input type="text" class="span2" value="" data-slider-min="0"
									data-slider-max="600" data-slider-step="5"
									data-slider-value="[250,450]" id="sl2">
							</div>
							<br> <b class="pull-left">$ 0</b> <b class="pull-right">$
								600</b>
						</div>
					</div>
					<!--/price-range-->

					<div class="shipping text-center">
						<!--shipping-->
						<img src="${pageContext.request.contextPath }/assets/user/images/home/shipping.jpg" alt="">
					</div>
					<!--/shipping-->

				</div>
			</div>








			<div class="col-sm-9 padding-right">
				<div class="features_items">
					<!--features_items-->
					<h2 class="title text-center">Features Items </h2>
					<c:forEach var="product" items="${listProductBy6Item }">
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center img-test">
										<img class="test4" src="${pageContext.request.contextPath }/assets/admin/img/${product.photo}" alt="">
										<h2>$ ${product.price}</h2>
										<p>${product.name}</p>
										<c:if test="${product.quantity > 0 }">
											<a href="#" class="btn btn-default add-to-cart"><i
											class="fa fa-shopping-cart"></i>Add to cart</a>
										</c:if>
										<c:if test="${product.quantity <= 0 }">
											<a href="#" class="btn btn-default add-to-cart disabled"><i
											class="fa fa-shopping-cart"></i>Product is empty</a>
										</c:if>
											
									</div>
									<div class="product-overlay">
										<div class="overlay-content">
											<a
												href="${pageContext.request.contextPath }/product/detail/${product.productId}.html">
												<h2>$ ${product.price}</h2>
												<p>${product.name}</p>
											</a>
											<c:choose>
												<c:when test="${product.quantity != 0}">
													<a data-href="${pageContext.request.contextPath}/cart.html"
														class="btn btn-default add-to-cart"
														onclick="addToCart('${product.productId }');"
														data-toggle="modal" data-target="#confirm-addToCart"><i
														class="fa fa-shopping-cart"></i>Add to cart</a>
												</c:when>
												<c:otherwise>
													<a
														href="${pageContext.request.contextPath}/product/buy/${product.productId}.html"
														class="btn btn-default add-to-cart disabled"><i
														class="fa fa-shopping-cart"></i>Product is empty</a>
												</c:otherwise>

											</c:choose>										

										

										</div>
									</div>
								</div>
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href="#"><i class="fa fa-plus-square"></i>Add
												to wishlist</a></li>
										<li><a href="#"><i class="fa fa-plus-square"></i>Add
												to compare</a></li>
									</ul>
								</div>
							</div>
						</div>
					</c:forEach>
					
					
					
					
					

				</div>
				<!--features_items-->

		
				<div class="category-tab">
					<!--category-tab-->
					<div class="col-sm-12">
						<ul class="nav nav-tabs">
							<c:forEach var="sp" items="${listSuppiler}" varStatus = "status">
								
								<li class="${status.first? 'active':'none'}"><a href="#${sp.name}"  data-toggle="tab">${sp.name}</a></li>
							
							</c:forEach>
						</ul>
					</div>
					
					<div class="tab-content">
					
						<c:forEach var="sp1" items="${listSuppiler }" varStatus="st1">
							<div class="tab-pane fade ${st1.first? 'active in':''}" id="${sp1.name}">
								<c:set var="s" value="0"></c:set>
								<c:forEach var="sp2" items="${sp1.products}">
									<c:if test="${s < 4 }">
										<div class="col-sm-3">
											<div class="product-image-wrapper">
												<div class="single-products">
													<div class="productinfo text-center img-size">
														<a href="${pageContext.request.contextPath }/product/detail/${sp2.productId}.html" >
														<img class="test3"
															src="${pageContext.request.contextPath }/assets/admin/img/${sp2.photo}"
															alt="">
														</a>	 
														<h2>$ ${sp2.price}</h2>
														<p>${sp2.name}</p>
														<c:if test="${sp2.quantity > 0 }">
															<a
																data-href="${pageContext.request.contextPath}/cart.html"
																class="btn btn-default add-to-cart"
																onclick="addToCart('${sp2.productId }');"
																data-toggle="modal" data-target="#confirm-addToCart"><i
																class="fa fa-shopping-cart"></i>Add to cart</a>

														</c:if>
														<c:if test="${sp2.quantity <=0  }">
															<a
																data-href="${pageContext.request.contextPath}/cart.html"
																class="btn btn-default add-to-cart disabled"
																onclick="addToCart('${sp2.productId }');"
																data-toggle="modal" data-target="#confirm-addToCart"><i
																class="fa fa-shopping-cart"></i>Add to cart</a>
														</c:if>

													</div>
	
												</div>
											</div>
										</div>
									</c:if>
									<c:set var="s" value="${s=s+1}"></c:set>
								</c:forEach>
							</div>
						</c:forEach>
						
					</div>
				</div>
				<!--/category-tab-->

				<div class="recommended_items">
					<!--recommended_items-->
					<h2 class="title text-center">recommended items</h2>

					<div id="recommended-item-carousel" class="carousel slide"
						data-ride="carousel">
						<div class="carousel-inner">
							<div class="item">
							<c:forEach var="item1" items="${listProductBy3ItemName1 }">
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center img-size">
												<a href="${pageContext.request.contextPath }/product/detail/${item1.productId}.html"><img class="test" src="${pageContext.request.contextPath }/assets/admin/img/${item1.photo}" alt=""></a>
												<h2>$ ${item1.price }</h2>
												<p>${item1.name }</p>
												<c:if test="${item1.quantity > 0 }">
													<a data-href="${pageContext.request.contextPath}/cart.html"
														class="btn btn-default add-to-cart"
														onclick="addToCart('${item1.productId }');"
														data-toggle="modal" data-target="#confirm-addToCart"><i
														class="fa fa-shopping-cart"></i>Add to cart </a>
												</c:if>
												<c:if test="${item1.quantity <= 0 }">
													<a data-href="${pageContext.request.contextPath}/cart.html"
														class="btn btn-default add-to-cart disabled"
														onclick="addToCart('${item1.productId }');"
														data-toggle="modal" data-target="#confirm-addToCart"><i
														class="fa fa-shopping-cart"></i>Product is empty </a>
												
												</c:if>
												
												</div>

										</div>
									</div>
								</div>
							</c:forEach>	
							</div>
							<div class="item active">
							<c:forEach var="item2" items="${listProductBy3ItemName2 }">
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center img-size">
												<a href="${pageContext.request.contextPath }/product/detail/${item2.productId}.html"><img class="test" src="${pageContext.request.contextPath }/assets/admin/img/${item2.photo}" alt="">
																									<h2>$ ${item2.price }</h2>
												<p>${item2.name }</p></a>
												<c:if test="${item2.quantity > 0}">
													<a data-href="${pageContext.request.contextPath}/cart.html"
														class="btn btn-default add-to-cart"
														onclick="addToCart('${item2.productId }');"
														data-toggle="modal" data-target="#confirm-addToCart"><i
														class="fa fa-shopping-cart"></i>Add to cart</a>
												</c:if>
												<c:if test="${item2.quantity <= 0}">
													<a data-href="${pageContext.request.contextPath}/cart.html"
														class="btn btn-default add-to-cart disabled"
														onclick="addToCart('${item2.productId }');"
														data-toggle="modal" data-target="#confirm-addToCart"><i
														class="fa fa-shopping-cart"></i>Product is empty</a>
												</c:if>		
												</div>

										</div>
									</div>
								</div>
							</c:forEach>	
							</div>
						</div>
						<a class="left recommended-item-control"
							href="#recommended-item-carousel" data-slide="prev"> <i
							class="fa fa-angle-left"></i>
						</a> <a class="right recommended-item-control"
							href="#recommended-item-carousel" data-slide="next"> <i
							class="fa fa-angle-right"></i>
						</a>
					</div>
				</div>
				<!--/recommended_items-->

			</div>
		</div>
	</div>
</section>
