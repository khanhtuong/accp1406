<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">

	
	function editQuantity(productId) {
		var quantity = parseInt($("#" + productId).val());
		
		var object1 = {
			"name" : quantity
		};

		
		if (quantity > 0) {
			var bill = $.extend(true, {}, object1);
		}

		console.log(bill);
		if (quantity > 0 && quantity < 100) {
			$
					.ajax({
						type : "POST",
						data : {
							productId : productId,
							quantity : quantity
						},
						url : "${pageContext.request.contextPath}/cart/update.html",
						success : function(res) {
							window.location
									.assign("http://localhost:8080/ACCP1406springMVC/cart.html");

						},

					})

		} else {
			alert('Pleasea value greater than 0 and less than 100')
			$("#" + productId).val(bill.name);
		}

	}

	$(document).ready(
			function() {
				$(".ddd").on(
						"click",
						function() {
							var $button = $(this);
							var oldValue = $button.closest('.sp-quantity')
									.find("input.quntity-input").val();

							if ($button.text() == "+") {
								var newVal = parseFloat(oldValue) + 1;
							} else {
								// Don't allow decrementing below zero
								if (oldValue > 0) {
									var newVal = parseFloat(oldValue) - 1;
								} else {
									newVal = 0;
								}
							}

							$button.closest('.sp-quantity').find(
									"input.quntity-input").val(newVal);

						});
			});

	$(document).on('show.bs.modal', '#confirm-deleteItem', function(e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

		$("#order1Form").validate({

			rules : {
				quantity : {
					min : 0,
					max : 100,
					required : true
				}
			},
			messages : {
				quantity : {
					min : "The value must be greater than 0",
					max : "The value must be less than 100",
					required : "Do not empty !"
				}
			}

		})
	});
</script>
<div class="modal fade" id="confirm-deleteItem" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Confirm</h4>
			</div>

			<div class="modal-body">
				<p>Do you want to remove this item from cart</p>
				<p class="debug-url"></p>
			</div>

			<div class="modal-footer">
				<a class="btn btn-danger "  data-dismiss="modal">Cancel</a>
				<a class="btn btn-danger btn-ok">Oke</a>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="confirm-deleteItem" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Confirm</h4>
			</div>

			<div class="modal-body">
				<p>Do you want to remove this item from cart</p>
				<p class="debug-url"></p>
			</div>

			<div class="modal-footer">
				<a class="btn btn-danger "  data-dismiss="modal">Cancel</a>
				<a class="btn btn-danger btn-ok">Oke</a>
			</div>
		</div>
	</div>
</div>
<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
				<li><a href="${pageContext.request.contextPath }/home.html">Home </a></li>
				<li class="active">Shopping Cart</li>
			</ol>
		</div>
		<div class="table-responsive cart_info">
		<c:if test="${sessionScope.cart != null }">
		<form action="" id="order1Form">
			<table class="table table-condensed">
				<thead>
					<tr class="cart_menu">
						<td class="image">Item</td>
						<td class="description">Description</td>
						<td class="price">Price</td>
						<td class="quantity">Quantity</td>
						<td class="total">Total</td>
						<td></td>
					</tr>
				</thead>
				<tbody>
				<c:set var="sum" value="0"></c:set>
				<c:forEach var="item" items="${sessionScope.cart }" varStatus="row">
					<c:set var="sum" value="${sum + item.product.price * item.quantity }"></c:set>
					<tr>
						<td class="cart_product"><a href="${pageContext.request.contextPath }/product/detail/${item.product.productId}.html"><img class="test6"
								src="${pageContext.request.contextPath }/assets/admin/img/${item.product.photo }" alt=""></a></td>
						<td class="cart_description">
							<h4>
								<a href="${pageContext.request.contextPath }/product/detail/${item.product.productId}.html">${item.product.name }</a>
							</h4>
							<p>Web ID: ${item.product.productId }</p>
						</td>
						<td class="cart_price">
							<p>$ ${item.product.price }</p>
						</td>
						<td class="cart_quantity">
							
									<!-- Begin -->
									<div class="sp-quantity">
										
										<div class="sp-input">
											<input type="text" name="quantity"  onchange="editQuantity('${item.product.productId}')" id="${item.product.productId}"  class="quntity-input" value="${item.quantity }" />
										</div>
										 
									</div>
									<!-- End -->
							
						</td>
						<td class="cart_total">
							<p class="cart_total_price">$ ${item.product.price * item.quantity}</p>
						</td>
							<td class="cart_delete">
									<a class="cart_quantity_delete "
									 data-toggle="modal" data-target="#confirm-deleteItem"
										
										data-href="${pageContext.request.contextPath }/cart/delete/${row.index }.html"><i
										class="fa fa-times"></i></a>
										
						  </td>
						</tr>
				</c:forEach>
					
				</tbody>
				<tr>
					<td colspan="6"><a class="btn btn-default test1 check_out" href="${pageContext.request.contextPath }/home.html">Continue to purchase</a>
						<a class="btn btn-default test2" href="${pageContext.request.contextPath }/cart/removeCart.html">Remove All</a>
					
					</td>
				
				</tr>
			</table>
			</form>
			</c:if>
			<c:if test="${sessionScope.cart == null }" >
				<H3>Cart is empty</H3></br>
				<a href="${pageContext.request.contextPath }/home.html">Click me </a>to return home page
			</c:if>
		</div>
	</div>
</section>
<section id="do_action">
	<div class="container">
		<div class="heading">
			<h3>What would you like to do next?</h3>
			<p>Choose if you have a discount code or reward points you want
				to use or would like to estimate your delivery cost.</p>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="chose_area">
					<ul class="user_option">
						<li><input type="checkbox"> <label>Use Coupon
								Code</label></li>
						<li><input type="checkbox"> <label>Use Gift
								Voucher</label></li>
						<li><input type="checkbox"> <label>Estimate
								Shipping &amp; Taxes</label></li>
					</ul>
					<ul class="user_info">
						<li class="single_field"><label>Country:</label> <select>
								<option>United States</option>
								<option>Bangladesh</option>
								<option>UK</option>
								<option>India</option>
								<option>Pakistan</option>
								<option>Ucrane</option>
								<option>Canada</option>
								<option>Dubai</option>
						</select></li>
						<li class="single_field"><label>Region / State:</label> <select>
								<option>Select</option>
								<option>Dhaka</option>
								<option>London</option>
								<option>Dillih</option>
								<option>Lahore</option>
								<option>Alaska</option>
								<option>Canada</option>
								<option>Dubai</option>
						</select></li>
						<li class="single_field zip-field"><label>Zip Code:</label> <input
							type="text"></li>
					</ul>
					<a class="btn btn-default update" href="">Get Quotes</a> <a
						class="btn btn-default check_out" href="">Continue</a>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="total_area">
					<ul>
						<li>Cart Sub Total <span>$ ${sum } </span></li>
						<li>Shipping Cost <span>Free</span></li>
						<li>Total <span>$ ${sum }</span></li>
					</ul>
				 <a
						class="${sessionScope.cart.size() ==0 ? 'btn btn-default disabled margin-button' : 'btn btn-default margin-button'}" href="${pageContext.request.contextPath }/cart/redirectToCheckOut.html"
						
						>Check Out</a>
				</div>
			</div>
		</div>
	</div>
</section>