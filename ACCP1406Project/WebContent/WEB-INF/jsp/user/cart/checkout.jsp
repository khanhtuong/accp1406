<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags/form"%>
<script>
$(document).ready(function() {
	$("#inforForm").validate({
		rules : {
			customerName : {
				minlength : 6,
				required : true
			},
			customerAddress : {
				minlength : 6,
				required : true
			},
			customerMobile : {
				minlength : 6,
				required : true
			},
			customerMessage : {
				minlength : 6,
				required : true
			},
			customerEmaill : {
				minlength : 6,
				required : true
			},
			customerMobile : {
				minlength : 12,
				required : true
			}
		},
		messages : {
			customerName : {
				minlength : "At least 6 characters !",
				required : "Do not empty !"
			},
			customerAddress : {
				minlength : "At least 6 characters !",
				required : "Do not empty !"
			},customerMobile : {
				minlength : "At least 12 numbers !",
				required : "Do not empty !"
			},customerMessage : {
				minlength : "At least 6 characters !",
				required : "Do not empty !"
			},customerEmaill : {
				minlength : "At least 6 characters !",
				required : "Do not empty !"
			},customerMobile : {
				minlength : "At least 12 digits",
				required : "Do not empty !"
			}
		},
		submitHandler : function(form) {
			if (confirm("Data Form is valid. Do you want to submit ?")) {
				form.submit();
			}
		}
	})
});

</script>
<%

	HttpSession session2 = request.getSession();

	if (null == session2.getAttribute("cart")) {
		response.sendRedirect("home.html");
		 return;

	}
%>

<div class="container">
	<div class="rows">
	<s:form class="form-horizontal" id="inforForm" commandName="order" method="POST" action="${pageContext.request.contextPath }/cart/addNewOder.html">
		<div class="col-sm-4">
		    <!-- Information -->
			<div class="signup-form">
				<!--sign up form-->
				<h2>BILLING ADDRESS</h2>
				
					<div class="form-group">
						<label class="control-label col-sm-3 title-order" >Name<span class="color-required">(*)</span>:</label>
						<div class="col-sm-10">
							<s:input path="customerName" type="text" class="form-control" 
								name="customerName"
							    value="${accountTempt.username }"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3 title-order" >Email<span class="color-required">(*)</span>:</label>
						<div class="col-sm-10">
							<s:input path="customerEmaill" type="email" class="form-control" 
								name="customerEmaill"
							    value="${accountTempt.email }"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3 title-order" >Mobile<span class="color-required">(*)</span>:</label>
						<div class="col-sm-10">
							<s:input path="customerMobile" type="number" class="form-control" 
								name="customerMobile"
							    value="${accountTempt.phone }"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3">Address<span
							class="color-required">(*)</span>:
						</label>
						<div class="col-sm-10">
							<s:input path="customerAddress" type="text" class="form-control"
								value="${accountTempt.address }" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3 title-order" >Message<span class="color-required">(*)</span></label>
						<div class="col-sm-10">
							<s:textarea path="customerMessage" class="form-control" rows="5"  
								id="comment"></s:textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3" ></label>
						<div class="col-sm-10">
							<input type="submit" class="btn btn-primary " value="Order confirmation">
						</div>
					</div>
				</div>
				
 
				
			</div>
			<!--/sign up form-->
		</div>
		<!-- Transfer and payment-->
		<div class="col-sm-5">
		    <!-- Information -->
			<div class="signup-form" >
				<!--sign up form-->
				<h2>TRANSFER</h2>
					<div class="form-group">
						<label class="control-label col-sm-3 padding-infor-title">Transfer<span class="color-required">(*)</span>:</label>
						<div class="col-sm-10 padding-infor-title">
							<s:select path="transfer" items="${listTransfer }"  
						itemLabel="transferName" itemValue="transferId"></s:select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3 padding-infor-title margin-top" >Payment<span class="color-required">(*)</span>:</label>		
						<div class="col-sm-10 padding-infor-title" >
						 <label class="radio-block span">
						 	<s:radiobuttons path="payment" items="${listPayment }" itemLabel="paymentName" itemValue="paymentId"/>&nbsp;
						 </label>
						</div>
					</div>
			</div>
			
			 
		</div>
		
	</s:form>
	</div>
</div>