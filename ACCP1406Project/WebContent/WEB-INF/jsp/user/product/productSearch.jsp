<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

 <script>  
        $(document).on('show.bs.modal', '#confirm-addToCart',function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });

        function addToCart(productId) {
    		$.ajax({
    			type : "GET",
    			data : {
    				
    			},
    			url : "${pageContext.request.contextPath}/product/buy/"+productId+".html",
    			success : function(res) {
    				
    				

    			},

    		})

    	}
            
</script>
<div class="modal fade" id="confirm-addToCart" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				
				<h4 class="modal-title" id="myModalLabel">Confirm</h4>
			</div>

			<div class="modal-body">
				<p>Products have been successfully added to your cart.</p>
				<p class="debug-url"></p>
			</div>

			<div class="modal-footer">
				<a class="btn btn-danger " href="${pageContext.request.contextPath }/home.html">Continue Shopping</a>
				<a class="btn btn-danger btn-ok">Go to cart</a>
			</div>
		</div>
	</div>
</div>
<section>
	<div class="container">
		<jsp:useBean id="pagedListHolder" scope="request" type="org.springframework.beans.support.PagedListHolder">
			
		</jsp:useBean>
		<c:url value="searchProduct.html?stringSearch=${test }" var="pageLink">
			<c:param name="p" value="~"></c:param>
		</c:url>
		
		<div class="row">
			<div class="col-sm-12 ">
				<div class="features_items">
					<!--features_items-->
					<h2 class="title text-center">Features Items</h2>
					<c:forEach var="product" items="${pagedListHolder.pageList }">
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center img-test8">
										<img class="test8"
											src="${pageContext.request.contextPath }/assets/admin/img/${product.photo}"
											alt="">
										<h2>$ ${product.price}</h2>
										<p>${product.name}</p>

										<a href="#" class="btn btn-default add-to-cart"><i
											class="fa fa-shopping-cart"></i>Add to cart</a>



									</div>
									<div class="product-overlay">
										<div class="overlay-content">
											<a href="${pageContext.request.contextPath }/product/detail/${product.productId}.html">
												<h2>$ ${product.price}</h2>
												<p>${product.name }</p>
											</a> <a data-href="${pageContext.request.contextPath}/cart.html"
												class="btn btn-default add-to-cart"
												onclick="addToCart('16');" data-toggle="modal"
												data-target="#confirm-addToCart"><i
												class="fa fa-shopping-cart"></i>Add to cart</a>

										</div>
									</div>
								</div>
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href="#"><i class="fa fa-plus-square"></i>Add
												to wishlist</a></li>
										<li><a href="#"><i class="fa fa-plus-square"></i>Add
												to compare</a></li>
									</ul>
								</div>
							</div>
						</div>
					</c:forEach>
					
					<tg:paging pagedLink="${pageLink }" pagedListHolder="${pagedListHolder }"></tg:paging>
				</div>
			</div>
		</div>
	</div>
</section>