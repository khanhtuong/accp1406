<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section id="advertisement">
	<div class="container">
		<img src="${pageContext.request.contextPath}/assets/user/images/shop/advertisement.jpg" alt="">
	</div>
</section>
<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
					<div class="left-sidebar">
					<h2>Category</h2>
					<div class="panel-group category-products" id="accordian">
						<!--category-productsr-->
						<c:forEach var="category" items="${listCate}">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="${pageContext.request.contextPath }/product/category/${category.categoryId}.html">${category.name }</a>
									</h4>
								</div>
							</div>
						</c:forEach>
					</div>
					<!--/category-productsr-->

					<div class="brands_products">
						<!--brands_products-->
						<h2>Brands</h2>
						<div class="brands-name">
							<c:forEach items="${listSupp}" var="supplier">
								<ul class="nav nav-pills nav-stacked">
									<li><a href="${pageContext.request.contextPath }/product/supplier/${supplier.supplierId}.html"> <span class="pull-right">(50)</span>${supplier.name }
									</a></li>
								</ul>
							</c:forEach>
						</div>
					</div>
					<!--/brands_products-->

					<div class="price-range">
						<!--price-range-->
						<h2>Price Range</h2>
						<div class="well">
							<div style="width: 175px;" class="slider slider-horizontal">
								<div class="slider-track">
									<div style="left: 41.6667%; width: 33.3333%;"
										class="slider-selection"></div>
									<div style="left: 41.6667%;"
										class="slider-handle round left-round"></div>
									<div style="left: 75%;" class="slider-handle round"></div>
								</div>
								<div style="top: -30px; left: 69.5833px;" class="tooltip top">
									<div class="tooltip-arrow"></div>
									<div class="tooltip-inner">250 : 450</div>
								</div>
								<input style="" class="span2" value="" data-slider-min="0"
									data-slider-max="600" data-slider-step="5"
									data-slider-value="[250,450]" id="sl2" type="text">
							</div>
							<br> <b>$ 0</b> <b class="pull-right">$ 600</b>
						</div>
					</div>
					<!--/price-range-->

					<div class="shipping text-center">
						<!--shipping-->
						<img src="${pageContext.request.contextPath}/assets/user/images/home/shipping.jpg" alt="">
					</div>
					<!--/shipping-->

				</div>
			</div>

			<div class="col-sm-9 padding-right">
				<div class="features_items">
					<!--features_items-->
					<h2 class="title text-center">Features Items</h2>					
					<c:forEach var="product" items="${listCateWithId}">
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img src="${pageContext.request.contextPath }/assets/user/images/product-details/${product.photo}" alt="">
										<h2>$ ${product.price}</h2>
										<p>${product.name}</p>
										<a href="#" class="btn btn-default add-to-cart"><i
											class="fa fa-shopping-cart"></i>Add to cart</a>
											
									</div>
									<div class="product-overlay">
										<div class="overlay-content">
											<h2>$ ${product.price}</h2>
											<p>${product.name}</p>
											<c:choose>
												<c:when test="${product.quantity != 0}">
													<a														
														data-href="${pageContext.request.contextPath}/cart.html"
														class="btn btn-default add-to-cart"
														onclick="addToCart('${product.productId }');"
														 data-toggle="modal" data-target="#confirm-addToCart"
														><i
														class="fa fa-shopping-cart"></i>Add to cart</a>
												</c:when>
												<c:otherwise>
													<a href="${pageContext.request.contextPath}/product/buy/${product.productId}.html" class="btn btn-default add-to-cart"><i
														class="fa fa-shopping-cart"></i>Product is empty</a>
												</c:otherwise>
											</c:choose>										
										</div>
									</div>
								</div>
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href="#"><i class="fa fa-plus-square"></i>Add
												to wishlist</a></li>
										<li><a href="#"><i class="fa fa-plus-square"></i>Add
												to compare</a></li>
									</ul>
								</div>
							</div>
						</div>
					</c:forEach>
									

					<ul class="pagination">
						<li class="active"><a href="">1</a></li>
						<li><a href="">2</a></li>
						<li><a href="">3</a></li>
						<li><a href="">�</a></li>
					</ul>
				</div>
				<!--features_items-->
			</div>
		</div>
	</div>
</section>