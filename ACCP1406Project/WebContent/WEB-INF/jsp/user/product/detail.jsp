<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
	$(document).ready(function() {
		$("#orderForm").validate({

			rules : {
				txtQuantity : {
					min : 1,
					max : 100,
					required : true
				}
			},
			messages : {
				txtQuantity : {
					min : "The value must be greater than 0",
					max : "The value must be less than 100",
					required : "Do not empty !"
				}
			}

		})
	});
</script>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId=1256615977692992";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="left-sidebar">
					<h2>Category</h2>
					<div class="panel-group category-products" id="accordian">
						<!--category-productsr-->
						<c:forEach var="category" items="${listCategory }">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="${pageContext.request.contextPath }/product/category/${category.categoryId}.html">${category.name }</a>
									</h4>
								</div>
							</div>

						</c:forEach>
						
					</div>
					<!--/category-products-->

					<div class="brands_products">
						<!--brands_products-->
						<h2>Brands</h2>
						<div class="brands-name">
							<c:forEach items="${listSuppiler}" var="supplier">
								<ul class="nav nav-pills nav-stacked">
									<li><a href="${pageContext.request.contextPath }/product/supplier/${supplier.supplierId}.html"><span class="pull-right">(${supplier.products.size()})</span>${supplier.name }
									</a></li>
								</ul>
							</c:forEach>
						</div>
					</div>
					<!--/brands_products-->

					<div class="price-range">
						<!--price-range-->
						<h2>Price Range</h2>
						<div class="well">
							<div style="width: 175px;" class="slider slider-horizontal">
								<div class="slider-track">
									<div style="left: 41.6667%; width: 33.3333%;"
										class="slider-selection"></div>
									<div style="left: 41.6667%;"
										class="slider-handle round left-round"></div>
									<div style="left: 75%;" class="slider-handle round"></div>
								</div>
								<div style="top: -30px; left: 69.5833px;" class="tooltip top">
									<div class="tooltip-arrow"></div>
									<div class="tooltip-inner">250 : 450</div>
								</div>
								<input style="" class="span2" value="" data-slider-min="0"
									data-slider-max="600" data-slider-step="5"
									data-slider-value="[250,450]" id="sl2" type="text">
							</div>
							<br> <b>$ 0</b> <b class="pull-right">$ 600</b>
						</div>
					</div>
					<!--/price-range-->

					<div class="shipping text-center">
						<!--shipping-->
						<img src="${pageContext.request.contextPath}/assets/user/images/home/shipping.jpg" alt="">
					</div>
					<!--/shipping-->

				</div>
			</div>

			<div class="col-sm-9 padding-right">
				<div class="product-details">
					<!--product-details-->
					<div class="col-sm-5">
						<div class="view-product">
							<img src="${pageContext.request.contextPath }/assets/admin/img/${product.photo }" width="400px" height="400px" alt="">
							<h3>ZOOM</h3>
						</div>
						<div id="similar-product" class="carousel slide"
							data-ride="carousel">

							
							
						</div>

					</div>
					<div class="col-sm-7">
					
						<div class="product-information">
							<form method="get" id="orderForm" action="${pageContext.request.contextPath }/product/buyInDetail/${product.productId }.html">
								<!--/product-information-->
								<img
									src="${pageContext.request.contextPath}/assets/user/images/product-details/new.jpg"
									class="newarrival" alt="">
								<h2>${product.name}</h2>
								<p>Web ID: ${product.productId }</p>
								<img
									src="${pageContext.request.contextPath}/assets/user/images/product-details/rating.png"
									alt=""> <span> <span>US $${product.price }</span>
									<label>Quantity:</label> <input name="txtQuantity" id="txtQuantity" value="1" type="text">
									<c:if test="${product.quantity > 0}">
										<button type="submit" class="btn btn-fefault cart">
											<i class="fa fa-shopping-cart"></i> Add to cart
										</button>
									</c:if>
								

								</span>
								<p>
									<b>Availability:</b>${product.quantity >0? " In stock": " Out of stock" }
								</p>
								<p>
									<b>Condition:</b> New
								</p>
								<p>
									<b>Brand:</b> E-SHOPPER
								</p>
								<a href=""><img
									src="${pageContext.request.contextPath}/assets/user/images/product-details/share.png"
									class="share img-responsive" alt=""></a>
							</form>
						</div>
						<!--/product-information-->
					</div>
				</div>
				<!--/product-details-->

				<div class="category-tab shop-details-tab">
					<!--category-tab-->
					<div class="col-sm-12">
						<ul class="nav nav-tabs">
							<li><a href="#details" data-toggle="tab">Details</a></li>
							<li><a href="#productInfo" data-toggle="tab">Product information</a></li>
							<li class="active"><a href="#reviews" data-toggle="tab">Reviews
									(5)</a></li>
						</ul>
					</div>
					<div class="tab-content">
						<div class="tab-pane fade" id="details">
							<p>${product.description}</p>
	
						</div>
						<div class="tab-pane fade" id="productInfo">
							<div class="col-sm-12">
								<table class="table">
									<tbody>
										<tr class="success">
											<td>Product code</td>
											<td>${product.productId}</td>
										</tr>
										<tr class="danger">
											<td>Publishing company</td>
											<td>${product.supplier.name }</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="tab-pane fade active in" id="reviews">
							<div class="col-sm-12">
								<ul>
									<li><a href=""><i class="fa fa-user"></i>EUGEN</a></li>
									<li><a href=""><i class="fa fa-clock-o"></i>12:41 PM</a></li>
									<li><a href=""><i class="fa fa-calendar-o"></i>31 DEC
											2014</a></li>
								</ul>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
									sed do eiusmod tempor incididunt ut labore et dolore magna
									aliqua.Ut enim ad minim veniam, quis nostrud exercitation
									ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis
									aute irure dolor in reprehenderit in voluptate velit esse
									cillum dolore eu fugiat nulla pariatur.</p>
								<p>
									<b>Write Your Review</b>
								</p>

								<div class="fb-comments"
								data-href="http://localhost:8080/ACCP1406springMVC/product/detail/${product.productId}.html"
								data-numposts="10"></div>
							</div>
						</div>

					</div>
				</div>
				<!--/category-tab-->

				
			</div>
		</div>
	</div>
</section>
</body>