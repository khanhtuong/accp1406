package help;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

public class UploadFilehelper {

	

	public static String simpleUpload(MultipartFile file, HttpServletRequest request, boolean encrypt_file_name,
			String upload_folder) {
		String filename = null;
		try {
			if (!file.isEmpty()) {
				String applicationPath = request.getServletContext().getRealPath("");
				if (encrypt_file_name) {
					String currentFilename = file.getOriginalFilename();
					String extension = currentFilename.substring(currentFilename.lastIndexOf("."),
							currentFilename.length());
					Long nameRadom = Calendar.getInstance().getTimeInMillis();
					String newFilename = nameRadom + extension;
					filename = newFilename;
				} else
					filename = file.getOriginalFilename();
				byte[] bytes = file.getBytes();
				String rootPath = applicationPath;
				String basePath = applicationPath + File.separator + upload_folder;
				System.out.println(basePath);
				File dir = new File(rootPath + File.separator + upload_folder);
				if (!dir.exists()) {
					dir.mkdirs();
				}

				File serverfile = new File(dir.getAbsolutePath() + File.separator + filename);
				
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverfile));
				//muon hien thi file upload len da viet ham doc file bufferefoututstream
				stream.write(bytes);
				stream.close();
				return filename;

			} else {
				filename = null;
			}

		} catch (Exception e) {
			filename = null;
		}
		return filename;

	}
	
	
	

}
