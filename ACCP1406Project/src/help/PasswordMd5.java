package help;

import java.math.BigInteger;
import java.security.MessageDigest;

public class PasswordMd5 {
	public static String MD5(String content) {
		try {

			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messagedigest = md.digest(content.getBytes());
			BigInteger number = new BigInteger(1, messagedigest);
			String hashtext = number.toString(16);
			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			return hashtext;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	
	}
	
	
	public static void main(String[] args){
		
		String s = "123";
		String mahoa =  MD5(s);
		System.out.println(mahoa);
	}
}




