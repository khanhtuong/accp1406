package com.cnc.controller.user;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import javax.servlet.http.HttpSession;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.cnc.entities.Account;
import com.cnc.entities.FeedBack;
import com.cnc.model.AccountModel;
import com.cnc.model.ContactModel;
import com.cnc.validater.user.ContactValidator;

@Controller
@RequestMapping("contact")
@SessionAttributes
public class ContactController {

	ContactModel contactModel = new ContactModel();	
	AccountModel accountModel = new AccountModel();
	
	@RequestMapping(method = RequestMethod.GET)
	public String index(ModelMap model, HttpSession session){
		try {
			String name =session.getAttribute("username").toString(); 
			if(name != null){
				model.put("account", accountModel.find(session.getAttribute("username").toString()));
			}
			
			FeedBack feedBack = new FeedBack();
			model.put("contact", feedBack);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return "contact.index";

	 }


	@RequestMapping(value = "add", method = RequestMethod.POST)
	public String add(@ModelAttribute("contact") @Valid FeedBack contact, BindingResult bindingResult,
			HttpServletRequest request, ModelMap model) {
		ContactValidator contactValidator = new ContactValidator();
		contactValidator.validate(contact, bindingResult);
		if (bindingResult.hasErrors()) {
			return "contact.index";
		} else {
			ContactModel contactModel = new ContactModel();
			contact.setCreateDate(new Date());
		
			contactModel.create(contact);

			model.put("msgUpdateSS", "Send feedback success!!!");
			return "account.changPasswordInfo";
		}
	}
	 	
	
}
