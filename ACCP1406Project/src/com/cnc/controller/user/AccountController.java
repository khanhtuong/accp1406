package com.cnc.controller.user;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.cnc.entities.Account;
import com.cnc.model.AccountModel;
import com.cnc.model.OderModel;
import com.cnc.model.OrdersDetailModel;

import help.PasswordMd5;

@Controller
@RequestMapping("account")
@SessionAttributes
public class AccountController {
	private AccountModel accountModel = new AccountModel();
	private static OderModel oderModel = new OderModel();
	private OrdersDetailModel oderDetailModel = new OrdersDetailModel();

	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") String user, ModelMap modelMap) {
		AccountModel accountModel = new AccountModel();
		Account account = new Account();
		account = accountModel.getAccountByName(user);
		System.out.println(account.toString());
		modelMap.put("account", account);

		return "account.update";
	}

	@RequestMapping(value = "update", method = RequestMethod.POST)
	public String update(@ModelAttribute("account") Account account, ModelMap modelMap, HttpSession session) {

		if (this.accountModel.getAccountByEmail(account.getEmail()) == null) {
			AccountModel accountModel = new AccountModel();
			account.setCreatedate(new Date());
			accountModel.update(account);

			modelMap.put("msgUpdateSS", "Update information success !!");
			return "account.changPasswordInfo";
		} else {
			String EmailTest = this.accountModel.find(session.getAttribute("username").toString()).getEmail();

			if (account.getEmail().equals(EmailTest)) {
				AccountModel accountModel = new AccountModel();
				account.setCreatedate(new Date());
				accountModel.update(account);
				modelMap.put("msgUpdateSS", "Update information success !!");
				return "account.changPasswordInfo";
			} else {
				modelMap.put("listOders",
						this.oderModel.getOdersByUserName(session.getAttribute("username").toString()));
				modelMap.put("msgEmailError", "This email is exist");
				return "account.update";

			}

		}

	}

	@RequestMapping(value = "changepassword/{id}", method = RequestMethod.GET)
	public String changepassword(@PathVariable("id") String user, ModelMap modelMap) {
		AccountModel accountModel = new AccountModel();
		Account account = accountModel.find(user);
		modelMap.put("account11", account);
		return "account.changepassword";
	}

	@RequestMapping(value = "changepassword", method = RequestMethod.POST)
	public String changepassword(HttpSession session, HttpServletRequest request, ModelMap modelMap) {
		String userName = session.getAttribute("username").toString();
		String password = request.getParameter("txtpassword1");

		if (!userName.isEmpty() && !password.isEmpty()) {
			Account accountDB = this.accountModel.getAccountByUserNameAndPassword(userName, PasswordMd5.MD5(password));
			if (accountDB != null) {
				AccountModel accountModel = new AccountModel();
				Account account2 = new Account();
				account2 = new AccountModel().find(userName);
				String newPassword = PasswordMd5.MD5(request.getParameter("txtnewPassword"));
				account2.setPassword(newPassword);
				accountModel.update(account2);
				modelMap.put("msgUpdateSS", "Update password success!!!");
				return "account.changPasswordInfo";
			} else {
				modelMap.put("msgError", "Wrong username or password");
				return "account.changepassword";
			}
		}
		return "account.changepassword";
	}

	@RequestMapping(value = "myorders/{user}", method = RequestMethod.GET)
	public String myorders(@PathVariable("user") String user, ModelMap modelMap) {
		modelMap.put("listOders", this.oderModel.getOdersByUserName(user));
		return "account.myorders";
	}

	@RequestMapping(value = "ordersdetail/{id}", method = RequestMethod.GET)
	public String orderdetail(@PathVariable("id") Integer id, ModelMap map) {
		map.put("listOrdersDetail", oderDetailModel.getOdersDetailById(id));
		return "account.ordersdetail";
	}
}
