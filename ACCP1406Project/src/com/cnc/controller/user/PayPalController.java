package com.cnc.controller.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.*;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.cnc.paypal.*;;


@Controller
@RequestMapping(value = "paypal")
@SessionAttributes

public class PayPalController {
	  private PayPalResult pr = new PayPalResult();
	@RequestMapping(method = RequestMethod.GET)
	public String index() {
		return "payment.index";
	}

	@RequestMapping(value = "/success", method = RequestMethod.GET)
	public String success(ModelMap mm, HttpServletRequest request, HttpSession httpSession) {
		PayPalSucess ps = new PayPalSucess();
		
		mm.put("result", ps.getPayPal(request));
		this.pr = ps.getPayPal(request);
		System.out.println(ps.getPayPal(request).getTxn_id());
		httpSession.removeAttribute("cart");
		return "redirect:/home.html";
	}

}
