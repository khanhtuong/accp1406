package com.cnc.controller.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.coyote.http11.Http11AprProcessor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.cnc.entities.Account;
import com.cnc.entities.Role;
import com.cnc.model.AccountModel;
import com.cnc.model.RoleModel;
import com.cnc.validater.*;
import com.cnc.validater.user.AccountValidator;
import com.mysql.fabric.xmlrpc.base.Array;


import help.PasswordMd5;


@Controller
@RequestMapping("login")
@SessionAttributes
public class LoginController {
	
	private AccountModel accountModel =  new AccountModel();
	private RoleModel roleModel = new RoleModel();
	@RequestMapping(method = RequestMethod.GET)
	public String index(ModelMap modelMap){
		modelMap.put("account", new Account());
		
		return "login.index";
	};
	
	@RequestMapping(value="login1", method = RequestMethod.POST)
	public String login(HttpServletRequest request, ModelMap modelMap, HttpSession session, HttpServletResponse response)
	{
		String userName = request.getParameter("txtUserName");
		String password = request.getParameter("txtPassword");
		if(!userName.isEmpty() && !password.isEmpty())
		{		
			Account accountDB = this.accountModel.getAccountByUserNameAndPassword(userName, PasswordMd5.MD5(password));
			if(accountDB != null){
				modelMap.put("khanh",userName );
				session.setAttribute("username", userName);
				return "redirect:/home.html";
			}else{
				modelMap.put("msg", "Wrong username or password");
				modelMap.put("account", new Account());
				return "login.index";
			}
		}
		return "redirect:/login.html";
		
	}
	@RequestMapping(value = "register", method = RequestMethod.POST)
	public String register(@ModelAttribute("account") @Valid Account account,
			BindingResult bindingResult, ModelMap modelMap, HttpServletRequest request, HttpSession session)
	{
		AccountValidator accountValidator = new AccountValidator();
		accountValidator.validate(account, bindingResult);
		if(bindingResult.hasErrors()) {
			return "login.index";
		}
		else {
			String password = request.getParameter("password");
			
			String username = request.getParameter("username").replaceAll("\\s","");
			Set<Role> listRole = new HashSet<>();
			listRole.add(this.roleModel.findByName("user"));
			account.setUsername(username);
			account.setPassword(PasswordMd5.MD5(password));
			account.setRoles(listRole);
			account.setCreatedate(new Date());
			this.accountModel.create(account);
			session.setAttribute("username", username);
			return "redirect:/home.html";
		}
	}
	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logOut(HttpSession httpSession){
		httpSession.removeAttribute("username");
		return "redirect:/login.html";
	}
}
