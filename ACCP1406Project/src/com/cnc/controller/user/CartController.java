package com.cnc.controller.user;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.cnc.entities.Account;
import com.cnc.entities.Item;
import com.cnc.entities.Orders;
import com.cnc.entities.Ordersdetail;
import com.cnc.entities.OrdersdetailId;
import com.cnc.entities.Payment;
import com.cnc.entities.Product;
import com.cnc.entities.Transfer;
import com.cnc.model.AccountModel;
import com.cnc.model.OderModel;
import com.cnc.model.OrdersDetailModel;
import com.cnc.model.PaymentModel;
import com.cnc.model.ProductModel;
import com.cnc.model.TransferModel;
import com.cnc.user.convert.PaymentEditor;
import com.cnc.user.convert.TransferEditor;


import jdk.nashorn.internal.ir.RuntimeNode.Request;

@Controller
@RequestMapping("cart")
@SessionAttributes
public class CartController {
	PaymentModel paymentModel = new PaymentModel();
	TransferModel transferModel = new TransferModel();
	AccountModel accountModel = new AccountModel();
	OderModel oderModel = new OderModel();
	ProductModel productModel = new ProductModel();
	OrdersDetailModel ordersDetailModel = new OrdersDetailModel();

	@InitBinder
	public void initBinder(WebDataBinder webDataBinder) {
		webDataBinder.registerCustomEditor(Transfer.class, new TransferEditor());
		webDataBinder.registerCustomEditor(Payment.class, new PaymentEditor());

	}

	@RequestMapping(method = RequestMethod.GET)
	public String index() {
		return "cart.index";
	}

	@RequestMapping(value = "delete/{index}", method = RequestMethod.GET)
	public String delete(@PathVariable("index") int index, HttpSession session) {
		List<Item> cart = (List<Item>) session.getAttribute("cart");
		cart.remove(index);
		session.setAttribute("cart", cart);

		return "redirect:/cart.html";
	}

	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public String update(HttpServletRequest request, HttpSession session, ModelMap momap) {
		List<Item> cart = (List<Item>) session.getAttribute("cart");
		String id = request.getParameter("productId");
		String quantity = request.getParameter("quantity");
		int index = isExists(Integer.parseInt(id), session);
		cart.get(index).setQuantity(0);
		cart.get(index).setQuantity(Integer.parseInt(quantity));
		session.setAttribute("cart", cart);
		return "cart.index";
	}

	@RequestMapping(value = "redirectToCheckOut", method = RequestMethod.GET)
	public String redirectToCheckOut(ModelMap modelMap, HttpSession httpSession, HttpServletRequest request) {
		if (httpSession.getAttribute("cart") != null) {
			if (httpSession.getAttribute("username") != null) {
				modelMap.put("accountTempt", this.accountModel.find(httpSession.getAttribute("username").toString()));
			} else {
				modelMap.put("accountTempt", new Account());
			}
			Orders orders = new Orders();
			orders.setPayment(this.paymentModel.find(1));
			modelMap.put("order", orders);
			modelMap.put("listPayment", this.paymentModel.findAll());
			modelMap.put("listTransfer", this.transferModel.findAll());
			return "cart.checkOut";
		} else {
			return "redirect:/home.html";
		}

	}

	@RequestMapping(value = "addNewOder", method = RequestMethod.POST)
	public String addNewOder(@ModelAttribute("order") Orders orders, HttpSession httpSession, ModelMap modelMap) {
		String paymentName = orders.getPayment().getPaymentName(); 
		if (paymentName.equals("PayPal")) {
			orders.setDatecreation(new Date());
			long sumPrice = 0;
			List<Item> cart = (List<Item>) httpSession.getAttribute("cart");
			if (this.accountModel.find(httpSession.getAttribute("username")) != null) {
				orders.setAccount(this.accountModel.find(httpSession.getAttribute("username").toString()));
			}
			for (Item item : cart) {
				sumPrice += item.getQuantity() * item.getProduct().getPrice();
			}
			orders.setTotalmoney(sumPrice);
			orders.setStatus(true);
			this.oderModel.create(orders);
			for (Item item : cart) {
				Ordersdetail ordersdetail = new Ordersdetail();
				ordersdetail.setId(new OrdersdetailId(item.getProduct().getProductId(), orders.getOrdersId()));
				ordersdetail.setPrice(item.getProduct().getPrice());
				ordersdetail.setQuantity(item.getQuantity());
				this.ordersDetailModel.create(ordersdetail);
			}
			return "redirect:/paypal.html";		
		}else{
			orders.setDatecreation(new Date());
			long sumPrice = 0;
			List<Item> cart = (List<Item>) httpSession.getAttribute("cart");
			if (this.accountModel.find(httpSession.getAttribute("username")) != null) {
				orders.setAccount(this.accountModel.find(httpSession.getAttribute("username").toString()));
			}
			for (Item item : cart) {
				sumPrice += item.getQuantity() * item.getProduct().getPrice();
			}
			orders.setTotalmoney(sumPrice);
			orders.setStatus(true);
			this.oderModel.create(orders);
			for (Item item : cart) {
				Ordersdetail ordersdetail = new Ordersdetail();
				ordersdetail.setId(new OrdersdetailId(item.getProduct().getProductId(), orders.getOrdersId()));
				ordersdetail.setPrice(item.getProduct().getPrice());
				ordersdetail.setQuantity(item.getQuantity());
				this.ordersDetailModel.create(ordersdetail);
			}
			httpSession.removeAttribute("cart");
			modelMap.put("msgUpdateSS", "Thanks for checkout !!!");
			return "account.changPasswordInfo";
		}
	}

	@RequestMapping(value="removeCart", method = RequestMethod.GET)
	public String removeCartAll(HttpSession httpSession){
		httpSession.removeAttribute("cart");
		return "redirect:/cart.html";
	}
	private int isExists(Integer id, HttpSession session) {
		List<Item> cart = (List<Item>) session.getAttribute("cart");
		for (int i = 0; i < cart.size(); i++)
			if (cart.get(i).getProduct().getProductId() == id)
				return i;
		return -1;
	}

	
}
