package com.cnc.controller.user;

import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import com.cnc.entities.*;
import com.cnc.model.CategoryModel;
import com.cnc.model.ProductModel;
import com.cnc.model.SupplierModel;

@Controller
@RequestMapping(value="home")
@SessionAttributes
public class HomeController {
	public static final String LIST_CATEGORY = "listCategory";
	public static final String LIST_SUPPILER = "listSuppiler";
	public static final String LIST_PRODUCT_LIMITED_BY_6_Item = "listProductBy6Item";
	public static final String LIST_PRODUCT_LIMITED_BY_3_Item_Name1 = "listProductBy3ItemName1";
	public static final String LIST_PRODUCT_LIMITED_BY_3_Item_Name2 = "listProductBy3ItemName2";
	CategoryModel categoryModel = new CategoryModel();
	SupplierModel supplierModel = new SupplierModel();
	ProductModel productModel = new ProductModel();
	@RequestMapping(method = RequestMethod.GET)
	public String index(ModelMap modelMap) { 	
		modelMap.put(HomeController.LIST_SUPPILER, supplierModel.getListSupplieres());
		modelMap.put(HomeController.LIST_CATEGORY, categoryModel.getListCategory());
		modelMap.put(HomeController.LIST_PRODUCT_LIMITED_BY_6_Item, productModel.getListProductLimitedBy6ItemAndCurrent());
		modelMap.put(HomeController.LIST_PRODUCT_LIMITED_BY_3_Item_Name1,productModel.get3ProductByCategoryNameDesc("Antiques & Collectibles"));
		modelMap.put(HomeController.LIST_PRODUCT_LIMITED_BY_3_Item_Name2,productModel.get3ProductByCategoryNameDesc("Comics & Graphic Novels"));		
		return "home.index";
	}
	
	@RequestMapping(value = "category/{id}", method = RequestMethod.GET)
	public String category(@PathVariable("id") Integer id, ModelMap modelMap) {		
		System.out.println(productModel.getProductwithIdCategory(id).size());
		modelMap.put("listCateWithId", productModel.getProductwithIdCategory(id));	
		return "home.index";	
	}	
	
	@RequestMapping(value = "supplier/{id}", method = RequestMethod.GET)
	public String supplier(@PathVariable("id") Integer id, ModelMap modelMap) {		
		modelMap.put("listSuppWithId", productModel.getProductwithIdSupplier(id));			
		return "home.index";	
	}	
	
	public static void main(String[] args){
		String test = "qweqwe qwewqe qwewqewqe  ";
		System.out.println(test.replaceAll("\\s",""));
	}
	
	

}
