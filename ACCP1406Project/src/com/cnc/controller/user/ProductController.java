package com.cnc.controller.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.cnc.entities.Account;
import com.cnc.entities.Item;
import com.cnc.entities.Product;
import com.cnc.model.AccountModel;
import com.cnc.model.CategoryModel;
import com.cnc.model.ProductModel;
import com.cnc.model.SupplierModel;


import com.sun.java.swing.plaf.motif.resources.motif;


@Controller
@RequestMapping("product")
@SessionAttributes
public class ProductController {	
	public static final String LIST_CATEGORY = "listCategory";
	public static final String LIST_SUPPILER = "listSuppiler";
	CategoryModel categoryModel = new CategoryModel();
	SupplierModel supplierModel = new SupplierModel();

	ProductModel productModel = new ProductModel();
	
	
	@RequestMapping(method = RequestMethod.GET)
	public String index(ModelMap model){
		model.put("listCate", categoryModel.findAll());
		model.put("listSupp", supplierModel.findAll());
		return "product.index";
	};
	
	@RequestMapping(value = "category/{id}", method = RequestMethod.GET)
	public String category(@PathVariable("id") Integer id, ModelMap modelMap,HttpServletRequest request) {	
		List<Product> searchByCategoryId = productModel.getProductwithIdCategory(id);
		if (searchByCategoryId.size() != 0) {
			PagedListHolder pagedListHolder = new PagedListHolder(searchByCategoryId);
			int page = ServletRequestUtils.getIntParameter(request, "p", 0);
			pagedListHolder.setPage(page);
			pagedListHolder.setPageSize(6);
			modelMap.put("listCateWithId", pagedListHolder);
			modelMap.put("idCategory", id);
			modelMap.put("listCate", categoryModel.getListCategory());
			modelMap.put("listSupp", supplierModel.getListSupplieres());
			return "product.index";
		} else {
			return "error.index";
		}
	}	
	
	@RequestMapping(value = "supplier/{id}", method = RequestMethod.GET)
	public String supplier(@PathVariable("id") Integer id, ModelMap modelMap, HttpServletRequest request) {		
		List<Product> searchSupplierById = productModel.getProductwithIdSupplier(id);
		if (searchSupplierById.size() != 0) {
			PagedListHolder pagedListHolder = new PagedListHolder(searchSupplierById);
			int page = ServletRequestUtils.getIntParameter(request, "p", 0);
			pagedListHolder.setPage(page);
			pagedListHolder.setPageSize(6);
			modelMap.put("listSuppWithId", pagedListHolder);
			modelMap.put("idSupplier", id);
			modelMap.put("listCate", categoryModel.getListCategory());
			modelMap.put("listSupp", supplierModel.getListSupplieres());
			return "product.index";
		} else {
			return "error.index";

		}
	}	
	
	@RequestMapping(value = "buy/{id}", method = RequestMethod.GET)
	public String buy(@PathVariable("id") Integer id, HttpSession session, ModelMap modelMap) {
		
		if (session.getAttribute("cart") == null) {
			List<Item> cart = new ArrayList<Item>();
			cart.add(new Item(productModel.find(id), 1));
			session.setAttribute("cart", cart);			
		} else {
			List<Item> cart = (List<Item>) session.getAttribute("cart");
			int index = isExists(id, session);
			if(index == -1)
				cart.add(new Item(productModel.find(id), 1));
			else {
				int quantity = cart.get(index).getQuantity() + 1;
				cart.get(index).setQuantity(quantity);
			}
			session.setAttribute("cart", cart);
		}
		return "cart.index";
	}
	
	@RequestMapping(value = "buyInDetail/{id}", method = RequestMethod.GET)
	public String buyInDetail(@PathVariable("id") Integer id, HttpSession session, ModelMap modelMap,
			HttpServletRequest request) {
		Product product = this.productModel.find(id);
		if(product != null)
		{
			String number = request.getParameter("txtQuantity");

			if (session.getAttribute("cart") == null) {
				List<Item> cart = new ArrayList<Item>();
				cart.add(new Item(productModel.find(id), Integer.parseInt(number)));
				session.setAttribute("cart", cart);
			} else {
				List<Item> cart = (List<Item>) session.getAttribute("cart");
				int index = isExists(id, session);
				if (index == -1)
					cart.add(new Item(productModel.find(id), Integer.parseInt(number)));
				else {
					int quantity = cart.get(index).getQuantity() + Integer.parseInt(number);
					cart.get(index).setQuantity(quantity);
				}
				session.setAttribute("cart", cart);
			}
			return "cart.index";
		} else {
			return "error.index";
		}

	}
	@RequestMapping(value="detail/{id}", method = RequestMethod.GET)
	public String detail(@PathVariable("id") Integer id, ModelMap modelMap){
		Product product = this.productModel.findProductById(id);
		if(product != null){
			modelMap.put("product", product);
			modelMap.put(ProductController.LIST_SUPPILER, supplierModel.getListSupplieres());
			modelMap.put(ProductController.LIST_CATEGORY, categoryModel.getListCategory());
		return "product.detail";
		} else{
			return "error.index";
		}
	}
	@RequestMapping(value = "sub/{id}", method = RequestMethod.GET)
	public String subQuantityItem(@PathVariable("id") Integer id, HttpSession session, ModelMap modelMap){
		if (session.getAttribute("cart") == null) {
			List<Item> cart = new ArrayList<Item>();
			cart.add(new Item(productModel.find(id), 1));
			session.setAttribute("cart", cart);			
		} else {
			List<Item> cart = (List<Item>) session.getAttribute("cart");
			int index = isExists(id, session);
			if(index == -1)
				cart.add(new Item(productModel.find(id), 1));
			else {
				int quantity = cart.get(index).getQuantity() - 1;
				cart.get(index).setQuantity(quantity);
			}
			session.setAttribute("cart", cart);
		}
		return "cart.index";
	};
	
	@RequestMapping(value = "detail", method = RequestMethod.GET)
	public String detail(){
		return "product.detail";
	};
	
	private int isExists(Integer id, HttpSession session) {
		List<Item> cart = (List<Item>) session.getAttribute("cart");
		for(int i=0; i<cart.size(); i++)
			if(cart.get(i).getProduct().getProductId() == id)
				return i;
		return -1;
	}
	
	@RequestMapping(value="searchProduct", method = RequestMethod.GET)
	public String searchProduct(HttpServletRequest request, ModelMap modelMap ){
		String stringSearch = request.getParameter("stringSearch");
		
		List<Product> products = new ArrayList<Product>();
		
		products = this.productModel.searchProduct(stringSearch);
		modelMap.put("test", stringSearch);
		PagedListHolder pagedListHolder = new PagedListHolder(products);
		int page = ServletRequestUtils.getIntParameter(request, "p", 0);
		pagedListHolder.setPage(page);
		pagedListHolder.setPageSize(6);
		modelMap.put("pagedListHolder", pagedListHolder);
		
		return "product.productSearch";	
	}
}
