package com.cnc.controller.admin;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.cnc.common.CategoryView;
import com.cnc.common.FeedBackView;
import com.cnc.common.FeedBackViewModel;
import com.cnc.common.OrdersView;
import com.cnc.common.OrdersViewModel;
import com.cnc.common.PaymentView;
import com.cnc.common.Paymentviewmodel;
import com.cnc.common.RevenueStatistic;
import com.cnc.common.TransferView;
import com.cnc.common.TransferViewModel;
import com.cnc.common.comondata;
import com.cnc.entities.Account;
import com.cnc.entities.Category;
import com.cnc.entities.Orders;
import com.cnc.entities.Ordersdetail;
import com.cnc.entities.OrdersdetailId;
import com.cnc.entities.Payment;
import com.cnc.entities.Product;
import com.cnc.entities.Role;
import com.cnc.entities.Transfer;
import com.cnc.model.AccountAdminModel;
import com.cnc.model.AccountModel;
import com.cnc.model.FeedBackAdminModel;
import com.cnc.model.OrderdetailsAdminModel;
import com.cnc.model.OrdersAdminModel;
import com.cnc.model.PaymentAdminModel;
import com.cnc.model.ProductAdminModel;
import com.cnc.model.RevenueStatisticAdminModel;
import com.cnc.model.TransferAdminModel;

@Controller
@RequestMapping(value="admin/information")

public class Informationadmin {

	
	FeedBackViewModel feedviewmodel =  new FeedBackViewModel();
	FeedBackAdminModel feedmodel =  new FeedBackAdminModel();
	OrdersViewModel ordersviewmodel =  new OrdersViewModel();
	OrdersAdminModel ordermodel =  new OrdersAdminModel();
	RevenueStatisticAdminModel revenstaticmodel =  new RevenueStatisticAdminModel();
	Paymentviewmodel paymentviewmodel =  new Paymentviewmodel();
	PaymentAdminModel paymentmodel =  new PaymentAdminModel();
	TransferAdminModel transfermodel =  new TransferAdminModel();
	TransferViewModel transferviewmodel =  new TransferViewModel();
	
	
	@RequestMapping(value="feedback", method = RequestMethod.GET)
	public String feedback(ModelMap modelMap,HttpSession session) {

		
		
		  Account account =  new AccountAdminModel().findaccountadmin( session.getAttribute("username").toString() );
			if(account!=null){
				boolean flag = false;
				for (Role rl : account.getRoles()) {
					if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
						flag = true;
						break;
					}
				}
				
				if(flag){
					modelMap.put("title", "Admin-feedback");
					return "feedbackadmin";
				}else{
					return "loginadmin";
					
				}
		
			
			}else{
				
				return "loginadmin";
			}
		
	
	}
	
	
	@RequestMapping(value="/listfeedback", method =  RequestMethod.GET)
	@ResponseBody
	public List<FeedBackView> listfeedback(HttpServletRequest request){
		String keyword =  request.getParameter("keywowrd");
	
       try {
    	   
    	
           if(keyword!=null){
        	   if(!isnnumber(keyword)){
        		   return feedviewmodel.searchfeedbackadmin(keyword,"");
        	   }else{
        		   return feedviewmodel.searchfeedbackadmin("",keyword);
        	   }
                	 
                   
            	
           }else{
        	   return feedviewmodel.findlladmin();
           }
           
    	 
    	
    	
    	   
  
	   } catch (Exception e) {
			System.out.println(e.getMessage());
		  
	           return null;
	  }
		
	}
	
	
	
	@RequestMapping(value="/searchfeed",method =  RequestMethod.GET)
	@ResponseBody
	public List<String>  searchfeed(HttpServletRequest request){
		
		return feedmodel.searchfullname(request.getParameter("term"));
	}
	
	@RequestMapping(value="/detailsfeedback",method = RequestMethod.GET)
	@ResponseBody
	public FeedBackView details(HttpServletRequest request){
		try {
		   return  feedviewmodel.findfeeadmin(Integer.parseInt(request.getParameter("id")));
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		
		}
	
		
	
		
	}
	
	


	
	
	@RequestMapping(value="/deletefeedbackadmin",method =  RequestMethod.POST)
	@ResponseBody
	public String 	deletefeedbackadmin(HttpServletRequest request){
		int id  =  Integer.parseInt(request.getParameter("id"));
		try {
			
		  feedmodel.delete(feedmodel.find(id));
			
		     
			return "success";
			
		
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	//Managed Orders
	
	@RequestMapping(value="managed-orders", method = RequestMethod.GET)
	public String managedorders(ModelMap modelMap,HttpSession session) {

		
		if(session.getAttribute("username") != null){
			modelMap.put("title", "Admin - manged ordes");

			modelMap.put("totalorder",ordermodel.findAll().size());
			return "managedorders";
			
		}else{
			
			return "loginadmin";
		}
		
		
		
	
	}
	
	
	
	
	@RequestMapping(value="/listorders", method =  RequestMethod.GET)
	@ResponseBody
	public List<OrdersView> listorders(HttpServletRequest request){

		int page =   Integer.parseInt(request.getParameter("page"));
		int   pagesize = Integer.parseInt(request.getParameter("pagesize"));
       try {
    	   
    	   
    	   
    	   return ordersviewmodel.listtws(   ((page-1) * pagesize), pagesize);

    		
    		
    	
    	
    	
	   } catch (Exception e) {
			System.out.println(e.getMessage());
		  
	           return null;
	  }
		
       
       
	}
	
	
	
	
	
	
	
	@RequestMapping(value="/searlistor", method =  RequestMethod.GET)
	@ResponseBody
	public List<OrdersView> searlistor(HttpServletRequest request){

        String keyword = request.getParameter("keywordor");
       try {
    	   
    	   
    	   
    
                   boolean flag =  false;

    	    	 	 for(OrdersView orview:ordersviewmodel.findll()){
      	    			  if(orview.getUsername().equals(keyword)){
      	    				 flag =  true;
      	    				 break;
     	    			  }
    	    		    }
   	    		 
   	    		    if(flag){
    	    		     	 return ordersviewmodel.searchorderviewsecond(keyword);
    	    	     	 }else{
    	    	     		 if(!isnnumber(keyword)){
    	    	     			   return ordersviewmodel.searchorderviewfisrt("%"+keyword+"%","");
    	    	     		 }else{
    	    	     			   return ordersviewmodel.searchorderviewfisrt("",keyword);
    	    	     		 }
    	    			 
    	    		      }
     	    
  	    	
    	     	     	        
                 
    	 
    		
    		
    	
    	
    	
	   } catch (Exception e) {
			System.out.println(e.getMessage());
		  
	           return null;
	  }
		
	}
	
	
	
	
	
	
	
	



	@RequestMapping(value="/searchorders",method =  RequestMethod.GET)
	@ResponseBody
	public List<String>  searchordercustomername(HttpServletRequest request){
		
		if(!ordermodel.searchcustomerName(request.getParameter("term")).isEmpty() ){
			return ordermodel.searchcustomerName(request.getParameter("term"));
		}
		else{
			return ordermodel.searchusername(request.getParameter("term"));
		}
	
	}
	
	
	
	
	@RequestMapping(value="/detail-order/{id}",method =  RequestMethod.GET)
	public String detailsordermanged(@PathVariable(value="id")int id, HttpServletRequest request,ModelMap modelMap,HttpSession session){
		
		  Account account =  new AccountAdminModel().findaccountadmin( session.getAttribute("username").toString() );
			if(account!=null){
				boolean flag = false;
				for (Role rl : account.getRoles()) {
					if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
						flag = true;
						break;
					}
				}
				
				if(flag){
					modelMap.put("title", "Admin-Details ordes");
			
					 Orders orders =   ordermodel.findordersadmin(id);
					 boolean sa = true;
					 if(orders.getStatus()==true){
						 sa = false;
					 }else{
						 sa= true ;
					 }
					 modelMap.put("sa", sa);
					 modelMap.put("orders",orders);
					 modelMap.put("listorderde", ordermodel.findordersadmin(id).getOrdersdetails());
				      return "managedordersdetails";
				}else{
					return "loginadmin";
					
				}
		
			
			}else{
				
				return "loginadmin";
			}

	      
	
	}
	
	@RequestMapping(value="/eidt-order/{id}",method =  RequestMethod.GET)
	public String eidtorder(@PathVariable(value="id") int id, HttpServletRequest request,ModelMap modelMap,HttpSession session){
		
	
		
		
		  Account account =  new AccountAdminModel().findaccountadmin( session.getAttribute("username").toString() );
			if(account!=null){
				boolean flag = false;
				for (Role rl : account.getRoles()) {
					if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
						flag = true;
						break;
					}
				}
				
				if(flag){
					
					modelMap.put("title", "Admin-Edit ordes");
			
					 modelMap.put("orders", ordermodel.findordersadmin(id));
				      return "orderedit";
				}else{
					return "loginadmin";
					
				}
		
			
			}else{
				
				return "loginadmin";
			}

	      
	
	}
	

	
	
	
	@RequestMapping(value="/eidt-order/{id}",method = RequestMethod.POST)
    public String updateorder(HttpServletRequest request,ModelMap  modelMap){

		try {
	    	String[] quantity = request.getParameterValues("quantity");
	    	String[] productid =  request.getParameterValues("productid");
	    	String[] price =  request.getParameterValues("price");
	    	int orderid =  Integer.parseInt(request.getParameter("orderid"));
	    	String address =  request.getParameter("address");
	    	String description =  request.getParameter("description");

	    	
	    	Orders or=  ordermodel.findordersadmin(orderid);
	    	
	    	
	    	if(or.getStatus()==false){
		    	modelMap.put("updatesuccessorder", "order have checkout");
	    	}else{
	    		
		    	or.setCustomerAddress(address);
		    	or.setCustomerMessage(description);

		         long s   = 0;
		    	
		    	for(int i=0;i<or.getOrdersdetails().size();i++){
		    	 	Ordersdetail ordersdetail = new Ordersdetail();
		    	 	ordersdetail.setId(new OrdersdetailId(Integer.parseInt(productid[i]), orderid));
		    	 	ordersdetail.setQuantity(Integer.parseInt(quantity[i]));
		            ordersdetail.setPrice(Long.parseLong(price[i]));
		    	 	new OrderdetailsAdminModel().update(ordersdetail);
		    	 	
		    	 	s= s+ (Integer.parseInt(quantity[i]) * Long.parseLong(price[i]));
		    	}
		    	or.setTotalmoney(s);
		 
		    	ordermodel.update(or);
		    	
		   
			
		    	modelMap.put("updatesuccessorder", "update order success");
	    	}
	    	

	    	
	    	
	    	
	

	       modelMap.put("orders", ordermodel.findordersadmin(orderid));
	   	modelMap.put("title", "Admin-Edit ordes");
	    	return "orderedit";
		} catch (Exception e) {
		 	modelMap.put("updatesuccessorder", "bill has been updated");
		  	return "orderedit";
			
		}

    }
	
	@RequestMapping(value="/detail-order/{id}",method =  RequestMethod.POST)
	public String checkoutorders(@PathVariable(value="id") int id, HttpServletRequest request,ModelMap  modelMap,HttpSession session){

		
		  Account account =  new AccountAdminModel().findaccountadmin( session.getAttribute("username").toString() );
			if(account!=null){
				boolean flag = false;
				for (Role rl : account.getRoles()) {
					if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
						flag = true;
						break;
					}
				}
				
				if(flag){
					try {
					
						long s= 0;
						Orders  or =  ordermodel.findordersadmin(id);
	
						if(or.getStatus() ==true){
							or.setStatus(false);
							
							ProductAdminModel productModel =  new ProductAdminModel();
							for(Ordersdetail orde:or.getOrdersdetails()){
								
								Product pr = productModel.find(orde.getProduct().getProductId());
								pr.setQuantity(pr.getQuantity()-orde.getQuantity());
								productModel.update(pr);
								s = s+(orde.getQuantity() * orde.getPrice());
					 		
				           	}
						
						
							or.setTotalmoney(s);
							// modelMap.put("orders", or);
					
							modelMap.put("message", "Checkout order success ");
						}else{
				
							modelMap.put("message", "order have checkout" );
						
						}
			

						ordermodel.update(or);
						 modelMap.put("orders", ordermodel.findordersadmin(id));
						 modelMap.put("listorderde", ordermodel.findordersadmin(id).getOrdersdetails());
							modelMap.put("title", "Admin-Details ordes");
							 modelMap.put("sa", true);
					      return "managedordersdetails";
						
					} catch (Exception e) {
						System.out.println(e.getMessage());
						return null;
					}
					
				}else{
					return "loginadmin";
					
				}
		
			
			}else{
				
				return "loginadmin";
			}
	}
	
	
	@RequestMapping(value="revenues", method = RequestMethod.GET)
	public String revenues(ModelMap modelMap,HttpSession session) {


		
	
		  Account account =  new AccountAdminModel().findaccountadmin( session.getAttribute("username").toString() );
				if(account!=null){
					boolean flag = false;
					for (Role rl : account.getRoles()) {
						if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
							flag = true;
							break;
						}
					}
					
					if(flag){
						modelMap.put("title", "Admin- Revenues");


			            comondata data =  new comondata();
			            modelMap.put("listdata", data.findlldate());
					     
						return "revenuesadmin";
					}else{
						return "loginadmin";
						
					}
			
				
				}else{
					
					return "loginadmin";
				}
		
	
	}
	
	
	@RequestMapping(value="/revenueschart",method =  RequestMethod.GET)
	@ResponseBody
	public List<RevenueStatistic>  revenueschart(HttpServletRequest request){
		try {
			SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd");
			  Calendar c = Calendar.getInstance();
			     c.setTime(new Date());
			return revenstaticmodel.findllproce(sdf.parse(""+c.get(Calendar.YEAR)+"-12-31"),sdf.parse(""+c.get(Calendar.YEAR)+"-01-01"));
		} catch (Exception e) {
			return null;
		}
	
	}
	
	
	
	
	@RequestMapping(value="/revenuesfindll",method =  RequestMethod.GET)
	@ResponseBody
	public List<RevenueStatistic> revenuesfindll(HttpServletRequest request,ModelMap model){
		try {
			SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd");
			
			String fromdata =  request.getParameter("fromdata");
			String todate =  request.getParameter("todate");
			
			
			
			
			if(fromdata!=null && todate!=null){
				 Calendar c1 = Calendar.getInstance();
				 Calendar c2 =  Calendar.getInstance();
				 c1.setTime(sdf.parse(fromdata));
				 c2.setTime(sdf.parse(todate));
				if( (c1.get(Calendar.MONTH)+1)< (c2.get(Calendar.MONTH)+1)){
					return revenstaticmodel.findllproce(sdf.parse(todate), sdf.parse(fromdata));
				}else{
			
					return revenstaticmodel.findllproce(sdf.parse(todate), sdf.parse(fromdata));
				}
			
			}else{
				  Calendar c = Calendar.getInstance();
				     c.setTime(new Date());
			
			   
				return revenstaticmodel.findllproce(sdf.parse(""+c.get(Calendar.YEAR)+"-12-31"),sdf.parse(""+c.get(Calendar.YEAR)+"-01-01"));
			}
		
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	@RequestMapping(value="/deleteorder",method =  RequestMethod.POST)
	@ResponseBody
	public String 	deleteorder(HttpServletRequest request){
		int id  =  Integer.parseInt(request.getParameter("id"));
		try {
			
			OrderdetailsAdminModel orderdemodel =  new OrderdetailsAdminModel();
		
			for(Ordersdetail ordel:ordermodel.find(id).getOrdersdetails()){
				orderdemodel.delete(ordel);
			}
			
			ordermodel.delete(ordermodel.find(id));
			
		     
			return "success";
			
		
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	
	
	@RequestMapping(value="payment", method = RequestMethod.GET)
	public String payment(ModelMap modelMap,HttpSession session) {

		

		
		
		  Account account =  new AccountAdminModel().findaccountadmin( session.getAttribute("username").toString() );
				if(account!=null){
					boolean flag = false;
					for (Role rl : account.getRoles()) {
						if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
							flag = true;
							break;
						}
					}
					
					if(flag){
						modelMap.put("title", "Admin - Payment");

						
						return "Paymentadmin";
					}else{
						return "loginadmin";
						
					}
			
				
				}else{
					
					return "loginadmin";
				}
	
	}

	@RequestMapping(value="/listpayment",method =  RequestMethod.GET)
	@ResponseBody
	public List<PaymentView>  listpayment(HttpServletRequest request){
		
	     try {
			
	    	 return paymentviewmodel.findlladmin();
	    	 
		} catch (Exception e) {
	           System.out.println(e.getMessage());
	           return null;
		}
	
	}
	
	@RequestMapping(value="/createpaymentmethods",method =  RequestMethod.POST)
	@ResponseBody
	public String createpaymentmethods(HttpServletRequest request,HttpSession session){
		
		String  namepayment=  request.getParameter("namepayment");
	
		int id  =  Integer.parseInt(request.getParameter("id"));
		try {
			
			if(id==0){
			
			Payment payment = new Payment();
			payment.setPaymentName(namepayment);
            paymentmodel.create(payment);
		
			}else{
			   
				Payment payment =  paymentmodel.find(id);
				payment.setPaymentName(namepayment);
				paymentmodel.update(payment);
				
			
			}
			return "success";
			
		
		} catch (Exception e) {
			return e.getMessage();
		}
		
	}

	
	@RequestMapping(value="/detailspayment",method = RequestMethod.GET)
	@ResponseBody
	public PaymentView detailspayment(HttpServletRequest request){
		try {
		   return  paymentviewmodel.findadmin(Integer.parseInt(request.getParameter("id")));
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		
		}
	
		
	
		
	}
	
	
	
	@RequestMapping(value="/deletepaymentadmin",method =  RequestMethod.POST)
	@ResponseBody
	public String 	deletepaymentadmin(HttpServletRequest request){
		int id  =  Integer.parseInt(request.getParameter("id"));
		try {
			
	   	  paymentmodel.delete(paymentmodel.find(id));
			
		     
			return "success";
			
		
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	
	//Managed Tranfer
	@RequestMapping(value="tranfer", method = RequestMethod.GET)
	public String tranfer(ModelMap modelMap,HttpSession session) {


		
		
		  Account account =  new AccountAdminModel().findaccountadmin( session.getAttribute("username").toString() );
			if(account!=null){
				boolean flag = false;
				for (Role rl : account.getRoles()) {
					if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
						flag = true;
						break;
					}
				}
				
				if(flag){
					modelMap.put("title", "Admin-Transfer");

					
					return "transferadmin";
				}else{
					return "loginadmin";
					
				}
		
			
			}else{
				
				return "loginadmin";
			}
	
	}
	
	
	@RequestMapping(value="/listtransfer",method =  RequestMethod.GET)
	@ResponseBody
	public List<TransferView> listtransfer(HttpServletRequest request){
		
	     try {
			
	    	 return transferviewmodel.findlladmin();
	    	 
		} catch (Exception e) {
	           System.out.println(e.getMessage());
	           return null;
		}
	
	}

	
	@RequestMapping(value="/savestransfer",method =  RequestMethod.POST)
	@ResponseBody
	public String 	savestransfer(HttpServletRequest request,HttpSession session){
		
		String   nametransfer=  request.getParameter("nametransfer");
	
		int id  =  Integer.parseInt(request.getParameter("id"));
		try {
			
			if(id==0){
			
	              Transfer transfer =  new Transfer();
	                transfer.setTransferName(nametransfer);
	                transfermodel.create(transfer);
		}
	        else{
			   
				   Transfer transfer =  transfermodel.find(id);
				   transfer.setTransferName(nametransfer);
				   transfermodel.update(transfer);
				
			
			}
			return "success";
			
		
		} catch (Exception e) {
			return e.getMessage();
		}
		
	}
	
	
	
	@RequestMapping(value="/detailstranfer",method = RequestMethod.GET)
	@ResponseBody
	public TransferView detailstranfer(HttpServletRequest request){
		try {
		   return  transferviewmodel.findadmin( Integer.parseInt(request.getParameter("id") ));
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		
		}
	
		
	
		
	}
	
	
	@RequestMapping(value="/delettransferadmin",method =  RequestMethod.POST)
	@ResponseBody
	public String delettransferadmin(HttpServletRequest request){
		int id  =  Integer.parseInt(request.getParameter("id"));
		try {
			
	   	    transfermodel.delete(transfermodel.find(id));
			
		     
			return "success";
			
		
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	private boolean isnnumber(String number){
		try {
			Integer.parseInt(number);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
