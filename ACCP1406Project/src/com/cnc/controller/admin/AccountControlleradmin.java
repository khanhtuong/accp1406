package com.cnc.controller.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.cnc.common.AccountView;
import com.cnc.common.AccountViewModel;
import com.cnc.common.CategoryView;
import com.cnc.common.ProductView;
import com.cnc.entities.Account;
import com.cnc.entities.Category;
import com.cnc.entities.Role;
import com.cnc.model.*;

import help.PasswordMd5;

@Controller
@RequestMapping(value="admin/account")

public class AccountControlleradmin {

	
	    
	  AccountAdminModel accountmodel  =  new AccountAdminModel();
	  AccountViewModel accountviewmodel =  new AccountViewModel();
	  RoleAdminModel rolemodel =  new RoleAdminModel();
	  
		@RequestMapping(value="user",method = RequestMethod.GET)
		public String managedaccountuser(ModelMap modelMap,HttpSession session) {

	
			
			
			  Account account = accountmodel.findaccountadmin(session.getAttribute("username").toString());
					if(account!=null){
						boolean flag = false;
						for (Role rl : account.getRoles()) {
							if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
								flag = true;
								break;
							}
						}
						
						if(flag){
							modelMap.put("title", "Admin");
							
							return "adminaccountuser";
						}else{
							return "loginadmin";
							
						}
				
					
					}else{
						
						return "loginadmin";
					}
			
		
		}
		
		
		  
			@RequestMapping(value="admin",method = RequestMethod.GET)
			public String managedaccountadmin(ModelMap modelMap,HttpSession session) {
				
				  Account account =  new AccountModel().find(session.getAttribute("username"));
					if(account!=null){
						boolean flag = false;
						for (Role rl : account.getRoles()) {
							if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
								flag = true;
								break;
							}
						}
						
						if(flag){
							modelMap.put("title", "Admin");
					         
							Account ac =  accountmodel.findaccountadmin(session.getAttribute("username").toString());
						    for(Role r :ac.getRoles()){
						    	if(r.getName().equals("superadmin")){
						    		modelMap.put("checksupadmin", true);
						    		break;
						    	}
						    }
							
							return "adminaccountadmin";
						}else{
							return "loginadmin";
							
						}
				
					
					}else{
						
						return "loginadmin";
					}
				
				
			
			}
	  
	  
	
	@RequestMapping(value="profile",method = RequestMethod.GET)

	public String profile(ModelMap modelMap,HttpSession session,HttpServletRequest request) {

	
		
		
		
		
		
		  Account account =  accountmodel.findaccountadmin(session.getAttribute("username").toString());
			if(account!=null){
				boolean flag = false;
				for (Role rl : account.getRoles()) {
					if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
						flag = true;
						break;
					}
				}
				
				if(flag){
					modelMap.put("title", "Admin");
				
					return "profileadmin";
				}else{
					return "loginadmin";
					
				}
		
			
			}else{
				
				return "loginadmin";
			}
		
		
		
		
		
		
	
	
	}
	
	
//	@RequestMapping(value="/save",method = RequestMethod.GET)
//	
//	public String save(ModelMap modelMap,HttpSession session,HttpServletRequest request) {
//
//	
//		
//		
//		
//		
//		
//		  Account account =  accountmodel.findaccountadmin(session.getAttribute("username").toString());
//			if(account!=null){
//				boolean flag = false;
//				for (Role rl : account.getRoles()) {
//					if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
//						flag = true;
//						break;
//					}
//				}
//				
//				if(flag){
//					modelMap.put("title", "Admin");
//				    modelMap.put("account", account);
//					return "profileadmin";
//				}else{
//					return "loginadmin";
//					
//				}
//		
//			
//			}else{
//				
//				return "loginadmin";
//			}
//		
//		
//		
//		
//		
//		
//	
//	
//	}
	@RequestMapping(value="/save",method =  RequestMethod.POST)
	@ResponseBody
	public String save(HttpServletRequest request,HttpSession session,ModelMap modlmap){
		

		try {
		   String fullname =   request.getParameter("fullname");
		   String email =   request.getParameter("email");
		   String phone =   request.getParameter("phone");
		   String age =   request.getParameter("age");
		   String address =   request.getParameter("address");
			Account accnew =   accountmodel.findaccountadmin(session.getAttribute("username").toString());
				accnew.setFullname(fullname);
				accnew.setAge(Integer.parseInt(age));
				accnew.setEmail(email);
				accnew.setPhone(phone);
				accnew.setAddress(address);
	            accountmodel.update(accnew);
	
	   
			return "success";
	
			
		
		} catch (Exception e) {
			return e.getMessage();
		}
		
	}
	
	@RequestMapping(value="/listaccountuser", method =  RequestMethod.GET)
	@ResponseBody
	public List<AccountView> listaccountuser(HttpServletRequest request){
	
			String username =  request.getParameter("username");
      try {
	   
    	 if(username!=null){
    		 return accountviewmodel.searchacuser(username);
    	 }else{
    		 return accountviewmodel.findlluser();
    	 }
    	 
    	  
 
	   } catch (Exception e) {
			System.out.println(e.getMessage());
		   return null;	
		   
              }
		
	}
	
	
	@RequestMapping(value="/listaccountadmin", method =  RequestMethod.GET)
	@ResponseBody
	public List<AccountView> listaccountadmin(HttpServletRequest request){
	
			String username =  request.getParameter("username");
      try {
	   
    	 if(username!=null){
    		 return accountviewmodel.searchacadmin(username);
    	 }else{
    		 return accountviewmodel.findlladmin();
    	 }
    	 
    	  
 
	   } catch (Exception e) {
			System.out.println(e.getMessage());
		   return null;	
		   
              }
		
	}	
	
	
	
	
	@RequestMapping(value="/accountprofile", method =  RequestMethod.GET)
	@ResponseBody
	public AccountView accountprofile(HttpSession session){
	
		  
      try {
	   
         
    	  return accountviewmodel.findusername(session.getAttribute("username").toString());
 
	   } catch (Exception e) {
			System.out.println(e.getMessage());
		   return null;	
		   
              }
		
	}	
	
	
	
	@RequestMapping(value="/searchusernameadmin",method =  RequestMethod.GET)
	@ResponseBody
	public List<String>  searchusernameadmin(HttpServletRequest request){
		
		return accountmodel.searchusernameadmin(request.getParameter("term"));
	}
	

//	@RequestMapping(value="/findaccountadmin", method =  RequestMethod.GET)
//	@ResponseBody
//	public AccountView findaccount(HttpServletRequest request,HttpSession session){
//	
//			
//      try {
//	   
//    
//    	 return  
//    	  
// 
//	   } catch (Exception e) {
//			System.out.println(e.getMessage());
//		   return null;	
//		   
//      }
//		
//	}	
	
	@RequestMapping(value="/findaccountadminusername", method =  RequestMethod.GET)
	@ResponseBody
	public AccountView findaccountadmin(HttpServletRequest request,HttpSession session){
	
			String username =  request.getParameter("username");
      try {
	   
    
    	 return  accountviewmodel.findusername(username);
    	  
 
	   } catch (Exception e) {
			System.out.println(e.getMessage());
		   return null;	
		   
      }
		
	}	
	
	
	@RequestMapping(value="/createaccountadmin",method =  RequestMethod.POST)
	@ResponseBody
	public String createaccountadmin(HttpServletRequest request,HttpSession session,ModelMap modlmap){
		
		String username =  request.getParameter("username");
		String password =  PasswordMd5.MD5(request.getParameter("password"));
		String fullname =  request.getParameter("fullname");
		String email =  request.getParameter("email");
		int age  =  Integer.parseInt(request.getParameter("age"));
		String phone =  request.getParameter("phone");
		String  address =  request.getParameter("address");
		try {
			
			
			boolean flag =  false;
			for(Account ac:accountmodel.findAll()){
				if(ac.getUsername().equals(username)){
					flag =  true;
					break;
				}
			}
			
			
			
		if(flag){
				
			
		      return "existusername";
				
	     }
			else{
				Account account = new Account();
				account.setUsername(username);
				account.setPassword(password);
				account.setFullname(fullname);
				account.setEmail(email);
				account.setAge(age);
				account.setPhone(phone);
				account.setAddress(address);
			    account.setCreatedate(new Date());
			    
			   Set<Role> list =  new HashSet<Role>();
			    list.add(new RoleAdminModel().find(8));
				account.setRoles(list);
				accountmodel.create(account);
				
		           
				return "success";
			}
			
	
			
		
		} catch (Exception e) {
			return e.getMessage();
		}
		
	}
	

	
	
	@RequestMapping(value="/deleteaccountadmin",method =  RequestMethod.POST)
	@ResponseBody
	public String deleteproductcategory(HttpServletRequest request){
		String username  =  request.getParameter("username");
		try {
			Account ac =  accountmodel.find(username);
		
			accountmodel.delete(ac);
		     
			return "success";
			
		
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	

	
	@RequestMapping(value="changepasswordadmin",method = RequestMethod.GET)
	public String changepassword(ModelMap modelMap,HttpSession session){
	
		
		  Account account =  new AccountModel().find(session.getAttribute("username"));
			if(account!=null){
				boolean flag = false;
				for (Role rl : account.getRoles()) {
					if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
						flag = true;
						break;
					}
				}
				
				if(flag){
					modelMap.put("title", "Admin - change password");
					
					return "changepasswordadmin";
				}else{
					return "loginadmin";
					
				}
		
			
			}else{
				
				return "loginadmin";
			}
		
		
		
		
		
		
		
		
		
		
		
	}
	
	@RequestMapping(value="changepasswordadmin",method = RequestMethod.POST)
	public String changepassword(ModelMap modelMap,HttpSession session,HttpServletRequest request,HttpServletResponse response){
		if(session.getAttribute("username") != null){
			
			modelMap.put("title", "Admin - change password");
	
			String password =  request.getParameter("password");
			String newpassword =  request.getParameter("newpassword");
		    if(password.isEmpty() && newpassword.isEmpty()){
				modelMap.put("errorpassword", "you must enter password");
				modelMap.put("errornewpassword", "you must enter new password");
		    	return "changepasswordadmin";
		    }
		    else if(password.isEmpty()){
				modelMap.put("errorpassword", "you must enter password");
		    	return "changepasswordadmin";
		    }
			
		    else if(newpassword.isEmpty()){
				modelMap.put("errornewpassword", "you must enter new password");
				    	return "changepasswordadmin";
		    }
		    else{
		    	boolean flag =  false;
		        for(Account ac: accountmodel.findAll()){
		        	if(ac.getPassword().equals(PasswordMd5.MD5(password))){
		        		flag =  true;
		        		break;
		        	}
		        }
		        
		        if(flag){
		        	Account ac =  accountmodel.find(session.getAttribute("username").toString());
		        	ac.setPassword(PasswordMd5.MD5(newpassword));
		        	accountmodel.update(ac);
		        	modelMap.put("messagechangepassword", "you have change password success");
		              return new LoginControllerAdmin().lougout(session, modelMap,request, response);
		        }else{
		        	modelMap.put("errorpasswordnot", "not have password you enter");
		           	return "changepasswordadmin";
		        }
		    }
					
		
		}else{
			
			return "loginadmin";
		}
	}

	
}
