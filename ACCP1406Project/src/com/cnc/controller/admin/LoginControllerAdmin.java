package com.cnc.controller.admin;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cnc.entities.*;

import com.cnc.model.*;

import help.PasswordMd5;



@Controller
@RequestMapping(value="admin")
public class LoginControllerAdmin {

	
	@RequestMapping(method = RequestMethod.GET)
	public String index() {
		return "redirect:admin/login.html";
	}
	
	@RequestMapping(value="login",method = RequestMethod.GET)
	public String loginadmin(ModelMap modelMap,HttpServletRequest request,HttpSession session) {

	
		
		Account ac = checkcookie(request);

		if (ac != null && session.getAttribute("username")==null) {
			
		
					
		
						   session.setAttribute("username", ac.getUsername());
							modelMap.put("title", "Admin -  login");
							return "homeadmin.index";
				
					
				
			
				
					
				
		
		} 
		else if(session.getAttribute("username")!=null && ac == null){
			  Account account =  new AccountModel().find(session.getAttribute("username"));
				if(account!=null){
					boolean flag = false;
					for (Role rl : account.getRoles()) {
						if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
							flag = true;
							break;
						}
					}
					
					if(flag){
						modelMap.put("title", "Admin-Home");
					
						return "homeadmin.index";
					}else{
						return "loginadmin";
						
					}
			
				
				}else{
					
					return "loginadmin";
				}
		}
		
		else if(session.getAttribute("username")==null && ac == null){
			modelMap.put("title", "login admin");
			return "loginadmin";
		}else{
			return "";
		}
	}
	
	
	AccountAdminModel accountmodel =  new AccountAdminModel();
	
	
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public String login(ModelMap momap, HttpSession session, HttpServletResponse response, HttpServletRequest request) {

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		if (username.isEmpty() && password.isEmpty()) {
			momap.put("error", "request enter username and  password");
			return "loginadmin";
		} else if (username.isEmpty()) {
			momap.put("error", "request enter username");
			return "loginadmin";
		} else if (password.isEmpty()) {
			momap.put("error", "request enter password");
			return "loginadmin";
		} else {
			Account ac = accountmodel.loginaccountadmin(username,PasswordMd5.MD5(password));
			if (ac != null) {
				boolean flag = false;
				for (Role rl : ac.getRoles()) {
					if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {

						flag = true;
						break;
					}
				}

				if (flag) {

					session.setAttribute("username", username);
					if (request.getParameter("remember")!=null) {
						Cookie ckusername = new Cookie("username", ac.getUsername());
						ckusername.setMaxAge(172800);
						response.addCookie(ckusername);
						Cookie ckpassword = new Cookie("password", ac.getPassword());
						ckpassword.setMaxAge(172800);
						response.addCookie(ckpassword);
					}
				
					session.setAttribute("createdate", ac.getCreatedate());
					return "redirect:/admin/home.html";
				} else {
					momap.put("error", "you not have permision");
					return "loginadmin";
				}

			} else {

				momap.put("error", "Account not valid ");
				return "loginadmin";
			}
		}

	}
	
	
	@RequestMapping(value="lougout", method = RequestMethod.GET)
	public String lougout(HttpSession session,ModelMap modelMap,HttpServletRequest request,HttpServletResponse response){
		
		 session.removeAttribute("username");
		 for(Cookie ck:request.getCookies()){
			 if(ck.getName().equalsIgnoreCase("username")){
				 ck.setMaxAge(0);
				 response.addCookie(ck);
			 }
			 if(ck.getName().equalsIgnoreCase("password")){
				 ck.setMaxAge(0);
				 response.addCookie(ck);
			 }
			 
		 }
		modelMap.put("title", "Admin-login");
		
		
		return "loginadmin";
	}
	
	
	public Account checkcookie(HttpServletRequest request) {

		Cookie[] cookies = request.getCookies();
		Account account = null;
		String username = "", password = "";
		for (Cookie ck : cookies) {
			if (ck.getName().equalsIgnoreCase("username")) {
				username = ck.getValue();
			}
			if (ck.getName().equalsIgnoreCase("password")) {
				password = ck.getValue();
			}
		}

		if (!username.isEmpty() && !password.isEmpty()) {
			account = new Account(username, password);
		}

		return account;

	}
	
	
	
}
