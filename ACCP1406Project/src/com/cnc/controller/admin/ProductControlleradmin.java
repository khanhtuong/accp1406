package com.cnc.controller.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.cnc.common.*;
import com.cnc.entities.*;
import com.cnc.model.*;


import help.*;

import java.text.SimpleDateFormat;
import java.util.*;
@Controller
@RequestMapping(value="admin/product")

public class ProductControlleradmin {

	


	  CategoryviewModel categoryviewmodel =  new CategoryviewModel();
	  CategoryAdminModel camodel =  new CategoryAdminModel();
	  ProductViewModel productviewmodel =  new ProductViewModel();
	  ProductAdminModel productmodel =  new ProductAdminModel();
	  SupplierAdminModel sumodel =  new SupplierAdminModel();
	  SupplierViewModel supplierviewmodel  =  new SupplierViewModel();
	  
	@RequestMapping(value="product-category", method = RequestMethod.GET)
	public String productcategory(ModelMap modelMap,HttpSession session) {
	
		  Account account =  new AccountAdminModel().findaccountadmin( session.getAttribute("username").toString() );
			if(account!=null){
				boolean flag = false;
				for (Role rl : account.getRoles()) {
					if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
						flag = true;
						break;
					}
				}
				
				if(flag){
					modelMap.put("title", "Admin - Product category");

					
					return "productcategoryadmin";
					
				}else{
					return "loginadmin";
					
				}
		
			
			}else{
				
				return "loginadmin";
			}
		
	
	}
	
	
	@RequestMapping(value="/listca", method =  RequestMethod.GET)
	@ResponseBody
	public List<CategoryView> listca(HttpServletRequest request){
		
			String nameca =  request.getParameter("nameca");
       try {
    	   
           if(nameca!=null){
        	  
        	  
        	   return  categoryviewmodel.searchca(nameca);
           }else{
        	  	 return  categoryviewmodel.findll();
           }
    	 
    	
    	
    	   
  
	   } catch (Exception e) {
			System.out.println(e.getMessage());
		   return null;
	
	  }
		
	}
	

	@RequestMapping(value="/searchca",method =  RequestMethod.GET)
	@ResponseBody
	public List<String>  searchca(HttpServletRequest request){
		
		return camodel.searchname(request.getParameter("term"));
	}
	
	
	@RequestMapping(value="/createproductcategoty",method =  RequestMethod.POST)
	@ResponseBody
	public String createproductcategoty(HttpServletRequest request,HttpSession session){
		
		String name =  request.getParameter("name");
		String alias =  request.getParameter("alias");
		String description =  request.getParameter("description");
		String status =  request.getParameter("status");
		int id  =  Integer.parseInt(request.getParameter("id"));
		try {
			
			if(id==0){
				Category category = new Category();
				category.setName(name);
				category.setAlias(alias);
				category.setDescription(description);
				category.setCreateDate(new Date());
				category.setCreateBy(session.getAttribute("username").toString());
				category.setStatus(Boolean.parseBoolean(status));
				camodel.create(category);
		
			}	else{
				
				Category ca = camodel.findacategoryadmin(id);
				ca.setAlias(alias);
				ca.setName(name);
				ca.setDescription(description);
				ca.setUpdateDate(new Date());
				ca.setUpdateBy(session.getAttribute("username").toString());
				ca.setStatus(Boolean.parseBoolean(status));
				camodel.update(ca);
			}
			return "success";
			
		
		} catch (Exception e) {
			return e.getMessage();
		}
		
	}
	@RequestMapping(value="/deleteproductcategory",method =  RequestMethod.POST)
	@ResponseBody
	public String deleteproductcategory(HttpServletRequest request){
		int id  =  Integer.parseInt(request.getParameter("id"));
		try {
			
			camodel.delete(camodel.find(id));
			
		     
			return "success";
			
		
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	
	@RequestMapping(value="/details",method = RequestMethod.GET)
	@ResponseBody
	public CategoryView details(HttpServletRequest request){
		try {
		   return  categoryviewmodel.findca(Integer.parseInt(request.getParameter("id")) );
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		
		}
	
		
	
		
	}
	



	
	
	//Magane Product Admin
	
	
	@RequestMapping(value="product", method = RequestMethod.GET)
	public String product(ModelMap modelMap,HttpSession session) {
		  Account account =  new AccountAdminModel().findaccountadmin( session.getAttribute("username").toString() );
				if(account!=null){
					boolean flag = false;
					for (Role rl : account.getRoles()) {
						if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
							flag = true;
							break;
						}
					}
					
					if(flag){
						modelMap.put("title", "Admin - Product");
						modelMap.put("totalproduct",productmodel.findAll().size());
						return "productadmin";
					}else{
						return "loginadmin";
						
					}
			
				
				}else{
					
					return "loginadmin";
				}
		
	
	}
	
	
	@RequestMapping(value="/listpr", method =  RequestMethod.GET)
	@ResponseBody
	public List<ProductView> listpr(HttpServletRequest request){
		
		
			int page =   Integer.parseInt(request.getParameter("page"));
			int   pagesize = Integer.parseInt(request.getParameter("pagesize"));
       try {
    	   
                
    	    return    productviewmodel.listtwsview(( (page-1) * pagesize), pagesize);
          
    
    	
       
      
    	   
  
	   } catch (Exception e) {
			System.out.println(e.getMessage());
		   return null;
	
	  }
		
	}
	
	@RequestMapping(value="/searlistpr", method =  RequestMethod.GET)
	@ResponseBody
	public List<ProductView> searlistpr(HttpServletRequest request){
		
			String namepr =  request.getParameter("namepr");
			
       try {
    	   
          List<ProductView> model = null;
    
          
          if(namepr!=null){
              model =  productviewmodel.searchpr(namepr);
            
          }
          
              
            
              
    
          
      
 
                
             return model;
          
    
    	
       
      
    	   
  
	   } catch (Exception e) {
			System.out.println(e.getMessage());
		   return null;
	
	  }
		
	}
	

	@RequestMapping(value="/searchpr",method =  RequestMethod.GET)
	@ResponseBody
	public List<String>  searchpr(HttpServletRequest request){
		
		return productmodel.searchnamepr(request.getParameter("term"));
	}
	

	@RequestMapping(value="add-product-admin", method = RequestMethod.GET)
	public String addproduct(ModelMap modelMap,HttpSession session){

		  Account account =  new AccountAdminModel().findaccountadmin( session.getAttribute("username").toString() );
				if(account!=null){
					boolean flag = false;
					for (Role rl : account.getRoles()) {
						if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
							flag = true;
							break;
						}
					}
					
					if(flag){
						modelMap.put("title", "Admin -  Add Product");
				           modelMap.put("listsu", sumodel.findAll());
				           modelMap.put("listcate", categoryviewmodel.findll());
				           modelMap.put("productview",new  ProductView());
							return "productadminadd";
					}else{
						return "loginadmin";
						
					}
			
				
				}else{
					
					return "loginadmin";
				}
		
	}
	@RequestMapping(value="add-product-admin", method = RequestMethod.POST)
	public String addproduct(@ModelAttribute(value="productview") ProductView productview,   ModelMap modelMap,HttpSession session,HttpServletRequest request){
		
		if(session.getAttribute("username") != null){
	
         
    
           
           
           
           int categoryid =  Integer.parseInt(request.getParameter("categoryid"));
           
           int supplierid =  Integer.parseInt(request.getParameter("supplierid"));
       		Product pr =  new Product();
			pr.setName(productview.getName());
			pr.setPrice(productview.getPrice());
			pr.setPromotionPrice(productview.getPromotionPrice());
			pr.setOriginalPrice(productview.getOriginalPrice());
			pr.setQuantity(productview.getQuantity());
			pr.setDescription(productview.getDescription());
			pr.setPhoto(UploadFilehelper.simpleUpload(productview.getFile(), request, true, "assets/admin/img"));
			pr.setStatus(true);
			pr.setCreateDate(new Date());
			pr.setCreateBy(session.getAttribute("username").toString());
			
			
		    Category ca =  camodel.find(categoryid);
		    Supplier su =  sumodel.find(supplierid);
			pr.setCategory(ca);
			pr.setSupplier(su);
			
			
	    	productmodel.create(pr);
			
		   modelMap.put("messae", "you create product success");
			modelMap.put("title", "Admin -  Add Product");
	           modelMap.put("listsu", sumodel.findAll());
	           modelMap.put("listcate", categoryviewmodel.findll());
	           modelMap.put("productview",new  ProductView());
			return "productadminadd";
        	   
        
		   
    	   

			
		}else{
			
			return "loginadmin";
		}
		
	}
	
	
	@RequestMapping(value="/deleteproduct",method =  RequestMethod.POST)
	@ResponseBody
	public String deleteproduct(HttpServletRequest request){
		int id  =  Integer.parseInt(request.getParameter("id"));
		try {
		  
			productmodel.delete(productmodel.find(id));
			
		     
			return "success";
			
		
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	@RequestMapping(value="edit-product/{id}", method = RequestMethod.GET)
	public String editproduct(@PathVariable(value="id") int id,HttpSession session,ModelMap modelmap){

		
		  Account account =  new AccountAdminModel().findaccountadmin( session.getAttribute("username").toString() );
			if(account!=null){
				boolean flag = false;
				for (Role rl : account.getRoles()) {
					if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
						flag = true;
						break;
					}
				}
				
				if(flag){
					modelmap.put("title", "Eidt Product");
					ProductView productView =  productviewmodel.findproadmin(id);
					modelmap.put("title", "Admin - Edit product");
					modelmap.put("listsu", sumodel.findAll());
					modelmap.put("listcate", categoryviewmodel.findll());
					modelmap.put("productview", productView);
				
					return "productadminedit";
				}else{
					return "loginadmin";
					
				}
		
			
			}else{
				
				return "loginadmin";
			}
	}
	
	
	
	@RequestMapping(value="edit-product", method = RequestMethod.GET)
	public String editproduct(HttpServletRequest request,HttpSession session,ModelMap modelmap){
		
		
		  Account account =  new AccountAdminModel().findaccountadmin( session.getAttribute("username").toString() );
				if(account!=null){
					boolean flag = false;
					for (Role rl : account.getRoles()) {
						if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
							flag = true;
							break;
						}
					}
					
					if(flag){
						modelmap.put("title", "Admin - Edit Product");

						modelmap.put("totalproduct",productmodel.findAll().size());
						return "productadmin";
					}else{
						return "loginadmin";
						
					}
			
				
				}else{
					
					return "loginadmin";
				}
		
		
	}
	
	@RequestMapping(value="edit-product", method = RequestMethod.POST)
	public String editproduct(@ModelAttribute(value="productview") ProductView productview,HttpServletRequest request,HttpSession session,ModelMap modelmap){
		
		if(session.getAttribute("username") != null){

		     int categoryid =  Integer.parseInt(request.getParameter("categoryid"));
	           
	           int supplierid =  Integer.parseInt(request.getParameter("supplierid"));
			
			ProductView prview =  productviewmodel.findproadmin(productview.getProductId());
			Product pr = productmodel.find(prview.getProductId());
			pr.setName(productview.getName());
			pr.setPrice(productview.getPrice());
			pr.setPromotionPrice(productview.getPromotionPrice());
			pr.setOriginalPrice(productview.getOriginalPrice());
			pr.setQuantity(productview.getQuantity());
			pr.setDescription(productview.getDescription());
			if(productview.getFile().isEmpty()){
				pr.setPhoto(productview.getPhoto());
			}else{
				pr.setPhoto(UploadFilehelper.simpleUpload(productview.getFile(), request, true, "assets/admin/img"));
			}
		
			pr.setStatus(true);
		    
			 pr.setUpdateDate(new Date());
			 pr.setUpdateBy(session.getAttribute("username").toString());
			
		    Category ca =  camodel.find(categoryid);
		    Supplier su =  sumodel.find(supplierid);
			pr.setCategory(ca);
			pr.setSupplier(su);
	
			
			productmodel.update(pr);
			modelmap.put("title", "Eidt Product");
	
			modelmap.put("title", "Admin - Edit product");
			modelmap.put("listsu", sumodel.findAll());
			modelmap.put("listcate", categoryviewmodel.findll());
			modelmap.put("productview",new  ProductView());
		modelmap.put("message", "you update product success");
			return "productadminedit";
			
		}else{
			
			return "loginadmin";
		}
	}
	
	
	@RequestMapping(value="/detailsprouctadmin",method = RequestMethod.GET)
	@ResponseBody
	public ProductView detailsprouctadmin(HttpServletRequest request){
		try {
			
		
			
		return productviewmodel.findproadmin(Integer.parseInt(request.getParameter("id")));
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		
		}
	
		
	
		
	}
	
	
	//Managed Supplier admin
	
	@RequestMapping(value="product-supplier", method = RequestMethod.GET)
	public String productSupplier(ModelMap modelMap,HttpSession session) {

		
		  Account account =  new AccountAdminModel().findaccountadmin( session.getAttribute("username").toString() );
				if(account!=null){
					boolean flag = false;
					for (Role rl : account.getRoles()) {
						if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
							flag = true;
							break;
						}
					}
					
					if(flag){
						modelMap.put("title", "Admin - Product Supplier");

						
						return "supplieradmin";
					}else{
						return "loginadmin";
						
					}
			
				
				}else{
					
					return "loginadmin";
				}
		
	
	}
	
	

	
	@RequestMapping(value="/listsupplier", method =  RequestMethod.GET)
	@ResponseBody
	public List<SupplierView>listsupplier(HttpServletRequest request){
		

		String namesu =  request.getParameter("namesu");
	       try {
	    	   
	           if(namesu!=null){
	        	  
	        	   return supplierviewmodel.searchsupplieradmin(namesu);
	        	
	           }else{
	        	   return supplierviewmodel.findlladmin();
	           }
    	   
  
	   } catch (Exception e) {
			System.out.println(e.getMessage());
		   return null;
	
	  }
		
	}
	
	@RequestMapping(value="/searchsupplier",method =  RequestMethod.GET)
	@ResponseBody
	public List<String>  searchsupplier(HttpServletRequest request){
		
		return sumodel.searchnamesupplier(request.getParameter("term"));
	}
	
	

	
	
	@RequestMapping(value="/productsupplier",method =  RequestMethod.POST)
	@ResponseBody
	public String productsupplier(HttpServletRequest request,HttpSession session){
		
		String  namesu =  request.getParameter("namesu");
		String  phonesu =  request.getParameter("phonesu");
		String  address =  request.getParameter("address");
		String status =  request.getParameter("status");
		int id  =  Integer.parseInt(request.getParameter("id"));
		try {
	
			if(id==0){
				Supplier su = new Supplier();
		
				su.setName(namesu);
				su.setAddress(address);
				su.setStatus(Boolean.parseBoolean(status));
				su.setCreatedate(new Date());
				su.setPhone(phonesu);
			
				sumodel.create(su);
		
			}	else{
				
				Supplier su =  sumodel.find(id);
				
				su.setName(namesu);
				su.setAddress(address);
				su.setStatus(Boolean.parseBoolean(status));
				su.setCreatedate(new Date());
				su.setPhone(phonesu);
		        sumodel.update(su);
			}
			return "success";
			
		
		} catch (Exception e) {
			return e.getMessage();
		}
		
	}
	

	@RequestMapping(value="/detailssupplier",method = RequestMethod.GET)
	@ResponseBody
	public SupplierView detailssupplier(HttpServletRequest request){
		try {
		   return  supplierviewmodel.findsuadmin(Integer.parseInt(request.getParameter("id")) );
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		
		}
	
		
	
		
	}
	

	
	@RequestMapping(value="/deleteproductsupplier",method =  RequestMethod.POST)
	@ResponseBody
	public String 	deleteproductsupplier(HttpServletRequest request){
		int id  =  Integer.parseInt(request.getParameter("id"));
		try {
			
			sumodel.delete(sumodel.find(id));
			
		     
			return "success";
			
		
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
}
