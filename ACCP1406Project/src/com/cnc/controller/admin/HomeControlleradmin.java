package com.cnc.controller.admin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.cnc.common.RevenueStatistic;
import com.cnc.entities.Account;
import com.cnc.entities.Orders;
import com.cnc.entities.Role;
import com.cnc.model.*;
@Controller
@RequestMapping(value="admin/home")

public class HomeControlleradmin {

	
	@RequestMapping(method = RequestMethod.GET)
	public String managed(ModelMap modelMap,HttpSession session,HttpServletRequest request) {

	  Account account =  new AccountAdminModel().findaccountadmin(session.getAttribute("username").toString());
		if(account!=null){
			boolean flag = false;
			for (Role rl : account.getRoles()) {
				if (rl.getName().equals("admin") || rl.getName().equals("superadmin")) {
					flag = true;
					break;
				}
			}
			
			if(flag){
				modelMap.put("title", "Admin-Home");
				countmember(session);
				countfeedback(session);
				totalbenefit(session);
				Staticwebsite(session);
				return "homeadmin.index";
			}else{
				return "loginadmin";
				
			}
	
		
		}else{
			
			return "loginadmin";
		}
		
	
	}
	
	
	private void  countmember(HttpSession session){
		try {
			AccountAdminModel model =  new AccountAdminModel();
			session.setAttribute("countaccount", model.findAll().size());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	private void  countfeedback(HttpSession session){
		FeedBackAdminModel model =  new FeedBackAdminModel();
	
		int i =  model.findAll().size();
		session.setAttribute("countfeedback", model.findAll().size());
	}
	
	public static void main(String[] args) {
		System.out.println(new FeedBackAdminModel().findAll().size());
	}
	
	private void totalbenefit(HttpSession session){
		try {
			RevenueStatisticAdminModel model =  new RevenueStatisticAdminModel();
			SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd");
			  Calendar c = Calendar.getInstance();
			     c.setTime(new Date());
				long totalBenefit = 0;
				long totalreven = 0;
				for(RevenueStatistic ren:model.findllproce(sdf.parse(""+c.get(Calendar.YEAR)+"-12-31"),sdf.parse(""+c.get(Calendar.YEAR)+"-01-01"))){
					totalBenefit+= ren.getBenefit();
					 totalreven+= ren.getRevenues();
				}
				
				session.setAttribute("totalBenefit", totalBenefit);
				session.setAttribute("totalreven", totalreven);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
	
	
	private 	void Staticwebsite(HttpSession session){
	
	      ProductAdminModel productmodel =  new ProductAdminModel();
	      CategoryAdminModel      categoryModel =  new CategoryAdminModel();
		  OrdersAdminModel ordermodel =  new OrdersAdminModel();
		  AccountAdminModel accountModel =  new AccountAdminModel();
		  SupplierAdminModel supplierModel =  new SupplierAdminModel();
		  PaymentAdminModel paymentModel =  new PaymentAdminModel();
		  
			session.setAttribute("totalproduct", productmodel.findAll().size());
			session.setAttribute("totalcategory", categoryModel.findAll().size());
			session.setAttribute("totalorder", ordermodel.findAll().size());
			session.setAttribute("totalaccount", accountModel.findAll().size());
			session.setAttribute("totalsuppliermodel", supplierModel.findAll().size());
			session.setAttribute("totalpayment", paymentModel.findAll().size());
	      
	
	}
	
	
	
	
	
}
