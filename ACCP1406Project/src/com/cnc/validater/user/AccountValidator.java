package com.cnc.validater.user;
import org.springframework.validation.*;

import com.cnc.entities.Account;

import com.cnc.model.AccountModel;
public class AccountValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {
		return Account.class.equals(arg0);
	}

	@Override
	public void validate(Object object, Errors error) {
		AccountModel accountModel = new AccountModel();
		Account account = (Account)object;
		if(accountModel.find(account.getUsername()) != null){
			error.rejectValue("username", "account.username.exists");
		}
		if(accountModel.getAccountByEmail(account.getEmail()) != null){
			error.rejectValue("email", "account.email.exists");
		}
		
		
	}

}
