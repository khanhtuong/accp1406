package com.cnc.model;

import org.hibernate.Session;

import com.cnc.entities.*;

public class TransferAdminModel extends AbstractModel<Transfer> {

	
	
	public TransferAdminModel(){
		super(Transfer.class);
	}
	
	public Transfer findtransfer(int id){
		Session session = null;
		try {
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				 sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			 session = sessionFactory.openSession();
			return (Transfer) session.
					createQuery("select tr from Transfer tr where tr.transferId =:transferId")
					.setInteger("transferId", id)
				
			        .uniqueResult();
			
		} catch (RuntimeException re) {
			
			System.out.println(re.getMessage());
			return null;
		}
//		}finally {
//			if(null != session){
//				
//				session.clear();
//			}
//		}
		
		
	}
	
}
