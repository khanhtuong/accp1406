package com.cnc.model;

import com.cnc.entities.*;
import java.io.*;
import java.util.*;
import org.hibernate.*;

@SuppressWarnings("unchecked")
public abstract class AbstractModel<T> {

    private Class<T> entityClass;
    protected final SessionFactory sessionFactory = HibernateUtil
            .getSessionFactory();

    public AbstractModel(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public AbstractModel() {
    }
    
    public List<T> findAll() {
    	Session session = null;
        try {
            if (!sessionFactory.getCurrentSession().getTransaction().isActive())
                sessionFactory.getCurrentSession().getTransaction().begin();  
            session = sessionFactory.openSession();
            return session.createQuery("from " + entityClass.getName()).list();

        } catch (RuntimeException re) {
        	
            return null;
        
        }finally {
			if(null != session){	

			session.clear();
			}
		}
    }

    public void update(T instance){
    	try {
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
    		
    		sessionFactory.getCurrentSession().merge(instance);
    		sessionFactory.getCurrentSession().getTransaction().commit();
		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			System.out.println(re.getMessage());
			throw re;
		}
    }
     
    public List<T> getProductwithIdCategory(Integer id) {
        try {
            if (!sessionFactory.getCurrentSession().getTransaction().isActive())
                sessionFactory.getCurrentSession().getTransaction().begin();            
            return sessionFactory.getCurrentSession()
                    .createQuery("from "+entityClass.getName()+" where categoryId =" + id).list();
        } catch (RuntimeException re) {
        	
            return null;
        }
    }
    
    public List<T> getProductwithIdSupplier(Integer id) {
        try {
            if (!sessionFactory.getCurrentSession().getTransaction().isActive())
                sessionFactory.getCurrentSession().getTransaction().begin();            
            return sessionFactory.getCurrentSession()
                    .createQuery("from "+entityClass.getName()+" where supplierId =" + id).list();
        } catch (RuntimeException re) {
        	
            return null;
        }
    }
    
    
	 
	 public void updatePass2(Account acc, String name){
		 Session session = sessionFactory.openSession();
		 Transaction tx = session.beginTransaction();
		 String hqlUpdate = "update Account c set c.password = :newPass where c.username = :userName";
		 // or String hqlUpdate = "update Customer set name = :newName where name = :oldName";
		 int updatedEntities = session.createQuery( hqlUpdate )
		         .setString("newPass", acc.getPassword() )		
		         .setString("userName", name)
		         .executeUpdate();
		 tx.commit();
		 session.close();
		 
	 }

    public void delete(T instance) {
        try {
            if (!sessionFactory.getCurrentSession().getTransaction().isActive())
                sessionFactory.getCurrentSession().getTransaction().begin();            
            sessionFactory.getCurrentSession().delete(instance);
            sessionFactory.getCurrentSession().getTransaction().commit();
        } catch (RuntimeException re) {
            sessionFactory.getCurrentSession().getTransaction().rollback();
            throw re;
        }
    }

    public void create(T instance) {
        try {
            if (!sessionFactory.getCurrentSession().getTransaction().isActive())
                sessionFactory.getCurrentSession().getTransaction().begin();            
            sessionFactory.getCurrentSession().persist(instance);
            sessionFactory.getCurrentSession().getTransaction().commit();
        } catch (RuntimeException re) {
            sessionFactory.getCurrentSession().getTransaction().rollback();
            throw re;
        }
    }


    public T find(Object primakey){
    	try {
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			return (T) sessionFactory.getCurrentSession().get(entityClass, (Serializable)primakey);
		} catch (RuntimeException re) {
			System.out.println(re.getMessage());
			return null;
		}

    }
}
