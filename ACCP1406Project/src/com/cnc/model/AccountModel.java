package com.cnc.model;

import javax.websocket.Session;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

import com.cnc.entities.*;

public class AccountModel extends AbstractModel<Account> {

	public AccountModel() {
		super(Account.class);
	}
	
	public Account findByName(String name) {
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			return (Account) sessionFactory.getCurrentSession().load(Account.class, (String)name);
			
		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}
	};
	
		
	public Account getAccountByUserNameAndPassword(String userName, String passWord){
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			Query query = sessionFactory.getCurrentSession()
					.createQuery("FROM Account p where p.username = :username and p.password = :password");
					query.setString("username", userName);
					query.setString("password", passWord);
				return (Account) query.uniqueResult();	
			
		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}
	}
	
	public Account getAccountByEmail(String email){
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			Query query = sessionFactory.getCurrentSession()
					.createQuery("FROM Account p where p.email = :email");
					query.setString("email", email);
					
				return (Account) query.uniqueResult();	
			
		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}
	}
	
	public Account getAccountByName(String userName){
		org.hibernate.Session session = null;
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			session =  sessionFactory.openSession();
			Query query = session.createQuery("FROM Account p where p.username = :userName");
			query.setString("userName", userName);
			return (Account) query.uniqueResult();
		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}finally {
			if(null != session){
				session.clear();
			}
		}

	}
	
	public static void main(String[] args) {
		System.out.println(new AccountModel().getAccountByName("quyenafc"));
	}
	
}
