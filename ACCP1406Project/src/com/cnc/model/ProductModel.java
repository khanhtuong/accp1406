package com.cnc.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.cnc.entities.Account;
import com.cnc.entities.Category;
import com.cnc.entities.Product;
import com.cnc.entities.Supplier;
import com.sun.xml.internal.messaging.saaj.packaging.mime.util.QEncoderStream;

public class ProductModel extends AbstractModel<Product>  {
     public ProductModel(){
    	 super(Product.class);
     }
     
	public ArrayList<Product> getProductByCategoryName(String categoryName) {
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			Query query = sessionFactory.getCurrentSession()
					.createQuery("FROM Product p where p.category.name = :categoryName");
			query.setString("categoryName", categoryName);
			return  (ArrayList<Product>) query.list();

		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}
	}
	
	public Product getProductByName(String productName){
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			Query query = sessionFactory.getCurrentSession()
					.createQuery("FROM Product p where p.name = :productName");
			query.setString("productName", productName);
			return (Product) query.uniqueResult();
		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}

	}
	
	
	
	public List<Product> get3ProductByCategoryNameDesc(String categoryName){
		Session session = null;
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			session = sessionFactory.openSession();
			Query query = session
					.createQuery("FROM Product p where p.category.name = :categoryName order by p.productId desc");
			query.setString("categoryName", categoryName);
			query.setMaxResults(3);
			return  (ArrayList<Product>) query.list();

		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}
		finally {
			if(null != session){
				session.clear();
			}
		}
	}
	
	public ArrayList<Product> getListSupplierLimitedBy4Item(String supplierName) {
		Session session = null;
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			session = sessionFactory.openSession();
			Query query = session
					.createQuery("FROM Product p where p.supplier.name = :supplierName order by p.productId desc");
			query.setString("supplierName", supplierName);
			query.setMaxResults(4);
			return (ArrayList<Product>) query.list();

		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}finally {
			if(null != session){
				session.clear();
			}
		}

	}
	
	public ArrayList<Product> getListProductLimitedBy6ItemAndCurrent(){
		Session session = null;
		
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			session = sessionFactory.openSession();
			Query query = session.createQuery("FROM Product p order by p.productId desc");
					query.setMaxResults(6);
			return  (ArrayList<Product>) query.list();

		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}finally {
			// close session
			if (null != session) {
				session.clear();
			}
		
		}
		
	}
	
	public Product findProductById(int id){
		Session session = null;
		
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			session = sessionFactory.openSession();
			Query query = session.createQuery("FROM Product p where p.productId = :id");
			query.setInteger("id", id);
			return  (Product) query.uniqueResult();

		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}finally {
			// close session
			if (null != session) {
				session.clear();
			}
		
		}
		
	}
	
	public List<Product> searchProduct(String textSearch){
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			Query query = sessionFactory.getCurrentSession()
					.createQuery("FROM Product p where p.name like  :textSearch or p.category.name like :textSearch ");
			query.setString("textSearch", "%"+textSearch +"%");
			query.setString("textSearch", "%"+textSearch + "%");
			return  (List<Product>) query.list();

		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}
	}
	
	public static void main(String args []){
		
	}
}
