package com.cnc.model;

import org.hibernate.Query;

import com.cnc.entities.Category;
import com.cnc.entities.Transfer;

public class TransferModel extends AbstractModel<Transfer> {

	public TransferModel() {
		super(Transfer.class);
	}

}
