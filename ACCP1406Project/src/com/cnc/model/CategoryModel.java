package com.cnc.model;

import java.util.ArrayList;
import java.util.List;


import org.hibernate.Query;
import org.hibernate.Session;

import com.cnc.entities.*;

public class CategoryModel  extends AbstractModel<Category> {

	
	public CategoryModel() {
		super(Category.class);
	}
	//User
	public Category getCategoryByName(String categoryName){
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			Query query = sessionFactory.getCurrentSession()
					.createQuery("FROM Category c where c.name = :name");
					query.setString("name", categoryName);
				
				return (Category) query.list();	
			
		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}
	}
	
	public List<Category> getListCategory(){
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			Query query = sessionFactory.getCurrentSession()
					.createQuery("FROM Category c where c.status = true");
				return (List<Category>) query.list();	
			
		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}
	}
	
	
public static void main(String[] args){
		
		System.out.println(new CategoryModel().getListCategory());
	}
	
	
	
	
	
	
	
	

	  //Admin
	
	
	@SuppressWarnings("unchecked")
	public List<String> search(String name){
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			return sessionFactory.getCurrentSession().createQuery("select name from Category where name like :name").setString("name", name + "%").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Category> searchname(String name){
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			return sessionFactory.getCurrentSession().createQuery("select ca from Category ca where ca.name like :name").setString("name", "%" + name + "%").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}

	
}
