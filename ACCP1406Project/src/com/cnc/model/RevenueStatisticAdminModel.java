package com.cnc.model;
import java.text.SimpleDateFormat;
import java.util.*;

import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.cnc.common.*;
import com.cnc.controller.admin.ProductControlleradmin;

public class RevenueStatisticAdminModel extends AbstractModel<RevenueStatistic> {

	public RevenueStatisticAdminModel(){
		super(RevenueStatistic.class);
	}
	@SuppressWarnings("unchecked")
	public List<RevenueStatistic> findllproce(Date todate,Date fromdate){
		Session session = null;
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive()) {
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			 session = sessionFactory.openSession();
	     
			return session.createQuery("select ors.datecreation as datecreation,"
					+ " sum(orde.price * orde.quantity) as Revenues,"
					+ " sum((orde.price * orde.quantity) - (orde.quantity * pr.originalPrice)) as Benefit"
					+ " from Orders ors,Ordersdetail orde,Product pr "
					+ " where ors.ordersId=orde.orders.ordersId and pr.productId=orde.product.productId and "
					+ " ors.datecreation<=:todate  and ors.datecreation>=:fromdate and ors.status=0 group by ors.datecreation").setDate("todate", todate)
					.setDate("fromdate",fromdate)
					.setResultTransformer(Transformers.aliasToBean(RevenueStatistic.class)).list();
			
			
		} catch (RuntimeException re) {
			System.out.println(re.getMessage());
			return null;
		}finally {
			if(null != session){	

			session.clear();
			}
		}
	}
	
	

	
	public static void main(String[] args){
    RevenueStatisticAdminModel remodel =  new RevenueStatisticAdminModel();
	SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd");
	     Calendar c = Calendar.getInstance();
	     c.setTime(new Date());
	 
	      
     try {
    	 
    
    	  
    	  for(RevenueStatistic re:remodel.findllproce(sdf.parse(""+c.get(Calendar.YEAR)+"-12-31"),sdf.parse(""+c.get(Calendar.YEAR)+"-01-01"))){
    		  System.out.println(re.getRevenues());
    	  }
	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	
    

    
	}
}
