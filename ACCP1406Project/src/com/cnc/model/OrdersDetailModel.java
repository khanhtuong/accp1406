package com.cnc.model;

import java.util.List;

import org.hibernate.Query;

import com.cnc.entities.Ordersdetail;

public class OrdersDetailModel extends AbstractModel<Ordersdetail> {
	public OrdersDetailModel(){
		super(Ordersdetail.class);
	}
	
	public List<Ordersdetail> getOdersDetailById(Integer id){
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			Query query = sessionFactory.getCurrentSession()
					.createQuery("FROM Ordersdetail p where p.orders.ordersId = :id");
					query.setInteger("id",id);
					return (List<Ordersdetail>) query.list();			
		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}
	}
}
