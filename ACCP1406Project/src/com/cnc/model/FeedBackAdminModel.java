package com.cnc.model;

import java.util.List;

import org.hibernate.Session;

import com.cnc.entities.*;

public class FeedBackAdminModel extends AbstractModel<FeedBack>{

	
	@SuppressWarnings("unchecked")
	public List<String> searchfullname(String fullname){
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			return sessionFactory.getCurrentSession().createQuery("select fullName from FeedBack where fullName like :fullname").setString("fullname", fullname + "%").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<FeedBack> searchfeedback(String fullname,String phone){
		Session session = null;
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			 session = sessionFactory.openSession();
			return  session
					.createQuery("select fe from FeedBack fe where fe.fullName like :fullName and fe.phone like:phone")
					.setString("fullName", "%" + fullname + "%").setString("phone","%" + phone + "%").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<FeedBack> listfeebackordeby(){
		Session session = null;
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			 session = sessionFactory.openSession();
			return session.createQuery("select fe from FeedBack fe order by fe.createDate desc").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	
	public FeedBack findfeedback(int id){
		Session session = null;
		try {
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				 sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			 session = sessionFactory.openSession();
			return (FeedBack) session.
					createQuery("select f from FeedBack f where f.feedBackId =:feedBackId")
					.setInteger("feedBackId", id)
				
			        .uniqueResult();
			
		} catch (RuntimeException re) {
			
			System.out.println(re.getMessage());
			return null;
		}
		
		
	}
	
	
	public FeedBackAdminModel(){
		super(FeedBack.class);
	}
	
	public List<FeedBack> getListFeedBack(){
		Session session = null;
		try {
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				 sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			 session = sessionFactory.openSession();
			return  session.createQuery("From FeedBack f").list();
			
		} catch (RuntimeException re) {
			
			System.out.println(re.getMessage());
			return null;
		}
		
	}
	public static void main(String[] args){
		
		System.out.println(new FeedBackAdminModel().findAll().size());
	}
	
}
