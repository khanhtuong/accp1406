package com.cnc.model;

import org.hibernate.Query;

import com.cnc.entities.Account;
import com.cnc.entities.Role;
import com.sun.org.apache.bcel.internal.generic.RETURN;

public class RoleModel extends AbstractModel<Role> {
	public RoleModel() {
		super(Role.class);
	}
	
	public Role findByName(String name) {
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			Query query = sessionFactory.getCurrentSession()
					.createQuery("FROM Role p where p.name = :name");
					query.setString("name", name);
			return  (Role) query.uniqueResult();
			
		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}
	};
	
	public static void main(String[] args){
		System.out.println(new RoleModel().findByName("user").getName());
		
	}
}
