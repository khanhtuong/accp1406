package com.cnc.model;

import java.util.List;

import org.hibernate.Session;

import com.cnc.entities.*;



public class CategoryAdminModel  extends AbstractModel<Category> {

	
	public CategoryAdminModel() {
		super(Category.class);
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<String> searchname(String name){
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			return sessionFactory.getCurrentSession().createQuery("select name from Category where name like :name").setString("name", name + "%").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Category> searchca(String name){
		Session session = null;
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			 session = sessionFactory.openSession();
			return session.createQuery("select ca from Category ca where ca.name like :name").setString("name", "%" + name + "%").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	
	public Category findacategoryadmin(int id){
		Session session = null;
		try {
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				 sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			 session = sessionFactory.openSession();
			return (Category) session.
					createQuery("select ac from Category ac where ac.categoryId =:categoryId")
					.setInteger("categoryId", id)
				
			        .uniqueResult();
			
		} catch (RuntimeException re) {
			
			System.out.println(re.getMessage());
			return null;
		}
		
		
	}
	
	
	
	public static void main(String[] args){
	      
		
		System.out.println(new CategoryAdminModel().findAll().size());
	}
}
