package com.cnc.model;

import com.cnc.entities.Payment;

public class PaymentModel extends AbstractModel<Payment> {
	public PaymentModel(){
		super(Payment.class);
	}
}
