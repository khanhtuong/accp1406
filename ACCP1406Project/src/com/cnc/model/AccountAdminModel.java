package com.cnc.model;

import com.cnc.model.*;

import java.util.List;

import org.hibernate.Session;

import com.cnc.common.AccountViewModel;
import com.cnc.entities.*;

public class AccountAdminModel extends AbstractModel<Account> {

	
	public AccountAdminModel(){
		super(Account.class);
	}
	
	
	
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<String> searchusernameadmin(String username){
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			return sessionFactory.getCurrentSession().createQuery("select username from Account where username like :username").setString("username", username + "%").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	
	
	
	

	public Account loginaccountadmin(String username,String password){
		try {
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				 sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			
			return (Account) sessionFactory.getCurrentSession().
					createQuery("select ac from Account ac where ac.username =:username and ac.password=:password")
					.setString("username", username)
					.setString("password", password)
			        .uniqueResult();
			
		} catch (RuntimeException re) {
			
			System.out.println(re.getMessage());
			return null;
		}
		
	}
	
	public Account findaccountadmin(String username){
		Session session = null;
		try {
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				 sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			 session = sessionFactory.openSession();
			return (Account) session.
					createQuery("select ac from Account ac where ac.username =:username")
					.setString("username", username)
				
			        .uniqueResult();
			
		} catch (RuntimeException re) {
			
			System.out.println(re.getMessage());
			return null;
		}
		
		
	}
	@SuppressWarnings("unchecked")
	public List<Account> searchaccountadmin(String username){
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			return sessionFactory.getCurrentSession().createQuery("select ac from Account ac where ac.username like :username").setString("username", "%" + username + "%").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Account> findaccountadmin(){
    	Session session = null;
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
		      session = sessionFactory.openSession();
			return        session.createQuery("select ac from Account ac").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	
	
	public static void main(String [] args){
		
		
		try{
			System.out.println(new AccountAdminModel().findAll().size());
		}catch (Exception e) {
		System.out.println(e.getMessage());
		}
		
	
		
	}
	
}
