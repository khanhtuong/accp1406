package com.cnc.model;



import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.cnc.entities.Account;
import com.cnc.entities.Category;
import com.cnc.entities.Product;
import com.cnc.entities.Supplier;







public class ProductAdminModel extends AbstractModel<Product>{

	
	public ProductAdminModel(){
		super(Product.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<String> searchnamepr(String name){
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			return sessionFactory.getCurrentSession().createQuery("select name from Product where name like :name").setString("name", name + "%").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Product> searchpr(String name){
		Session session = null;
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			 session = sessionFactory.openSession();
			return session.createQuery("select pr from Product pr where pr.name like :name").setString("name", "%" + name + "%").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Product findproduct(int id){
		Session session = null;
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			 session = sessionFactory.openSession();
			return (Product) session
					.createQuery("select pr from Product pr where pr.productId=:id")
					.setInteger("id", id).uniqueResult();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	

	@SuppressWarnings("unchecked")
	public List<Product> listproductordeby(){
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			return sessionFactory.getCurrentSession().createQuery("select pr from Product pr order by pr.createDate desc").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Product> listtws(int page, int pagesize){
		Session session = null;
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
				
			}
			 session = sessionFactory.openSession();
			return session.createQuery("select pr from Product pr  order by pr.createDate desc")
				.setMaxResults(pagesize).setFirstResult(page).list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}finally {
			if(null != session){
				
				session.clear();
			}
		}
	}
	

	public static void main(String args[]){
		
	  
//		ProductAdminModel model  =  new ProductAdminModel();
//
//          
//		Product product =  model.findproduct(56);
//		product.setName("test417");
//		product.setPhoto("aaaaaa");
//		product.setPrice(12);
//		product.setOriginalPrice(Long.parseLong("8"));
//		product.setPromotionPrice(Long.parseLong("4"));
//		product.setQuantity(21);
//		product.setUpdateDate(new Date());
//		product.setUpdateBy("quyenafc");
//		product.setStatus(true);
//	    Category ca =  new CategoryAdminModel().find(1);
//	    Supplier su =  new CategoryAdminModel().find(1);
//		product.setCategory(ca);
		
		
	
	}
	
}
