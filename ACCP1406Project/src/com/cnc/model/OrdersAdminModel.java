package com.cnc.model;

import java.util.List;

import org.hibernate.Session;

import com.cnc.entities.Account;
import com.cnc.entities.FeedBack;
import com.cnc.entities.Orders;
import com.cnc.entities.Ordersdetail;
import com.cnc.entities.OrdersdetailId;
import com.cnc.entities.Product;

public class OrdersAdminModel extends AbstractModel<Orders>{

	
	
	public OrdersAdminModel(){
		super(Orders.class);
	}
	@SuppressWarnings("unchecked")
	public List<String> searchcustomerName(String customerName){
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			return sessionFactory.getCurrentSession().createQuery("select customerName from Orders where customerName like :customerName")
					.setString("customerName", customerName + "%").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<String> searchusername(String username){
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			return sessionFactory.getCurrentSession().createQuery("select account.username from Orders where account.username like :username")
					.setString("username", username + "%").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Orders> searchordersfisrt(String customername,String phone){
		Session session = null;
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
		      session = sessionFactory.openSession();
			return  session.
					createQuery("select ors from Orders ors where ors.customerName  like :customerName or ors.customerMobile=:phone")
					.setString("customerName", customername ).setString("phone", phone).list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	
	

	
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<Orders> searchorderssecond(String username){
		Session session = null;
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
		      session = sessionFactory.openSession();
			return  session.
					createQuery("select ors from Orders ors where ors.account.username like:username")
					.setString("username","%" +  username + "%").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Orders> listtws(int page, int pagesize){
    	Session session = null;
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
		      session = sessionFactory.openSession();
			return session.createQuery("select ors from Orders ors order by ors.datecreation desc")
				.setMaxResults(pagesize).setFirstResult(page).list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	public Orders findordersadmin(int id){
		Session session = null;
		try {
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				 sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			 session = sessionFactory.openSession();
			return (Orders) session.
					createQuery("select ors from Orders ors where ors.ordersId =:ordersId")
					.setInteger("ordersId", id)
				
			        .uniqueResult();
			
		} catch (RuntimeException re) {
			
			System.out.println(re.getMessage());
			return null;
		}
		
		
	}
	
	public static void main(String[] args){
		
//		
//		for(Orders or:new OrdersModel().searchordersfisrt("")){
//			
//			
//			System.out.println(or.getTotalmoney());
//			
//		}
	

		for(Orders or :new OrdersAdminModel().searchordersfisrt("%anh%", "")){
			System.out.println(or.getCustomerMobile());
			System.out.println(or.getCustomerName());
		}
		
	
	
	}
}
