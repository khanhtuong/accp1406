package com.cnc.model;

import java.util.*;

import org.hibernate.Query;
import org.hibernate.Session;

import com.cnc.entities.Account;
import com.cnc.entities.Category;
import com.cnc.entities.Product;
import com.cnc.entities.Supplier;

import antlr.collections.List;

public class SupplierModel extends AbstractModel<Supplier> {
	public SupplierModel() {
		super(Supplier.class);
	}

	public ArrayList<Supplier> getListSupplieres(){
		Session session = null;
		try {
			session = sessionFactory.openSession();
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			Query query = sessionFactory.getCurrentSession()
					.createQuery("FROM Supplier s where s.status = true");
				return (ArrayList<Supplier>) query.list();	
			
		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}
		finally {
			if(null != session){
				session.clear();
			}
		}
	}

	
	
	
	
	public static void main(String[] args) {

		System.out.println(new SupplierModel().getListSupplieres());
		java.util.List<Supplier> list = new SupplierModel().getListSupplieres();
		for(int i = 0; i< list.size(); i++ ){
			System.out.println(list.get(i).getProducts());
		}
	}

}
