package com.cnc.model;

import java.util.List;

import org.hibernate.Query;

import com.cnc.entities.Account;
import com.cnc.entities.Orders;

public class OderModel extends AbstractModel<Orders> {
	public OderModel(){
		super(Orders.class);
	}
	
	public List<Orders> getOdersByUserName(String username){
		try {
			if (!sessionFactory.getCurrentSession().getTransaction().isActive())
				sessionFactory.getCurrentSession().getTransaction().begin();
			Query query = sessionFactory.getCurrentSession()
					.createQuery("FROM Orders p where p.account.username = :username");
					query.setString("username",username);
					return (List<Orders>) query.list();
			
		} catch (RuntimeException re) {
			sessionFactory.getCurrentSession().getTransaction().rollback();
			throw re;
		}
	}
	public static void main(String[] args) {
		System.out.println(new OderModel().getOdersByUserName("manhadmin"));		
	}
}
