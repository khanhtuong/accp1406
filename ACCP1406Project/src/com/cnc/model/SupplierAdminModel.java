package com.cnc.model;

import java.util.List;

import org.hibernate.Session;

import com.cnc.common.SupplierView;
import com.cnc.entities.*;

public class SupplierAdminModel extends AbstractModel<Supplier> {

	@SuppressWarnings("unchecked")
	public List<String> searchnamesupplier(String name){
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			return sessionFactory.getCurrentSession().createQuery("select name from Supplier where name like :name").setString("name", name + "%").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Supplier> searchsupplier(String name){
		try {
			
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			return sessionFactory.getCurrentSession().createQuery("select su from Supplier su where su.name like :name").setString("name", "%" + name + "%").list();
			
		} catch (Exception e) {
		
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	
	
	public Supplier findsuppiler(int id){
		Session session = null;
		try {
			if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
				 sessionFactory.getCurrentSession().getTransaction().begin();
			}
			
			 session = sessionFactory.openSession();
			return (Supplier) session.
					createQuery("select su from Supplier su where su.supplierId =:supplierId")
					.setInteger("supplierId", id)
				
			        .uniqueResult();
			
		} catch (RuntimeException re) {
			
			System.out.println(re.getMessage());
			return null;
		}
		
		
	}
	public SupplierAdminModel() {
		super(Supplier.class);
	}
	
	  
			
}
