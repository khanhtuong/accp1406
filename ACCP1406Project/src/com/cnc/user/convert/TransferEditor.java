package com.cnc.user.convert;

import java.beans.PropertyEditorSupport;

import com.cnc.entities.Transfer;
import com.cnc.model.PaymentModel;
import com.cnc.model.TransferModel;

public class TransferEditor extends PropertyEditorSupport {

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		TransferModel transferModel = new TransferModel();
		this.setValue(transferModel.find(Integer.parseInt(text)));
	}
	
}