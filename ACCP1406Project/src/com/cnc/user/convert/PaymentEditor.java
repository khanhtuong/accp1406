package com.cnc.user.convert;

import java.beans.PropertyEditorSupport;

import com.cnc.model.PaymentModel;

public class PaymentEditor extends PropertyEditorSupport {

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		PaymentModel paymentModel = new PaymentModel();
		this.setValue(paymentModel.find(Integer.parseInt(text)));
	}

}
