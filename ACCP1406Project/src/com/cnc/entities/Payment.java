package com.cnc.entities;
// Generated Oct 10, 2016 9:18:48 AM by Hibernate Tools 5.1.0.Alpha1

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Payment generated by hbm2java
 */
@Entity
@Table(name = "payment", catalog = "mydemoaptech")
public class Payment implements Serializable {

	private int paymentId;
	private String paymentName;
	private Set<Orders> orderses = new HashSet<Orders>(0);

	public Payment() {
	}

	public Payment(int paymentId) {
		this.paymentId = paymentId;
	}

	public Payment(int paymentId, String paymentName, Set<Orders> orderses) {
		this.paymentId = paymentId;
		this.paymentName = paymentName;
		this.orderses = orderses;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "paymentId", unique = true, nullable = false)
	public int getPaymentId() {
		return this.paymentId;
	}

	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}

	@Column(name = "paymentName")
	public String getPaymentName() {
		return this.paymentName;
	}

	public void setPaymentName(String paymentName) {
		this.paymentName = paymentName;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "payment")
	public Set<Orders> getOrderses() {
		return this.orderses;
	}

	public void setOrderses(Set<Orders> orderses) {
		this.orderses = orderses;
	}

}
