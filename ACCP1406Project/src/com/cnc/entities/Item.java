package com.cnc.entities;

import java.io.Serializable;

public class Item implements Serializable {

	

	private Product product;
	private int quantity;

	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Item(Product product, int quantity) {
		super();
		this.product = product;
		this.quantity = quantity;
	}

	public Item() {
		super();
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.quantity +""+ this.product;
	}
}
