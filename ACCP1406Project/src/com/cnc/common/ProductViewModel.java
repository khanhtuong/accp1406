package com.cnc.common;

import java.util.ArrayList;
import java.util.List;

import com.cnc.entities.Category;
import com.cnc.entities.Product;
import com.cnc.model.*;

public class ProductViewModel {

	
	ProductAdminModel productmodel =  new ProductAdminModel();
	CategoryviewModel cateviewmodel =  new CategoryviewModel();
	SupplierViewModel suppviewmodel = new SupplierViewModel();
	
	public List<ProductView> findll() {
		List<ProductView> list = new ArrayList<ProductView>();

		for (Product pr : productmodel.listproductordeby()) {
		     list.add(new ProductView(pr.getProductId(), pr.getName(), pr.getPrice(), pr.getQuantity(), pr.getDescription(),
		    		 pr.getPhoto(), pr.getCreateDate(), pr.getCreateBy(), pr.getUpdateDate(), pr.getUpdateBy(),
		    		 pr.isStatus(), pr.getPromotionPrice(), pr.getOriginalPrice(),			
		    		 cateviewmodel.findca(pr.getCategory().getCategoryId()),suppviewmodel.findsuadmin(pr.getSupplier().getSupplierId()) ));
		}

		return list;
	}
	
	
	public List<ProductView> searchpr(String name) {
		List<ProductView> list = new ArrayList<ProductView>();

		for (Product pr : productmodel.searchpr(name)) {
		    list.add(new ProductView(pr.getProductId(), pr.getName(), pr.getPrice(), pr.getQuantity(), pr.getDescription(),
		    		 pr.getPhoto(), pr.getCreateDate(), pr.getCreateBy(), pr.getUpdateDate(), pr.getUpdateBy(),
		    		 pr.isStatus(), pr.getPromotionPrice(), pr.getOriginalPrice(),
		    		 cateviewmodel.findca(pr.getCategory().getCategoryId()),suppviewmodel.findsuadmin(pr.getSupplier().getSupplierId()) ));
		}

		return list;
	}
	
	public List<ProductView> listtwsview(int page, int pagesize) {
		List<ProductView> list = new ArrayList<ProductView>();

		for (Product pr : productmodel.listtws(page, pagesize)) {
			list.add(new ProductView(pr.getProductId(), pr.getName(), pr.getPrice(), pr.getQuantity(),
					pr.getDescription(), pr.getPhoto(), pr.getCreateDate(), pr.getCreateBy(), pr.getUpdateDate(),
					pr.getUpdateBy(), pr.isStatus(), pr.getPromotionPrice(), pr.getOriginalPrice(),
					cateviewmodel.findca(pr.getCategory().getCategoryId()),
					suppviewmodel.findsuadmin(pr.getSupplier().getSupplierId())));
		}

		return list;
	}
	
	
	public ProductView findproadmin(int id) {
	

		Product pr =  productmodel.findproduct(id);

   
		return new ProductView(pr.getProductId(), pr.getName(), pr.getPrice(),
				pr.getQuantity(), pr.getDescription(), pr.getPhoto(), pr.getCreateDate(),
				pr.getCreateBy(), pr.getUpdateDate(),pr.getUpdateBy(), pr.isStatus(), pr.getPromotionPrice(), pr.getOriginalPrice(),
				cateviewmodel.findca(pr.getCategory().getCategoryId()),suppviewmodel.findsuadmin(pr.getSupplier().getSupplierId()));
	}
	
	public static void main(String args[]){
		try {
			System.out.println(new ProductViewModel().findll().size());
		} catch (Exception e) {
		System.out.println(e.getMessage());
		}
	
	}
	
}
