package com.cnc.common;

import java.util.Date;

public class SupplierView {
	private int supplierId;
	private String name;
	private String address;
	private String phone;
	private boolean status;
	private Date createdate;
	public int getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}

	
	
	public SupplierView(int supplierId, String name, String address, String phone, boolean status, Date createdate) {
		super();
		this.supplierId = supplierId;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.status = status;
		this.createdate = createdate;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public SupplierView() {
		super();
	}
	
	
}
