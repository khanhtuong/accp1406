package com.cnc.common;

import java.io.Serializable;
import java.util.Date;

public class RevenueStatistic implements Serializable{

	
	
	private Date datecreation;
	private long revenues;
	private long benefit;
	
	
	public RevenueStatistic(Date datecreation, long revenues, long benefit) {
		super();
		this.datecreation = datecreation;
		this.revenues = revenues;
		this.benefit = benefit;
	}
	public Date getDatecreation() {
		return datecreation;
	}
	public void setDatecreation(Date datecreation) {
		this.datecreation = datecreation;
	}
	public long getRevenues() {
		return revenues;
	}
	public void setRevenues(long revenues) {
		this.revenues = revenues;
	}
	public long getBenefit() {
		return benefit;
	}
	public void setBenefit(long benefit) {
		this.benefit = benefit;
	}
	public RevenueStatistic() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	
	
	
	
	
	
	
	

	
	
	
	
}
