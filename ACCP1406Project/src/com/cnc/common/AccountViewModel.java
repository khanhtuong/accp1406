package com.cnc.common;


import java.util.ArrayList;
import java.util.List;

import com.cnc.entities.*;
import com.cnc.model.*;
public class AccountViewModel {

	
	AccountAdminModel accountmodel =  new AccountAdminModel();
	
	
	public List<AccountView> findlluser() {
		List<AccountView> list = new ArrayList<AccountView>();

		for (Account ac : accountmodel.findaccountadmin()) {
			for(Role rl: ac.getRoles()){
				if(rl.getName().equals("user")){
					list.add(new AccountView(ac.getUsername(), "", ac.getFullname(),
							ac.getEmail(), ac.getAge(), ac.getCreatedate(),ac.getPhone(),ac.getAddress()));
				}
			}
	
		}

		return list;
	}
	
	public List<AccountView> findlladmin() {
		List<AccountView> list = new ArrayList<AccountView>();

		for (Account ac : accountmodel.findaccountadmin()) {
			for(Role rl: ac.getRoles()){
				if(rl.getName().equals("admin") || rl.getName().equals("superadmin")){
					list.add(new AccountView(ac.getUsername(), "", ac.getFullname(),
							ac.getEmail(), ac.getAge(), ac.getCreatedate(),ac.getPhone(),ac.getAddress()));
				}
			}
	
		}

		return list;
	}
	
	
	public List<AccountView> searchacuser(String username) {
		List<AccountView> list = new ArrayList<AccountView>();

		for (Account ac : accountmodel.searchaccountadmin(username)) {
			for(Role rl: ac.getRoles()){
				if(rl.getName().equals("user")){
					list.add(new AccountView(ac.getUsername(), "", ac.getFullname(),
							ac.getEmail(), ac.getAge(), ac.getCreatedate(),ac.getPhone(),ac.getAddress()));
				}
			}
	
		}

		return list;
	}
	
	public List<AccountView> searchacadmin(String username) {
		List<AccountView> list = new ArrayList<AccountView>();

		for (Account ac : accountmodel.searchaccountadmin(username)) {
			for(Role rl: ac.getRoles()){
				if(rl.getName().equals("admin") || rl.getName().equals("superadmin")){
					list.add(new AccountView(ac.getUsername(), "", ac.getFullname(),
							ac.getEmail(), ac.getAge(), ac.getCreatedate(),ac.getPhone(),ac.getAddress()));
				}
			}
	
		}

		return list;
	}
	
	public  AccountView  findusername(String username) {
		
         Account acccount =  accountmodel.findaccountadmin(username);

         
         AccountView accountview =  new AccountView(acccount.getUsername(), "", acccount.getFullname() ,acccount.getEmail()
        		 ,acccount.getAge(), acccount.getCreatedate(),acccount.getPhone(),acccount.getAddress());
         
		return accountview;
	}
	
	
	public static void main(String[] args){
		try{
			System.out.println(new AccountViewModel().findlluser().size());
		}catch (Exception e) {
		System.out.println(e.getMessage());
		}
		
	}
	
}
