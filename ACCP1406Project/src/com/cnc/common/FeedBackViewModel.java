package com.cnc.common;

import java.util.ArrayList;
import java.util.List;

import com.cnc.entities.*;
import com.cnc.model.*;

public class FeedBackViewModel {

	
	FeedBackAdminModel femodel = new FeedBackAdminModel();

	public List<FeedBackView> findlladmin() {
		List<FeedBackView> list = new ArrayList<FeedBackView>();

		for (FeedBack fe :  femodel.listfeebackordeby()) {
			list.add(new FeedBackView(fe.getFeedBackId(), fe.getFullName(), fe.getPhone(), fe.getEmail(), fe.getCreateDate(), fe.getTitle(), fe.getContent()));
		}

		return list;
	}
	
	public List<FeedBackView> searchfeedbackadmin(String fullname, String phone) {
		List<FeedBackView> list = new ArrayList<FeedBackView>();

		for (FeedBack fe :  femodel.searchfeedback(fullname, phone)) {
			list.add(new FeedBackView(fe.getFeedBackId(), fe.getFullName(), fe.getPhone(), fe.getEmail(), fe.getCreateDate(), fe.getTitle(), fe.getContent()));
		}

		return list;
	}
	
	
	
	public  FeedBackView  findfeeadmin(int id) {
		
       FeedBack feedback =  femodel.findfeedback(id);

        
        FeedBackView feedBackView =  new FeedBackView(feedback.getFeedBackId(), feedback.getFullName(), feedback.getPhone(),
        		feedback.getEmail(), feedback.getCreateDate(), feedback.getTitle(),feedback.getContent());
        
		return feedBackView;
	}
	
}
