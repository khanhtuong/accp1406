package com.cnc.common;

import java.util.ArrayList;
import java.util.List;

import com.cnc.entities.Transfer;

import com.cnc.model.TransferAdminModel;

public class TransferViewModel {

	
	
	TransferAdminModel transferModel =  new TransferAdminModel();
	
	
	public List<TransferView> findlladmin() {
		List<TransferView> list = new ArrayList<TransferView>();

		for (Transfer tr : transferModel.findAll()) {
			list.add(new TransferView(tr.getTransferId(), tr.getTransferName()));
		}

		return list;
	}
	
	
	public TransferView findadmin(int id) {
		
  
		Transfer transfer =  transferModel.findtransfer(id);
		
		TransferView transferView =  new TransferView(transfer.getTransferId(), transfer.getTransferName());

		return transferView;
	}
	
	

}
