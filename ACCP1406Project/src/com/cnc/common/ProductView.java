package com.cnc.common;


import java.io.Serializable;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.cnc.entities.Category;
import com.cnc.entities.Supplier;



public class ProductView implements Serializable {
	private int productId;

	private String name;
	private long price;
	private int quantity;
	private String description;
	private String photo;
	private Date createDate;
	private String createBy;
	private Date updateDate;
	private String updateBy;
	private boolean status;
	private Long promotionPrice;
	private long originalPrice;
	private CategoryView categoryview;
	private SupplierView supplierview;
	private MultipartFile file;
	
	
	


	public CategoryView getCategoryview() {
		return categoryview;
	}
	public void setCategoryview(CategoryView categoryview) {
		this.categoryview = categoryview;
	}
	public SupplierView getSupplierview() {
		return supplierview;
	}
	public void setSupplierview(SupplierView supplierview) {
		this.supplierview = supplierview;
	}
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getPrice() {
		return price;
	}
	public void setPrice(long price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public Long getPromotionPrice() {
		return promotionPrice;
	}
	public void setPromotionPrice(Long promotionPrice) {
		this.promotionPrice = promotionPrice;
	}
	public long getOriginalPrice() {
		return originalPrice;
	}
	public void setOriginalPrice(long originalPrice) {
		this.originalPrice = originalPrice;
	}
	
	
	
	



	
	public ProductView(int productId, String name, long price, int quantity, String description, String photo,
			Date createDate, String createBy, Date updateDate, String updateBy, boolean status, Long promotionPrice,
			long originalPrice, CategoryView categoryview, SupplierView supplierview) {
		super();
		this.productId = productId;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
		this.description = description;
		this.photo = photo;
		this.createDate = createDate;
		this.createBy = createBy;
		this.updateDate = updateDate;
		this.updateBy = updateBy;
		this.status = status;
		this.promotionPrice = promotionPrice;
		this.originalPrice = originalPrice;
		this.categoryview = categoryview;
		this.supplierview = supplierview;
	
	}
	public ProductView(int productId, String name, long price, int quantity, String description, String photo,
			Date createDate, String createBy, Date updateDate, String updateBy, boolean status, Long promotionPrice,
			long originalPrice) {
		super();
		this.productId = productId;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
		this.description = description;
		this.photo = photo;
		this.createDate = createDate;
		this.createBy = createBy;
		this.updateDate = updateDate;
		this.updateBy = updateBy;
		this.status = status;
		this.promotionPrice = promotionPrice;
		this.originalPrice = originalPrice;

	}
	public ProductView() {
		this.categoryview = new CategoryView();
		this.supplierview = new SupplierView();
		// TODO Auto-generated constructor stub
	}

	
	
	
	
	
	
}
