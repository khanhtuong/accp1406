package com.cnc.common;

public class PaymentView {

	
	
	private int paymentId;
	private String paymentName;
	public int getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}
	public String getPaymentName() {
		return paymentName;
	}
	public void setPaymentName(String paymentName) {
		this.paymentName = paymentName;
	}
	public PaymentView(int paymentId, String paymentName) {
		super();
		this.paymentId = paymentId;
		this.paymentName = paymentName;
	}
	public PaymentView() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}
