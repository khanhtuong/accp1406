package com.cnc.common;

import java.util.ArrayList;
import java.util.List;

import com.cnc.entities.Product;
import com.cnc.entities.Supplier;
import com.cnc.model.*;

public class SupplierViewModel {

	
	SupplierAdminModel sumodel = new SupplierAdminModel();

	public List<SupplierView> findlladmin() {
		List<SupplierView> list = new ArrayList<SupplierView>();

		for (Supplier su :  sumodel.findAll()) {
			list.add(new SupplierView(su.getSupplierId(), su.getName(), su.getAddress(), su.getPhone(), su.isStatus(), su.getCreatedate()));
		}

		return list;
	}
	
	public List<SupplierView> searchsupplieradmin(String name) {
		List<SupplierView> list = new ArrayList<SupplierView>();

		for (Supplier su : sumodel.searchsupplier(name)) {
		    list.add(new SupplierView(su.getSupplierId(),su.getName(),su.getAddress(),su.getPhone(),su.isStatus(),su.getCreatedate()));
		}

		return list;
	}

	
	public  SupplierView  findsuadmin(int id) {
		
      Supplier supplier =  sumodel.findsuppiler(id);

        
      SupplierView supplierview =  new SupplierView(supplier.getSupplierId(), supplier.getName(), supplier.getAddress(),supplier.getPhone(), supplier.isStatus(), supplier.getCreatedate());
        
		return supplierview;
	}
	
	
}
