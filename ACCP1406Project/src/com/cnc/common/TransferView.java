package com.cnc.common;

public class TransferView {

	
	private int transferId;
	private String transferName;
	public int getTransferId() {
		return transferId;
	}
	public void setTransferId(int transferId) {
		this.transferId = transferId;
	}
	public String getTransferName() {
		return transferName;
	}
	public void setTransferName(String transferName) {
		this.transferName = transferName;
	}
	public TransferView(int transferId, String transferName) {
		super();
		this.transferId = transferId;
		this.transferName = transferName;
	}
	public TransferView() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
