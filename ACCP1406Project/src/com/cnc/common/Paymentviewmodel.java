package com.cnc.common;

import java.util.ArrayList;
import java.util.List;

import com.cnc.entities.*;
import com.cnc.model.*;

public class Paymentviewmodel {

	
	
	PaymentAdminModel paymentModel =  new PaymentAdminModel();
	
	
	public List<PaymentView> findlladmin() {
		List<PaymentView> list = new ArrayList<PaymentView>();

		for (Payment pay :  paymentModel.findAll()) {
			list.add(new PaymentView(pay.getPaymentId(),pay.getPaymentName()) );
		}

		return list;
	}
	
	
	public PaymentView findadmin(int id) {
		
  
		Payment payment =  paymentModel.find(id);
		
		PaymentView paymentview =  new PaymentView(payment.getPaymentId(), payment.getPaymentName());

		return paymentview;
	}
	
}
