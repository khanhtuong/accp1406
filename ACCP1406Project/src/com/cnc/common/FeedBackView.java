package com.cnc.common;

import java.util.Date;

public class FeedBackView {

	private int feedBackId;
	private String fullName;
	private String phone;
	private String email;
	private Date createDate;
	private String title;
	private String content;
	public int getFeedBackId() {
		return feedBackId;
	}
	public void setFeedBackId(int feedBackId) {
		this.feedBackId = feedBackId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public FeedBackView(int feedBackId, String fullName, String phone, String email, Date createDate, String title,
			String content) {
		super();
		this.feedBackId = feedBackId;
		this.fullName = fullName;
		this.phone = phone;
		this.email = email;
		this.createDate = createDate;
		this.title = title;
		this.content = content;
	}
	public FeedBackView() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
