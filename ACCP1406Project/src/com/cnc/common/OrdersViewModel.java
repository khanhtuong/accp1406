package com.cnc.common;

import java.util.ArrayList;
import java.util.List;

import com.cnc.entities.*;
import com.cnc.model.*;

public class OrdersViewModel {

	
	private OrdersAdminModel ordersmodel =  new OrdersAdminModel();
	
	
	
	public List<OrdersView> findll() {
		List<OrdersView> list = new ArrayList<OrdersView>();

		for (Orders or : ordersmodel.findAll()) {
			if(or.getAccount()!=null){
			     list.add(new OrdersView(or.getOrdersId(),or.getDatecreation(), or.getTotalmoney(), or.getStatus(),or.getCustomerName()
			    		 , or.getCustomerMobile(), or.getAccount().getUsername()));
			}else{
			     list.add(new OrdersView(or.getOrdersId(),or.getDatecreation(), or.getTotalmoney(), or.getStatus(),or.getCustomerName()
			    		 , or.getCustomerMobile(), ""));
			}
	
		}
		
		

		return list;
	}
	
	public List<OrdersView> listtws(int page, int pagesize) {
		List<OrdersView> list = new ArrayList<OrdersView>();

		for (Orders ors : ordersmodel.listtws(page,pagesize)) {
		
			if(ors.getAccount()!=null){
			     list.add(new OrdersView(ors.getOrdersId(),ors.getDatecreation(), ors.getTotalmoney(), ors.getStatus(),ors.getCustomerName()
			    		 , ors.getCustomerMobile(), ors.getAccount().getUsername()));
			}else{
			     list.add(new OrdersView(ors.getOrdersId(),ors.getDatecreation(), ors.getTotalmoney(), ors.getStatus(),ors.getCustomerName()
			    		 , ors.getCustomerMobile(), ""));
			}
		
		}
		
		

		return list;
	}

	public List<OrdersView> searchorderviewfisrt(String customername,String phone){
		List<OrdersView> list = new ArrayList<OrdersView>();

		for (Orders ors :  ordersmodel.searchordersfisrt(customername,phone)) {
	
			if(ors.getAccount()!=null){
			     list.add(new OrdersView(ors.getOrdersId(),ors.getDatecreation(), ors.getTotalmoney(), ors.getStatus(),ors.getCustomerName()
			    		 , ors.getCustomerMobile(), ors.getAccount().getUsername()));
			}else{
			     list.add(new OrdersView(ors.getOrdersId(),ors.getDatecreation(), ors.getTotalmoney(), ors.getStatus(),ors.getCustomerName()
			    		 , ors.getCustomerMobile(), ""));
			}
		
			
		}

		return list;
	}
	
//	
//	public findorder findorder(int id){
//		Orders or =  ordersmodel.find(id);
//		
//		for(Ordersdetail orde:or.getOrdersdetails()){
//			return new findorder(or.getOrdersId(), o, orde.getProduct().getName(), or.getCustomerAddress(), or.getCustomerMessage(), or.getStatus());
//		}
//	}
//	
	

	public List<OrdersView> searchorderviewsecond(String customerMobile){
		List<OrdersView> list = new ArrayList<OrdersView>();

		for (Orders ors :  ordersmodel.searchorderssecond(customerMobile)) {
			if(ors.getAccount()!=null){
			     list.add(new OrdersView(ors.getOrdersId(),ors.getDatecreation(), ors.getTotalmoney(), ors.getStatus(),ors.getCustomerName()
			    		 , ors.getCustomerMobile(), ors.getAccount().getUsername()));
			}else{
			     list.add(new OrdersView(ors.getOrdersId(),ors.getDatecreation(), ors.getTotalmoney(), ors.getStatus(),ors.getCustomerName()
			    		 , ors.getCustomerMobile(), ""));
			}
		}

		return list;
	}
	
	
	public static void main(String[] args){
		
		try {
			for(OrdersView orview:new OrdersViewModel().listtws(1, 9)){
				System.out.println(orview.getCustomerName());
				
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
}
