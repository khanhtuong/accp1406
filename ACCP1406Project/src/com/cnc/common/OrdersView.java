package com.cnc.common;

import java.util.Date;

public class OrdersView {
	private int ordersId;
	private Date datecreation;
	private long totalmoney;
	private boolean status;
	private String customerName;
	private String customerMobile;
	private String username;
	
	
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getOrdersId() {
		return ordersId;
	}
	public void setOrdersId(int ordersId) {
		this.ordersId = ordersId;
	}
	public Date getDatecreation() {
		return datecreation;
	}
	public void setDatecreation(Date datecreation) {
		this.datecreation = datecreation;
	}
	public long getTotalmoney() {
		return totalmoney;
	}
	public void setTotalmoney(long totalmoney) {
		this.totalmoney = totalmoney;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerMobile() {
		return customerMobile;
	}
	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}
	public OrdersView(int ordersId, Date datecreation, long totalmoney, boolean status, String customerName,
			String customerMobile, String username) {
		super();
		this.ordersId = ordersId;
		this.datecreation = datecreation;
		this.totalmoney = totalmoney;
		this.status = status;
		this.customerName = customerName;
		this.customerMobile = customerMobile;
		this.username = username;
	}
	public OrdersView() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	

	
	
}
