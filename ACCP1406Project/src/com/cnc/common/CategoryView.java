package com.cnc.common;

import java.util.Date;

public class CategoryView {

	
	private int categoryId;
	private String name;
	private String alias;
	private Date createDate;
	private String createBy;
	private Date updateDate;
	private String updateBy;
	private String description;
	private boolean status;
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public CategoryView() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CategoryView(int categoryId, String name, String alias, Date createDate, String createBy,
			Date updateDate, String updateBy, String description, boolean status) {
		super();
		this.categoryId = categoryId;
		this.name = name;
		this.alias = alias;
		this.createDate = createDate;
		this.createBy = createBy;
		this.updateDate = updateDate;
		this.updateBy = updateBy;
		this.description = description;
		this.status = status;
	}
 
	
	
	
}
