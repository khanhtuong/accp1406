package com.cnc.common;

public class findorder {

	
	
	public int id;
	public int quantity;
	public String productname;
	public String address;
	public String description;
	public boolean status;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public findorder() {
		super();
		// TODO Auto-generated constructor stub
	}

	public findorder(int id, int quantity, String productname, String address, String description, boolean status) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.productname = productname;
		this.address = address;
		this.description = description;
		this.status = status;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	
	
}
