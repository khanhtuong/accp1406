package com.cnc.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cnc.entities.Account;
import com.cnc.entities.Category;
import com.cnc.model.*;

public class CategoryviewModel {

	CategoryAdminModel camodel = new CategoryAdminModel();

	public List<CategoryView> findll() {
		List<CategoryView> list = new ArrayList<CategoryView>();

		for (Category ca : camodel.findAll()) {
			list.add(new CategoryView(ca.getCategoryId(), ca.getName(), ca.getAlias(), ca.getCreateDate(),
					ca.getCreateBy(), ca.getUpdateDate(), ca.getUpdateBy(), ca.getDescription(), ca.isStatus()));
		}

		return list;
	}
	
	
	public List<CategoryView> searchca(String name) {
		List<CategoryView> list = new ArrayList<CategoryView>();

		for (Category ca : camodel.searchca(name)) {
			list.add(new CategoryView(ca.getCategoryId(), ca.getName(), ca.getAlias(), ca.getCreateDate(),
					ca.getCreateBy(), ca.getUpdateDate(), ca.getUpdateBy(), ca.getDescription(), ca.isStatus()));
		}

		return list;
	}
	
	public  CategoryView  findca(int id) {
		
       Category category =  camodel.findacategoryadmin(id);

        
        CategoryView categoryview =  new CategoryView(category.getCategoryId(),category.getName(),category.getAlias(),category.getCreateDate(),category.getCreateBy(),category.getUpdateDate(),category.getUpdateBy(),category.getDescription(),category.isStatus());
        
		return categoryview;
	}
}
