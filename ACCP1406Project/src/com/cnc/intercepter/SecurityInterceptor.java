package com.cnc.intercepter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class SecurityInterceptor extends HandlerInterceptorAdapter {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String loginUrl = request.getContextPath() + "/home.html";
		if (request.getSession().getAttribute("cart") == null) {
			response.sendRedirect(loginUrl);
			return false;
		}
		return true;
	}
}
