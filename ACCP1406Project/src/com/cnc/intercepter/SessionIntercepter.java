package com.cnc.intercepter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class SessionIntercepter extends HandlerInterceptorAdapter {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String loginUrl = request.getContextPath() + "/home.html";
		if (request.getSession().getAttribute("username") == null) {
			response.sendRedirect(loginUrl);
			return false;
		}
		return true;
	}
}
