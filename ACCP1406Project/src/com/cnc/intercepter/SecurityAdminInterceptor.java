package com.cnc.intercepter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.client.RestTemplate;
import com.cnc.entities.Account;
import com.cnc.entities.Role;
import com.cnc.model.AccountModel;

public class SecurityAdminInterceptor extends HandlerInterceptorAdapter {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String loginUrl = request.getContextPath() + "/admin/login.html";
		Account account = new Account();
		AccountModel accountModel = new AccountModel();
	
		Set<Role> roles = accountModel.find(request.getSession().getAttribute("username")).getRoles();
		if (request.getSession().getAttribute("username") == null) {
			response.sendRedirect(loginUrl);
			return false;
		}
		if (request.getSession().getAttribute("username") != null) {
			for (Role role : roles) {
				if (role.getName().equals("admin") || role.getName().equals("superadmin")) {
					return true;
				} else {
					response.sendRedirect(loginUrl);
					return false;
				}
			}
		}
		return true;
	}
}
